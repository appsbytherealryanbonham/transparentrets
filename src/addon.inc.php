<?php
function transparentRETS_install_addon()
{
    global $conn, $config, $misc;
    require_once dirname(__FILE__) . '/transparentRETS.inc.php';
    require_once dirname(__FILE__) . '/or2_transparentRETS.inc.php';
    $RETS = new transparentRETS_or2();

    $current_version = $RETS->transparentRETS_current_version();

    $sql = 'SELECT addons_version FROM ' . $config['table_prefix_no_lang']
        . 'addons WHERE addons_name = \'transparentRETS\'';
    $recordSet = $conn->Execute($sql);
    $version = $recordSet->fields(0);
    $sql = [];

    if ($version == '') {
        $sql[] = 'INSERT INTO ' . $config['table_prefix_no_lang']
            . 'addons (addons_version, addons_name) VALUES (\'' . $current_version . '\',\'transparentRETS\')';
        $sql[] = 'CREATE TABLE transparentrets_servers (
        `account` VARCHAR(255) NOT NULL,
        `password` VARCHAR(255) NOT NULL,
        `url` VARCHAR(255) NOT NULL,
        `port` INTEGER NOT NULL,
        `application` VARCHAR(255) NOT NULL,
        `version` VARCHAR(255) NOT NULL,
        `client_password` VARCHAR(255) NOT NULL,
        `keep_alive` BOOLEAN NOT NULL,
        `encode_request` BOOLEAN NOT NULL,
        `post_request` BOOLEAN NOT NULL,
        `account_id` INTEGER NOT NULL AUTO_INCREMENT,
        uapassword varchar(50) NULL,
        rets_vers varchar(50) NOT NULL DEFAULT \'RETS/1.5\',
        listing_batch INT(11) NOT NULL DEFAULT 200,
        photo_batch INT(11) NOT NULL DEFAULT 10,
        `remote_photos` BOOLEAN NOT NULL DEFAULT 0,
        `force_https` BOOLEAN NOT NULL DEFAULT 0,
        PRIMARY KEY(`account_id`)
    );';
        $sql[] = 'CREATE TABLE transparentrets_resources (
        resource_id INTEGER NOT NULL AUTO_INCREMENT,
        resource_name VARCHAR(255) NOT NULL,
        resource_property BOOLEAN NOT NULL,
        account_id INTEGER NOT NULL,
        resources_version VARCHAR(20) NULL,
        class_version VARCHAR(20) NULL,
        object_version VARCHAR(20) NULL,
        searchhelp_version VARCHAR(20) NULL,
        lookup_version VARCHAR(20) NULL,
        resources_agent BOOLEAN NOT NULL DEFAULT 0,
        resources_office BOOLEAN NOT NULL DEFAULT 0,
        PRIMARY KEY (resource_id)
    );';


        $sql[] = 'CREATE TABLE transparentrets_classes (
        class_id INTEGER NOT NULL AUTO_INCREMENT,
        class_name VARCHAR(255) NOT NULL,
        visible_name VARCHAR(64) NULL,
        class_import BOOLEAN NOT NULL,
        class_id_import INTEGER NOT NULL,
        default_agent INTEGER NOT NULL,
        listing_title VARCHAR(255) NOT NULL,
        last_update INTEGER NOT NULL,
        resource_id INTEGER NOT NULL,
        table_version VARCHAR(20) NULL,
        adv_search TEXT NULL,
        modified_map BOOLEAN NOT NULL DEFAULT 0,
        PRIMARY KEY (class_id)
    );';




        $sql[] = 'CREATE TABLE transparentrets_metadata (
        metadata_id INTEGER NOT NULL AUTO_INCREMENT,
        class_id INTEGER NOT NULL,
        metadata_fieldname VARCHAR(256) NOT NULL,
        metadata_fieldtype VARCHAR(10) NOT NULL,
        metadata_default BOOLEAN NOT NULL,
        metadata_uniquefield BOOLEAN NOT NULL,
        metadata_searchfield BOOLEAN NOT NULL,
        metadata_keyfield BOOLEAN NOT NULL,
        metadata_lookupname VARCHAR(256) NULL,
        metadata_interpretation VARCHAR(15) default NULL,
        metadata_required INTEGER default NULL,
        metadata_longname VARCHAR(256) default NULL,
        metadata_shortname VARCHAR(256) default NULL,
        metadata_searchhelpid VARCHAR(256) NOT NULL,
        metadata_orfield INTEGER NOT NULL,
        metadata_listingsupdate_field BOOLEAN NOT NULL,
        metadata_photoupdate_field BOOLEAN NOT NULL,
        metadata_agentid_field BOOLEAN NOT NULL,
        PRIMARY KEY (metadata_id)
    );';
        $sql[] = 'CREATE TABLE transparentrets_lookuptype (
        lookuptype_id INTEGER NOT NULL AUTO_INCREMENT,
        resource_id INTEGER NOT NULL,
        metadata_lookupname VARCHAR(256) NOT NULL,
        metadata_value VARCHAR(255) NOT NULL,
        metadata_longvalue VARCHAR(255) NOT NULL,
        metadata_shortvalue VARCHAR(255) NOT NULL,
        PRIMARY KEY (lookuptype_id)
    );';
        $sql[] = 'CREATE TABLE transparentrets_objects (
        object_id INTEGER NOT NULL AUTO_INCREMENT,
        object_name VARCHAR(255) NOT NULL,
        object_photo BOOLEAN NOT NULL,
        resource_id INTEGER NOT NULL,
        PRIMARY KEY (object_id)
    );';
        $sql[] = 'CREATE TABLE transparentrets_search (
        search_id INTEGER NOT NULL AUTO_INCREMENT,
        class_id INTEGER NOT NULL,
        metadata_id INTEGER NOT NULL,
        search_value TEXT NOT NULL,
        PRIMARY KEY (search_id)
    );';
        $sql[] = 'CREATE TABLE transparentrets_searchhelp (
            searchhelp_id INTEGER NOT NULL AUTO_INCREMENT,
            metadata_searchhelpid VARCHAR(256) NOT NULL,
            metadata_searchhelpvalue TEXT NOT NULL,
            resource_id INTEGER NOT NULL,
            PRIMARY KEY (searchhelp_id)
        );';

        foreach ($sql as $s) {
            $recordSet = $conn->Execute($s);
        }
        return null;
    }
    $sql = [];
    if ($version != $current_version) {
        switch ($version) {
            case '1':
                $sql[] = 'ALTER TABLE transparentrets_servers DROP COLUMN standard_responce';
                $sql[] = 'ALTER TABLE transparentrets_servers DROP COLUMN time_offset';
                // no break
            case '1.0.1':
                $sql[] = 'ALTER TABLE transparentrets_classes ADD COLUMN visible_name VARCHAR(64) NULL';
                // no break
            case '1.0.2':
            case '1.0.3':
            case '1.0.4':
            case '1.0.5':
            case '1.0.6':
                //Release only to asre as a beta.
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN resources_version VARCHAR(20) NULL';
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN class_version VARCHAR(20) NULL';
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN object_version VARCHAR(20) NULL';
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN searchhelp_version VARCHAR(20) NULL';
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN lookup_version VARCHAR(20) NULL';
                $sql[] = 'ALTER TABLE transparentrets_classes ADD COLUMN 	table_version VARCHAR(20) NULL';
                //Drop Unused columns
                $sql[] = 'ALTER TABLE transparentrets_classes DROP COLUMN metadata_version';
                $sql[] = 'ALTER TABLE transparentrets_metadata DROP COLUMN metadata_searchhelpvalue';
                $sql[] = 'CREATE TABLE transparentrets_searchhelp (
                    searchhelp_id INTEGER NOT NULL AUTO_INCREMENT,
                    metadata_searchhelpid VARCHAR(32) NOT NULL,
                    metadata_searchhelpvalue VARCHAR(255) NOT NULL,
                    resource_id INTEGER NOT NULL,
                    PRIMARY KEY (searchhelp_id)
                );';
                // no break
            case '1.1.0':
                $sql[] = 'ALTER TABLE transparentrets_servers ADD COLUMN uapassword varchar(50) NULL';
                $sql[] = 'ALTER TABLE transparentrets_servers ADD COLUMN rets_vers varchar(50) NOT NULL DEFAULT \'RETS/1.5\'';
                $sql[] = 'ALTER TABLE transparentrets_servers ADD COLUMN listing_batch INT(11) NOT NULL DEFAULT 200';
                $sql[] = 'ALTER TABLE transparentrets_servers ADD COLUMN photo_batch INT(11) NOT NULL DEFAULT 10';
                $sql[] = 'ALTER TABLE transparentrets_classes ADD COLUMN adv_search TEXT NULL';
                // no break
            case '1.2.0':
            case '1.2.1':
            case '1.2.2':
            case '1.2.3':
            case '1.2.4':
            case '1.2.5':
            case '1.2.6':
            case '1.2.7':
            case '1.2.8':
            case '1.2.9':
            case '1.2.10':
            case '1.2.11':
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN resources_agent BOOLEAN NOT NULL DEFAULT 0';
                $sql[] = 'ALTER TABLE transparentrets_resources ADD COLUMN resources_office BOOLEAN NOT NULL DEFAULT 0';
                $sql[] = 'ALTER TABLE transparentrets_classes ADD COLUMN modified_map BOOLEAN NOT NULL DEFAULT 0';
                // no break
            case '1.2.12':
            case '1.2.13':
            case '1.2.13b':
            case '1.2.14':
            case '2.0.0':
            case '2.0.1':
            case '2.0.2':
            case '2.0.3':
            case '2.0.4':
                $sql[] = 'ALTER TABLE transparentrets_metadata CHANGE COLUMN `metadata_fieldname` `metadata_fieldname` VARCHAR(256) NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_metadata CHANGE COLUMN `metadata_lookupname` `metadata_lookupname` VARCHAR(256) NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_metadata CHANGE COLUMN `metadata_longname` `metadata_longname` VARCHAR(256) NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_metadata CHANGE COLUMN `metadata_shortname` `metadata_shortname` VARCHAR(256) NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_metadata CHANGE COLUMN `metadata_searchhelpid` `metadata_searchhelpid` VARCHAR(256) NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_searchhelp CHANGE COLUMN `metadata_searchhelpvalue` `metadata_searchhelpvalue` TEXT NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_searchhelp CHANGE COLUMN `metadata_searchhelpid` `metadata_searchhelpid` VARCHAR(256) NOT NULL';
                $sql[] = 'ALTER TABLE transparentrets_lookuptype CHANGE COLUMN `metadata_lookupname` `metadata_lookupname` VARCHAR(256) NOT NULL';
                // no break
            case '2.1.0':
                $sql[] = 'ALTER TABLE transparentrets_servers ADD COLUMN `remote_photos` BOOLEAN NOT NULL DEFAULT 0';
                //`remote_photos` BOOLEAN NOT NULL,

                // no break
            case '2.1.1':
            case '2.1.2':
            case '2.2.0':
            case '2.2.1':
            case '2.3.0-beta.1':
                $sql[] = 'ALTER TABLE transparentrets_servers ADD COLUMN `force_https` BOOLEAN NOT NULL DEFAULT 0';
                // no break
            case '2.3.0-beta.2':
            case '2.3.0':
            case '2.3.1':
            case '2.3.2':
            case '2.3.3':
            case '2.4.0-beta.1':
            default:
                $sql[] = 'UPDATE ' . $config['table_prefix_no_lang']
                    . 'addons SET addons_version = \'' . $current_version . '\' WHERE addons_name = \'transparentRETS\'';
                break;
        }
    }
    foreach ($sql as $s) {
        $recordSet = $conn->Execute($s);
        if (!$recordSet) {
            $misc->log_error($s);
        }
    }
}

function transparentRETS_show_admin_icons()
{
    global $config;
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $admin_link = [];
    // Check for tags: Admin, Agent, canEditForms, canViewLogs, editpages, havevtours
    $login_status = $login->verify_priv('Admin');

    if ($login_status !== false) {
        $admin_link[] =
            '<a href="index.php?action=addon_transparentRETS_config_server"><img src="' . $config['baseurl'] . '/addons/transparentRETS/rets-server.png" /><br />RETS Servers</a>';
        $admin_link[] =
            '<a href="index.php?action=addon_transparentRETS_config_imports"><img src="' . $config['baseurl'] . '/addons/transparentRETS/rets-import.png" /><br />RETS Import Settings</a>';
        //$admin_link[]='<a href="index.php?action=addon_transparentRETS_debug_transaction"><img src="'.$config['baseurl'].'/addons/transparentRETS/rets-import.jpg" /><br />Debug Transaction</a>';
    }

    return $admin_link;
}

function transparentRETS_load_template()
{
    $template_array = [''];
    return $template_array;
}

function transparentRETS_run_action_user_template()
{
    $_GET['action'];

    switch (false) {
        default:
            $data = '';
            break;
    }

    return $data;
}
function transparentRETS_run_action_ajax()
{
    global $config, $conn, $misc;
    ini_set('max_execution_time', 0);

    require_once dirname(__FILE__) . '/transparentRETS.inc.php';
    require_once dirname(__FILE__) . '/or2_transparentRETS.inc.php';
    $RETS = new transparentRETS_or2();
    $data = '';
    switch ($_GET['action']) {
        case 'addon_transparentRETS_configureRETS':
            if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {
                $sql = "SELECT * FROM transparentrets_servers WHERE account_id = " . $_GET['server_id'];
                $servers = $conn->Execute($sql);

                if (!$servers) {
                    $misc->log_error($sql);
                }

                if ($servers->RecordCount() == 1) {
                    $data .= $RETS->config_servers_show_server_settings_form(
                        'EDIT',
                        $servers->fields['account'],
                        $servers->fields['password'],
                        $servers->fields['url'],
                        $servers->fields['client_password'],
                        $servers->fields['port'],
                        $servers->fields['application'],
                        $servers->fields['version'],
                        $servers->fields['keep_alive'],
                        $servers->fields['encode_request'],
                        $servers->fields['post_request'],
                        $servers->fields['uapassword'],
                        $servers->fields['listing_batch'],
                        $servers->fields['photo_batch'],
                        $servers->fields['rets_vers'],
                        $servers->fields['remote_photos'],
                        $servers->fields['force_https']
                    );
                } else {
                    $data .= '<span class="rets_error">Server Config Not Found</span>';
                    $data .= $RETS->config_servers_show_server_settings_form();
                }
            } else {
                $data = $RETS->config_servers_show_server_settings_form();
            }
            break;

        case 'addon_transparentRETS_resourceConfig':
            //pane #2 Resource Config
            $data = $RETS->config_servers_resource_screen();
            break;
        case 'addon_transparentRETS_detectRETSClasses':
            $sql =
                'SELECT resource_name, resource_id FROM transparentrets_resources WHERE account_id = ' . $_GET['server_id'] . ' AND resource_property =1';
            $resource = $conn->Execute($sql);

            if (!$resource) {
                $this->log_error($sql);
            }
            $data .= $RETS->detectRETSClasses($_GET['server_id'], $resource->fields['resource_id']);
            break;
        case 'addon_transparentRETS_classConfig':
            //pane #3 Class Config
            $data = $RETS->config_servers_class_screen();
            break;

        case 'addon_transparentRETS_mediaConfig':
            //pane #4 Media Config
            $data =  $RETS->config_servers_media_screen();
            break;
        case 'addon_transparentRETS_detectRETSResources':
            if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {
                $data .= $RETS->detectRETSResources($_GET['server_id']);
            }
            break;
        case 'addon_transparentRETS_detectRETSMediaObject':
            if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {
                $sql = 'SELECT resource_name, resource_id FROM transparentrets_resources WHERE account_id = ' . $_GET['server_id'] . ' AND resource_property =1';
                $resource = $conn->Execute($sql);
                $data .= $RETS->detectRETSObjects($_GET['server_id'], $resource->fields['resource_id']);
            }
            break;
        case 'addon_transparentRETS_updateField':
            if (isset($_GET['field_id']) && is_numeric($_GET['field_id']) && isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['orfield']) && is_numeric($_GET['orfield'])) {
                $old_field = $RETS->get_metadata_orfield($_GET['field_id']);
                $RETS->set_metadata_orfield($_GET['field_id'], $_GET['orfield']);
                if ($_GET['orfield'] == 0) {
                    $RETS->app_field_unmap($old_field, $_GET['class_id']);
                }
                $RETS->flag_class_map_modified($_GET['class_id']);
                header('Content-type: application/json');
                $data .= json_encode(['error' => "0", 'status_msg' => 'Field Saved']);
            }
            break;
        case 'addon_transparentRETS_automapfield':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['field_id'])) {
                $data .= $RETS->autocreate_orfield($_GET['field_id'], false);

                $RETS->flag_class_map_modified($_GET['class_id']);
            }
            break;
        case 'addon_transparentRETS_auto_unmapfield':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['field_id'])) {
                $old_field = $RETS->get_metadata_orfield($_GET['field_id']);
                $RETS->app_field_unmap($old_field, $_GET['class_id']);

                $RETS->flag_class_map_modified($_GET['class_id']);
            }
            break;
        case 'addon_transparentRETS_automapexistingfields':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id'])) {
                $data .= $RETS->autocreate_orfields($_GET['class_id']);

                $RETS->flag_class_map_modified($_GET['class_id']);
            }
            break;
        case 'addon_transparentRETS_saveimportcriteria':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id'])) {
                $data .= $RETS->save_search_criteria($_GET['class_id']);
            }
            break;
        case 'addon_transparentRETS_save_listingupdate_field':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['listingupdate_field']) && is_numeric($_GET['listingupdate_field'])) {
                $data .= $RETS->save_listingupdate_field($_GET['class_id'], $_GET['listingupdate_field']);
            }
            break;
        case 'addon_transparentRETS_save_photoupdate_field':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['photoupdate_field'])) {
                $data .= $RETS->save_photoupdate_field($_GET['class_id'], intval($_GET['photoupdate_field']));
            }
            break;
        case 'addon_transparentRETS_save_agentid_field':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['agentid_field']) && is_numeric($_GET['agentid_field'])) {
                $data .= $RETS->save_agentid_field($_GET['class_id'], $_GET['agentid_field']);
            }
            break;
        case 'addon_transparentRETS_save_default_agentid':
            if (isset($_GET['class_id']) && is_numeric($_GET['class_id']) && isset($_GET['default_agentid']) && is_numeric($_GET['default_agentid'])) {
                $data .= $RETS->save_default_agentid($_GET['class_id'], $_GET['default_agentid']);
            }
            break;
        case 'addon_transparentRETS_save_listing_title':
            if (isset($_POST['class_id']) && is_numeric($_POST['class_id']) && isset($_POST['listing_title'])) {
                $data .= $RETS->save_listing_title($_POST['class_id'], $_POST['listing_title']);
            }
            break;
        case 'addon_transparentRETS_save_resource_config':
            if (isset($_GET['server_id']) && is_numeric($_GET['server_id']) && isset($_GET['prop_resource']) && is_numeric($_GET['prop_resource'])) {
                $data .= $RETS->save_resource_config($_GET['server_id'], $_GET['prop_resource']);
            }
            break;
        case 'addon_transparentRETS_save_media_config':
            if (isset($_GET['resource_id']) && is_numeric($_GET['resource_id']) && isset($_GET['media_object']) && is_numeric($_GET['media_object'])) {
                $data .= $RETS->update_media($_GET['media_object'], $_GET['resource_id']);
            }
            break;
        case 'addon_transparentRETS_check_metadata_updates':
            if (isset($_GET['resource_id']) && is_numeric($_GET['resource_id']) && isset($_GET['class_id']) && is_numeric($_GET['class_id'])) {
                $data .= $RETS->check_metadata_updates($_GET['resource_id'], $_GET['class_id']);
                $data .= $RETS->show_field_map($_GET['class_id']);
            }
            break;
        case 'addon_transparentRETS_loadRETS':
            $data .= $RETS->run_loader();
            break;
        case 'addon_transparentRETS_run_debug_transaction':
            $data .= $RETS->run_debug_transaction($_POST['class_id'], $_POST['rets_query']);
            break;
        case 'addon_transparentRETS_show_rets_criteria_query':
            $data .= $RETS->show_rets_criteria_query($_POST['class_id']);
            break;
        case 'addon_transparentRETS_show_rets_criteria_count':
            $data .= $RETS->show_rets_criteria_count($_POST['class_id']);
            break;

        case 'addon_transparentRETS_make_fieldname_csv':
            $class_id = intval($_GET['class_id']);
            $data .= $RETS->make_fieldname_csv($class_id);
            break;
    }
    return $data;
}
function transparentRETS_run_action_admin_template()
{
    global $config, $conn;
    ini_set('max_execution_time', 0);
    require_once dirname(__FILE__) . '/transparentRETS.inc.php';
    require_once dirname(__FILE__) . '/or2_transparentRETS.inc.php';
    $RETS = new transparentRETS_or2();
    $data = '';
    $data .= $RETS->show_header();
    switch ($_GET['action']) {
        case 'addon_transparentRETS_config_server':
            $data .= $RETS->config_servers();
            break;
        case 'addon_transparentRETS_config_imports':
            $data .= $RETS->config_imports();
            break;
        case 'addon_transparentRETS_loadRETS':
            $data .= $RETS->run_loader();
            break;
        case 'addon_transparentRETS_updateMetadata':
            $r_id = intval($_GET['r_id']);
            $c_id = intval($_GET['c_id']);
            if (isset($_GET['force_metadataupdate']) && $_GET['force_metadataupdate'] == 1) {
                $force_update = true;
            } else {
                $force_update = false;
            }
            $data .= $RETS->check_metadata_updates($r_id, $c_id, true, $force_update);
            break;
        case 'addon_transparentRETS_cleanTables':
            $data .= $RETS->cleantables();
            break;
        case 'addon_transparentRETS_cleanAppFields':
            $data .= $RETS->cleanappfields();
            break;
    }

    $data .= $RETS->show_footer();
    return $data;
}