<?php
class transparentRETS_or2 extends TransparentRETS_loader
{
    public function __construct()
    {
        global $conn;
        parent::__construct();
        $this->conn = $conn;
    }

    public function show_header()
    {
        global $config, $jscript;

        $jscript .= '<script type="text/javascript" src="' . $config['baseurl'] . '/addons/transparentRETS/domLib.js"></script>

			<script type="text/javascript" src="' . $config['baseurl'] . '/addons/transparentRETS/domTT.js"></script>
			<script type="text/javascript" src="' . $config['baseurl'] . '/addons/transparentRETS/jquery.csv-0.71.min.js"></script>
			

<style type="text/css">
div.domTTOverlib {
	border: 1px solid #333366;
	background-color: #333366;
}

div.domTTOverlib .caption {
	font-family: Verdana, Helvetica;
	font-size: 10px;
	font-weight: bold;
	color: #FFFFFF;
}

div.domTTOverlib .contents {
	font-size: 10px;
	font-family: Verdana, Helvetica;
	padding: 2px;
	background-color: #F1F1FF;
}

div.rets_error {
	font-weight:bold;
	color:red;
	width:100%;
	text-align:center;
}



.trets_std_button:hover {
	border-color: #999;
	box-shadow: none;
	background: #f7f7f7; /* Old browsers */
	background: -moz-linear-gradient(top, #f7f7f7 0%, #fcfcfc 50%, #efefef 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f7f7f7), color-stop(50%,#fcfcfc), color-stop(100%,#efefef)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #f7f7f7 0%,#fcfcfc 50%,#efefef 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #f7f7f7 0%,#fcfcfc 50%,#efefef 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #f7f7f7 0%,#fcfcfc 50%,#efefef 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f2f2f2", endColorstr="#efefef",GradientType=0 ); /* IE6-9 */
}
.trets_std_button {
	outline: none;
	color: #2f4f4f;
	-moz-border-radius: 10px;
	-khtml-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	cursor: pointer;	
    border: 1px solid #aaaaaa;
    margin-left: 2px;
    padding: 4px 6px;
    -webkit-box-shadow: 1px 1px 0px rgba(1, 0, 0, 0.65);
	-moz-box-shadow: 1px 1px 0px rgba(1, 0, 0, 0.65);
	box-shadow: 1px 1px 0px rgba(1, 0, 0, 0.65);
	background: #f2f2f2; /* Old browsers */
	background: -moz-linear-gradient(top, #f2f2f2 0%, #fcfcfc 41%, #e0e0e0 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f2f2), color-stop(41%,#fcfcfc), color-stop(100%,#e0e0e0)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%); /* IE10+ */

}

.trets_std_button.red {
	background: #f4e1e1; /* Old browsers */
	background: -moz-linear-gradient(top, #f4e1e1 0%, #f4a8a8 51%, #e87676 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f4e1e1), color-stop(51%,#f4a8a8), color-stop(100%,#e87676)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #f4e1e1 0%,#f4a8a8 51%,#e87676 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #f4e1e1 0%,#f4a8a8 51%,#e87676 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #f4e1e1 0%,#f4a8a8 51%,#e87676 100%); /* IE10+ */
}

.trets_std_button.red:hover {

}

.select_bar {
	background: #F4F7FF;
	background: -moz-linear-gradient(top, #F4F7FF 0%, #D2D8E0 40%, #C0CEC7 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F4F7FF), color-stop(40%,#D2D8E0), color-stop(100%,#C0CEC7));
	background: -webkit-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	background: -o-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	background: -ms-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f4f7ff", endColorstr="#c0cec7",GradientType=0 );
	-webkit-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	-moz-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);

}

#select_class fieldset, #select_server fieldset {
	margin: 0;
}
.select_bar span {
	font-size: 12pt;
	text-shadow: 1px 1px 1px white;
}
#select_class select, #select_server select {
	font-size: 12pt;
	background: #fcefce;
	border-width: 1px 2px 2px 1px;
	border-color: #777;
	border-style: solid;	
}

#select_class select:focus, #select_server select:focus {
	background: #fff;
}

#select_class option[selected=selected], #select_server option[selected=selected] {
	background: #yellow;
}

.page_name_div {
	float: right;
	margin-right: 10px;
}

.page_name_div span {
	font-size: 1.4em;
	color: #000; 
	text-shadow: 1px 1px 1px white;
}

.page_name_div img {
	vertical-align: bottom;
}

#application_status_message {
	background: rgba(179,220,237,1);
	background: -moz-linear-gradient(top, rgba(179,220,237,1) 0%, rgba(41,184,229,1) 49%, rgba(188,224,238,1) 100%);
	background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(179,220,237,1)), color-stop(49%, rgba(41,184,229,1)), color-stop(100%, rgba(188,224,238,1)));
	background: -webkit-linear-gradient(top, rgba(179,220,237,1) 0%, rgba(41,184,229,1) 49%, rgba(188,224,238,1) 100%);
	background: -o-linear-gradient(top, rgba(179,220,237,1) 0%, rgba(41,184,229,1) 49%, rgba(188,224,238,1) 100%);
	background: -ms-linear-gradient(top, rgba(179,220,237,1) 0%, rgba(41,184,229,1) 49%, rgba(188,224,238,1) 100%);
	background: linear-gradient(to bottom, rgba(179,220,237,1) 0%, rgba(41,184,229,1) 49%, rgba(188,224,238,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#b3dced", endColorstr="#bce0ee", GradientType=0 );
}

#class_table {
	width: 100%;
	border: 1px solid #000;
}

#class_table th {
	background: #d9d9d9;
}

#class_table td {
	border-bottom: 1px solid #000;
	padding: 4px;
}

#class_table img {
	display: block;
    margin-left: auto;
    margin-right: auto
}

#class_table td:first-child{
	width: 33%;
}

#class_table tr:hover {
	background: #fff;
}

#trets_import_output {
	width: 100%;
	min-height: 450px;
	border-top: 1px solid #eee;
	border-bottom: 1px solid #eee;
	padding-top: 4px;
}

#processing {
	padding-bottom: 5px;
}

#lic_div {
	padding: 4px;
	margin-bottom: 10px;
	background: #F4F7FF;
	background: -moz-linear-gradient(top, #F4F7FF 0%, #D2D8E0 40%, #C0CEC7 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F4F7FF), color-stop(40%,#D2D8E0), color-stop(100%,#C0CEC7));
	background: -webkit-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	background: -o-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	background: -ms-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f4f7ff", endColorstr="#c0cec7",GradientType=0 );
	-webkit-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	-moz-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75)
}

#lic_div div {
	margin-top: 0em;
	display: inline-block;
	line-height: 24px;
}

#lic_table {
	width: 100%;
	text-align: right;
	padding: 4px 0px;
	margin: 0px
	background: #F4F7FF;
	background: -moz-linear-gradient(top, #F4F7FF 0%, #D2D8E0 40%, #C0CEC7 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F4F7FF), color-stop(40%,#D2D8E0), color-stop(100%,#C0CEC7));
	background: -webkit-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	background: -o-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	background: -ms-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f4f7ff", endColorstr="#c0cec7",GradientType=0 );
	-webkit-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	-moz-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
}

#lic_table td a {
	border: 1px solid #999;
	margin: 0px 1px;
	padding: 4px 6px;
	background: #f2f2f2;
	background: -moz-linear-gradient(top, #f2f2f2 0%, #fcfcfc 41%, #e0e0e0 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f2f2), color-stop(41%,#fcfcfc), color-stop(100%,#e0e0e0));
	background: -webkit-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
	background: -o-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
	background: -ms-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f2f2f2", endColorstr="#e0e0e0",GradientType=0 );
}

#lic_table td img {
	vertical-align: bottom;
	width: 16px;
	height: 16px;
}

#csv_export, #csv_import {
 	cursor: pointer;	
    border: 1px solid #aaaaaa;
    margin-left: 2px;
    padding: 4px 6px;
    background: #f2f2f2; /* Old browsers */
	background: -moz-linear-gradient(top, #f2f2f2 0%, #fcfcfc 41%, #e0e0e0 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f2f2), color-stop(41%,#fcfcfc), color-stop(100%,#e0e0e0)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f2f2f2", endColorstr="#e0e0e0",GradientType=0 ); /* IE6-9 */
}

#csv_export:hover, #csv_import:hover, #lic_table td a:hover {
	background: #f7f7f7; /* Old browsers */
	background: -moz-linear-gradient(top, #f7f7f7 0%, #fcfcfc 50%, #efefef 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f7f7f7), color-stop(50%,#fcfcfc), color-stop(100%,#efefef)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #f7f7f7 0%,#fcfcfc 50%,#efefef 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #f7f7f7 0%,#fcfcfc 50%,#efefef 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #f7f7f7 0%,#fcfcfc 50%,#efefef 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f2f2f2", endColorstr="#efefef",GradientType=0 ); /* IE6-9 */
}

#csv_export [type=button], #csv_import [type=file] {
    cursor: inherit;
    display: block;
    filter: alpha(opacity=0);
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
    cursor: pointer;
}
#csv_export img, #csv_import img {
	vertical-align: bottom;
	width: 16px;
	height: 16px;
}
@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
  img[src*=".svg"] {
    width: 100%; 
  }
}

#form_search_criteria textarea {
	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
     -moz-box-sizing: border-box;    /* Firefox, other Gecko */
     width: 100%;
}

#form_search_criteria td {
	padding: 4px;
}

#id_listing_title {
	width: 90%;
}

#import_criteria {
	border-collapse:collapse
}

#import_criteria tr {
	border-bottom: 1px solid #bbb;
}

#import_criteria label{
	font-size: 1.1em;
	line-height: 1em;
}

</style>';

        if (isset($_GET['action']) && $_GET['action'] == 'addon_transparentRETS_config_imports') {
            $this->page_name = 'RETS Import Settings';
            $this->page_img = '<img src="' . $config['baseurl'] . '/addons/transparentRETS/rets-import.png" width="25">';
            $util_link = '<a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_server">
						<img src="' . $config['baseurl'] . '/addons/transparentRETS/rets-server.png" width="20"> RETS Server settings
					</a>';
        } else {
            $this->page_name = 'RETS Server settings';
            $this->page_img = '<img src="' . $config['baseurl'] . '/addons/transparentRETS/rets-server.png" width="25">';
            $util_link = '<a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports">
						<img src="' . $config['baseurl'] . '/addons/transparentRETS/rets-import.png" width="20"> RETS import settings
					</a>';
        }

        $display = '<div class ="application_main">

				<div class="application_section">
					<div class="application_section_title">TransparentRETS <span style="font-size: 8pt;">v' . $this->transparentRETS_current_version() . '</span></div>
					<div class="application_status_message" id="application_status_message"></div>
				</div>
				<div class="inner_div">
					<div class="inner_div_wrapper">';


        $display .= '<div>';
        return $display;
    }

    public function show_footer()
    {
        $display = '</div>
					<div style="width:100%; text-align: center;">
						TransparentRETS&trade; v' . $this->transparentRETS_current_version() . '</a> &copy;2021
					</div>
				</div>
				<div style="clear: both;"></div>
			</div>
		</div>';

        return $display;
    }

    public function log_action($msg)
    {
        global $config, $misc;
        $misc->log_action($msg);
        return $msg;
    }

    public function log_error($msg)
    {
        global $config, $misc;
        $misc->log_error($msg);
    }

    public function delete_listing($listing_id)
    {
        global $api;
        $result = $api->load_local_api('listing__delete', ['listing_id' => $listing_id]);
        if ($result['error'] == 0) {
            return true;
        }
        return false;
    }

    public function debug_transaction($class_id)
    {
        global $config;
        $display = '';
        /*
        if(isset($_POST['class_id'])){
            $class_id = intval($_POST['class_id']);
                $display.= '<div>Debug Info<br />Class: '.$class_id.'</div>';
                $display .= $this->run_debug_transaction($class_id,$_POST['rets_query']);
        }
        */
        $retsQuery = '';
        if (in_array('rets_query', $_POST)) {
            $retsQuery = strip_html($_POST['rets_query']);
        }
        $display .= '<form action="#" method="get">
					<div>
					<div style="float: left; margin-left: 10px;"> 
						<strong>Custom RETS Query:</strong>
					</div>	
					<div style="float: right; margin-right: 10px; margin-bottom: 10px;"> 
						<button class="trets_std_button" id="copyquery" >Copy</button> to Advanced Search Options
					</div>
					<div style="clear: right;"></div>
					</div>
				

					<textarea id="rets_query" name="rets_query" style="width:100%;height:60px;">' . $retsQuery . '</textarea>
					<input type="hidden" name="action" value="addon_transparentRETS_config_imports" />

				</form>
				<br />
				<div style="float: left;">
					<button id="run_rets_debug_query" class="trets_std_button">Run Query</button> <button id="clear_trets_debug_output" class="trets_std_button red">Clear Output</button>
				</div>
				<div style="float: right; border: 1px solid #e2e2e2; padding: 4px;">
					<span>Presently set Import Criteria: </span>
					<button id="show_rets_criteria_query" class="trets_std_button">Show DMQL</button> 
					<button id="show_rets_criteria_count" class="trets_std_button">Listing Count</button>
				</div>
				
				<div style="height: 20px; clear: right;">&nbsp;</div>
				
				<strong>OUTPUT:</strong> 
				<br />
				<textarea style="width:100%;height:300px; font-size: 1.2em; font-family:Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace;"  id="trets_debug_output"></textarea>
				<div id="dialog-debug" class="d-none" title="Confirm">
					<p id="debug-message_text"></p>
				</div>
				
		<script type="text/javascript">
				$("#clear_trets_debug_output").click(function(){
					$("#trets_debug_output").val("");
				});
				$("#run_rets_debug_query").click(function(){
					ShowPleaseWait();
					$.post( "ajax.php?action=addon_transparentRETS_run_debug_transaction", { class_id: "' . $class_id . '", rets_query: $("#rets_query").val() }, function( data ) {
						$("#trets_debug_output").val(data);
						HidePleaseWait();
					});
				});
				$("#show_rets_criteria_query").click(function(){
					ShowPleaseWait();
					$.post( "ajax.php?action=addon_transparentRETS_show_rets_criteria_query", { class_id: "' . $class_id . '" }, function( data ) {
						$("#trets_debug_output").val(data);
						HidePleaseWait();
					});
				});
				$("#show_rets_criteria_count").click(function(){
					ShowPleaseWait();
					$.post( "ajax.php?action=addon_transparentRETS_show_rets_criteria_count", { class_id: "' . $class_id . '" }, function( data ) {
						$("#trets_debug_output").val(data);
						HidePleaseWait();
					});
				});
				
				$("#copyquery").click(function(e){
                    e.preventDefault();
					var cur_classid = "' . $class_id . '";
					if (cur_classid == "") {
                        raiseModal("Error", "You must first select a Property Class");
					} else{
						var querytext = $("#rets_query").val();
                        if (querytext == ""){
                            status_error("No Query specified");							
                        } else {	
                            var insert_html = [
                                "",
                                "<span class=\"danger\">Warning!</span>", 
                                "<br /><br />", 
                                "This will overwrite your presently configured import criteria.",
                                "<div>",
                                "<button id=\"copy_query_btn\" type=\"button\" class=\"btn btn-primary btn-sm copy_query\" aria-label=\"Close\">Ok</button>",
                                "<button type=\"button\" class=\"btn btn-danger btn-sm\" data-bs-dismiss=\"modal\" aria-label=\"Close\">Cancel</button>",
                                "</div>"
                                ].join("");
                            raiseModal("Confirm", insert_html);	
                        }
					}
				});	

                $("#customizeModal").on("click", "#copy_query_btn", function (e) {
                    // do something...
                    e.preventDefault();
                    var querytext = $("#rets_query").val();
                    $.post("ajax.php?action=addon_transparentRETS_saveimportcriteria&class_id=' . $class_id . '", {
                        transparentrets_adv_search:  querytext
                    },
                    function(data) {
                        if(data.error == "1"){
                            status_error(data.error_msg,focus);
                        }else{
                            status_msg(data.status_msg,focus);
                        }
                        closeModal();
                    });
                       
                    
                });
		</script>';

        return $display;
    }

    public function config_servers()
    {
        global $config;
        $display = '<script type="text/javascript">
        $(document).ready(function () {
    
            //Enable ajax based tags
            $(\'[data-bs-toggle="tabajax"]\').click(function (e) {
                e.preventDefault();
                var $this = $(this),
                    loadurl = $this.attr("href"),
                    targ = $this.attr("data-bs-target");
                // if ($(targ).html() == "") {
                    $.get(loadurl, function (data) {
                        $(targ).html(data);
                    });
                // }
                $this.tab("show");
            });
            $("#CR_tab").click();
        });
        </script>';

        $display .= '<div class="card card-frame mb-4">
        <div class="card-body py-2">';

        if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {
            $server_id = $_GET['server_id'];
        } else {
            $server_id = '';
        }

        //Check to see if we need to save data
        if (isset($_POST['type'])) {
            if ($_POST['type'] == 'INSERT') {
                $_GET['server_id'] = $this->insert_server(
                    $_POST['RETS_SERVER_ACCOUNT'],
                    $_POST['RETS_SERVER_PASSWORD'],
                    $_POST['RETS_SERVER_URL'],
                    $_POST['RETS_DEFAULT_PORT'],
                    $_POST['APPLICATION'],
                    $_POST['VERSION'],
                    $_POST['RETS_CLIENT_PASSWORD'],
                    $_POST['KEEP_ALIVE_SERVER'],
                    $_POST['ENCODE_REQUESTS'],
                    $_POST['POST_REQUESTS'],
                    $_POST['RETS_UA_PASSWORD'],
                    $_POST['RETS_VERSION'],
                    $_POST['LISTING_BATCH'],
                    $_POST['PHOTO_BATCH'],
                    $_POST['REMOTE_PHOTOS'],
                    $_POST['FORCE_HTTPS']
                );
                $display .= '
				<script type="text/javascript">
				status_msg("Server Added");
				</script>';
            } else {
                $this->update_server(
                    $_POST['RETS_SERVER_ACCOUNT'],
                    $_POST['RETS_SERVER_PASSWORD'],
                    $_POST['RETS_SERVER_URL'],
                    $_POST['RETS_DEFAULT_PORT'],
                    $_POST['APPLICATION'],
                    $_POST['VERSION'],
                    $_POST['RETS_CLIENT_PASSWORD'],
                    $_POST['KEEP_ALIVE_SERVER'],
                    $_POST['ENCODE_REQUESTS'],
                    $_POST['POST_REQUESTS'],
                    $_POST['RETS_UA_PASSWORD'],
                    $_POST['RETS_VERSION'],
                    $_POST['LISTING_BATCH'],
                    $_POST['PHOTO_BATCH'],
                    $_POST['REMOTE_PHOTOS'],
                    $_POST['FORCE_HTTPS']
                );
                $display .= '
				<script type="text/javascript">
				status_msg("Server Updated");
				</script>';
            }
        }

        if (isset($_GET['delete_aid']) && is_numeric($_GET['delete_aid'])) {
            $this->delete_server($_GET['delete_aid']);
        }

        $display .= $this->config_servers_show_server_select();

        $display .= BR;

        $display .= '<div class="nav-wrapper position-relative end-0">
        <ul
         class="nav nav-pills nav-fill p-1"
         id="nav-tab"
         role="tablist"
        >
            <li class="nav-item">
                <a
                class="nav-link mb-0 px-0 py-1 active"
                data-bs-toggle="tabajax"
                data-bs-target="#CR"
                id="CR_tab"
                href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_configureRETS&amp;server_id=' . $server_id . '"
                role="tab"
                aria-controls="CR"
                aria-selected="true"
                >
                Configure RETS Server
                </a>
            </li>
            <li class="nav-item">
                <a
                class="nav-link mb-0 px-0 py-1"
                data-bs-toggle="tabajax"
                data-bs-target="#RC"
                id="RC_tab"
                href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_resourceConfig&amp;server_id=' . $server_id . '"
                role="tab"
                aria-controls="RC"
                aria-selected="false"
                >
                Resource Configuration
                </a>
            </li>
            <li class="nav-item">
                <a
                class="nav-link mb-0 px-0 py-1"
                data-bs-toggle="tabajax"
                data-bs-target="#MC"
                id="MC_tab"
                href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_mediaConfig&amp;server_id=' . $server_id . '"
                role="tab"
                aria-controls="MC"
                aria-selected="false"
                >
                Media Configuration
                </a>
            </li>
            <li class="nav-item">
                <a
                class="nav-link mb-0 px-0 py-1"
                data-bs-toggle="tabajax"
                data-bs-target="#CC"
                id="CC_tab"
                href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_classConfig&amp;server_id=' . $server_id . '"
                role="tab"
                aria-controls="CC"
                aria-selected="false"
                >
                Class Utilities
                </a>
            </li>
       </ul> 
      ';


        // $display .= '<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_configureRETS&amp;server_id=' . $server_id . '" id ="CR">Configure RETS Server</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_resourceConfig&amp;server_id=' . $server_id . '" id ="RC">Resource Configuration</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_mediaConfig&amp;server_id=' . $server_id . '" id ="MC">Media Configuration</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_classConfig&amp;server_id=' . $server_id . '" id ="CC">Class Utilities</a></li>' . BR;


        $display .= ' <div class="tab-content">
            <div
                class="tab-pane fade show active"
                id="CR"
                role="tabpanel"
                aria-labelledby="CR_tab"
            ></div>
            <div
                class="tab-pane fade"
                id="RC"
                role="tabpanel"
                aria-labelledby="RC_tab"
            ></div>
            <div
                class="tab-pane fade"
                id="MC"
                role="tabpanel"
                aria-labelledby="MC_tab"
            ></div>
            <div
                class="tab-pane fade"
                id="CC"
                role="tabpanel"
                aria-labelledby="CC_tab"
            ></div>
        </div></div>';

        // $display .= '<div id="css-tabs">
        // 				<ul>' . BR;

        // $display .= '<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_configureRETS&amp;server_id=' . $server_id . '" id ="CR">Configure RETS Server</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_resourceConfig&amp;server_id=' . $server_id . '" id ="RC">Resource Configuration</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_mediaConfig&amp;server_id=' . $server_id . '" id ="MC">Media Configuration</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/ajax.php?action=addon_transparentRETS_classConfig&amp;server_id=' . $server_id . '" id ="CC">Class Utilities</a></li>' . BR;

        // $display .= '</ul>' . BR;



        // //End Tabs
        // $display .= '</div>' . BR . '<!--END -->' . BR;
        $display .= '</div></div>';
        return $display;
    }

    public function config_servers_media_screen()
    {
        global $config;
        $display = '';

        if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {
            //

            //Make Sure a Property Resource has been selected
            $sql =
                'SELECT resource_name, resource_id FROM transparentrets_resources 
				WHERE account_id = ' . $_GET['server_id'] . ' 
				AND resource_property =1';
            $resource = $this->conn->Execute($sql);

            if (!$resource) {
                $this->log_error($sql);
            }

            if (isset($_POST['detectObject']) && $_POST['detectObject'] == 1) {
                $this->detectRETSObjects($_GET['server_id'], $resource->fields['resource_id']);
            }


            if ($resource->RecordCount() > 0) {
                $sql =
                    'SELECT object_id, object_name,object_photo FROM transparentrets_objects 
					WHERE resource_id = ' . $resource->fields['resource_id'];
                $rclasses = $this->conn->Execute($sql);

                if (!$rclasses) {
                    $this->log_error($sql);
                }
                if ($rclasses->RecordCount() > 0) {
                    $display .= '<form action="index.php?server_id=' . $_GET['server_id']
                        . '&amp;action=addon_transparentRETS_config_server" method="post">
							<fieldset>
								<label onmouseover="domTT_activate(this, event, \'caption\', \'Media Resource\', \'content\',\'Select the Media Resource that holds the Photos for listings. This is normally called &quot;Photo&quot; and should have been selected for you automatically.\', \'trail\', \'x\');" for="id_media_object">
									Select Media Resource: <select name="media_object" id="id_media_object">
															<option value="">Select the Media (Photo) Object</option>';

                    while (!$rclasses->EOF) {
                        $display .= '<option value="' . $rclasses->fields['object_id'] . '"';

                        if ($rclasses->fields['object_photo'] == true) {
                            $display .= ' selected="selected" ';
                        }

                        $display .= '>' . $rclasses->fields['object_name'] . '</option>';
                        $rclasses->MoveNext();
                    }
                    $display
                        .= '</select></label> <input type="button" class="trets_std_button" onclick="save_media_config();return false;" value="Select Media Object" /></fieldset></form>';
                    $display .= '
				<script type="text/javascript">
				function save_media_config(){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_save_media_config&resource_id=' . $resource->fields['resource_id'] . '&media_object="+$("#id_media_object").val(), function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);

						}
						HidePleaseWait();
					});

				};
				</script>';
                } else {
                    $display .= '<form action="index.php?server_id=' . $_GET['server_id']
                        . '&amp;action=addon_transparentRETS_config_server" method="post"><fieldset><input type="hidden" name="detectObject" value="1" /><input type="submit" onclick="RundetectRETSMediaObject();return false;"value="Get object/media information from RETS server" /></fieldset></form>';
                    $display .= '
					<script type="text/javascript">
					

						function RundetectRETSMediaObject(){
							ShowPleaseWait();
							$("#detectResourcesDiv").load("ajax.php?action=addon_transparentRETS_detectRETSMediaObject&server_id=' . $_GET['server_id'] . '",function(){
							HidePleaseWait();
							});

						};

					
					</script>
				';
                }
            } else {
                $display
                    .= '<div class="rets_error">You need to select a resource on the previous tab</div>';
            }
        } else {
            $display .= '<div class="rets_error">You need to select a server</div>';
        }

        return $display;
    }

    public function config_servers_class_screen()
    {
        global $config;
        $display = '';

        if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {
            //Make Sure a Property Resource has been selected

            $server_id = $_GET['server_id'];

            $sql = 'SELECT resource_name, resource_id 
				FROM transparentrets_resources 
				WHERE account_id = ' . $_GET['server_id'] . ' 
				AND resource_property = 1';
            $resource = $this->conn->Execute($sql);

            if (!$resource) {
                $this->log_error($sql);
            }

            if ($resource->RecordCount() > 0) {
                $sql = 'SELECT class_id,class_name,visible_name,class_import,class_id_import 
					FROM transparentrets_classes 
					WHERE resource_id = ' . $resource->fields['resource_id'];
                $rclasses = $this->conn->Execute($sql);

                if (!$rclasses) {
                    $this->log_error($sql);
                }

                if ($rclasses->RecordCount() > 0) {
                    $display .= '<div style="font-weight:bold;">
									Available Classes in the RETS Property Resource
								</div>
								<br />
					
								<div style="width: 75%; float: left;">
									<table border="0" cellpadding="0" cellspacing="0" id="class_table">
										<tr>
											<th>RETS Class Name</th>
											<th>Update RETS Import</th>
											<th>Refresh Metadata</th>
											<th>Refresh ALL Listings</th>
											<th>Refresh ALL Photos</th>
										</tr>';
                    $num = 0;
                    while (!$rclasses->EOF) {
                        $display .= '<tr>
										<td>
											' . $rclasses->fields['class_name'] . ' 
											<a href="index.php?class_id=' . $rclasses->fields['class_id'] . '&action=addon_transparentRETS_config_imports" style="font-weight: bold;"> 
												(' . $rclasses->fields['visible_name'] . ')
											</a>
										</td>';

                        if ($rclasses->fields['class_import'] > 0 && $rclasses->fields['class_id_import'] > 0) {
                            //Show Metadata, update, Refresh listings and photos
                            //index.php?action=addon_transparentRETS_loadRETS&amp;pclass='.$rclasses->fields['class_id'].'
                            //action=addon_transparentRETS_updateMetadata&amp;r_id='.$resource->fields['resource_id'].'&amp;c_id='.$rclasses->fields['class_id'].'
                            $display .= '
										<td>
											<a href="#" onclick="confirmtask(1, ' . $rclasses->fields['class_id'] . ' , \'' . $rclasses->fields['visible_name'] . '\' ); return false;">
												<img src="../addons/transparentRETS/icons/1downarrow.png" alt="Update RETS Import" title="Update ' . $rclasses->fields['visible_name'] . ' RETS Import"/>
											</a>
										</td>	
										<td>
											<a href="#"  onclick="confirmtask(2, ' . $rclasses->fields['class_id'] . ', \'' . $rclasses->fields['visible_name'] . '\' ); return false;">
												<img src="../addons/transparentRETS/icons/refresh.png" alt="Refresh Metadata" title="Refresh ' . $rclasses->fields['visible_name'] . ' Metadata" />
											</a>			
										</td>
										<td>
											<a href="#"  onclick="confirmtask(3, ' . $rclasses->fields['class_id'] . ', \'' . $rclasses->fields['visible_name'] . '\' ); return false;">
												<img src="../addons/transparentRETS/icons/2downarrow.png" alt="Refresh ALL Listings" title="Refresh ALL ' . $rclasses->fields['visible_name'] . ' Listings" />
											</a>
										</td>
										<td>
											<a href="#"  onclick="confirmtask(4, ' . $rclasses->fields['class_id'] . ', \'' . $rclasses->fields['visible_name'] . '\' ); return false;">
												<img src="../addons/transparentRETS/icons/2downarrow.png" alt="Refresh ALL Photos" title="Refresh ALL ' . $rclasses->fields['visible_name'] . ' Photos" />
											</a>
										</td>';
                            $num++;
                        } else {
                            $display .= '<td colspan="4">
											Not Configured. <a href="index.php?class_id=' . $rclasses->fields['class_id'] . '&action=addon_transparentRETS_config_imports" style="font-weight: bold;">Click to setup</a>
										</td>';
                        }
                        $display .= '</tr>';


                        $rclasses->MoveNext();
                    }
                    //this has to all be on one line
                    $importwindow = '<iframe src=\"about:blank\" id=\"trets_import_output\"	style=\"font-size: 1em; font-family:Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace;\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"auto\"></iframe>';
                    //$importwindow = '<div style=\"font-size: 1em; font-family:Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace;\" id=\"trets_import_output\"></div>';

                    $display .= '</table>
								</div>
								<div style="width: 25%; float: left;">
								
									<div style="width: 100%; margin: 0px 6px; border: 1px solid #666;">
									<div style="background: #d9d9d9; text-align: center; padding: 6px; border-bottom: 1px solid #666;">
										<span style="font-size: 1.1em; font-weight: bold; ">Affects ALL Classes</span>
									</div>	
									<div style="padding: 10px;">	
										<a href="" class="trets_std_button" onclick="confirmlong(1); return false;">Update ALL</a>
									</div>
									<div style="padding: 10px;">	
										<a href="#" class="trets_std_button" onclick="confirmlong(2); return false;" >Refresh METADATA</a>
									</div>
									<div style="padding: 10px;">											
										<a href="#" class="trets_std_button" onclick="confirmlong(3); return false;">Refresh LISTINGS</a>
									</div>
									<div style="padding: 10px;">	
										<a href="#" class="trets_std_button" onclick="confirmlong(4); return false">Refresh PHOTOS</a>
									</div>
									</div>		
								</div>
								<div style="clear: left;">
								
								<div id="dialog-import" class="d-none" title="">
									<div id="processing"></div>
									<div style="padding-top: 5px;" id="import-message_text"></div>
								</div>
								
								<script type="text/javascript">
								
									function confirmtask(classtask, class_id, visible_name){
										var importwindow = "' . $importwindow . '";
										$("#processing").empty();
										var t_modifiers = "";
										var in_use ="";
										
										switch (classtask) {
											case 1:
												t_modifiers = "&pclass="+class_id;
												break;
											case 2:
												t_modifiers = "&pclass="+class_id+"&force_metadataupdate=1";
												break;
											case 3:
												t_modifiers = "&pclass="+class_id+"&listing_last_update=0&listingupdate_only=1";
												break;
											case 4:
												t_modifiers = "&pclass="+class_id+"&photo_last_update=0";
												break;
											default:
												break;
										}
										
										in_use = "<div>Using import modifier: <span style=\"font-weight: bold; font-size: 1em;\">"+t_modifiers+"</span></div>";
										
										var t_task = [ "<div id=\"thinker\" class=\"spinner-border text-primary\" role=\"status\"> <span class=\"sr-only\">Loading...</span> </div>",
														"&nbsp;"+in_use
													].join("");
                                        
                                        var html = $("#dialog-import").html();
			                            raiseModal("RETS Server "+' . $server_id . '+"- Import Activity", html);
                                        
										$("#customizeModal #processing").html(t_task);
										$("#customizeModal #import-message_text").html(importwindow);
										$("#trets_import_output").attr("src","ajax.php?action=addon_transparentRETS_loadRETS" + t_modifiers);  
										$("#trets_import_output").on("load", function () {
											$("#thinker").replaceWith("Import Completed for RETS class ID#"+class_id+" - <b>"+visible_name+"<b>");
											document.getElementById("trets_import_output").contentWindow.scrollTo(0,document.getElementById("trets_import_output").contentWindow.document.body.scrollHeight);
										}); 
									return false;
									}
								
									function confirmlong(longtask){
										var importwindow = "' . $importwindow . '";
										$("#processing").empty();
										var t_modifiers = "";
										var insert_html ="";
										var in_use ="";
										
										var update_warning = [
				        						"",
												"This will perform a regular incremental import of listings and photos for all configured classes.",
												"<br /><br />", 
												"This is the same update import normally triggered by CRON once per day or more frequently if necessary."
											].join("");	
										
										var long_warning = [
				        						"",
												"This can require a LOT of time and if your server is not correctly configured for sufficient memory, or to allow long-running processes, your import will not complete. This can leave your listing database in an unpredictable or corrupted state.",
												"<br /><br />", 
												"ONLY click OK If you are 100% certain your server will allow for significant memory use and long running PHP and mySQL processes."
											].join("");
											
										var meta_warning = [
				        						"",
												"This will force an update to all RETS metadata for all configured classes, and then perform an update import.",
												"<br /><br />", 
												"Use this only if your RETS server is not compliant and not updating the metadata version when metadata updates are performed, or if instructed by support."
											].join("");	
										
										switch (longtask) {
											case 1:
												t_modifiers = "";
												insert_html = update_warning;
												break;
											case 2:
												t_modifiers = "&force_metadataupdate=1";
												insert_html = meta_warning;
												break;
											case 3:
												t_modifiers = "&listing_last_update=0&listingupdate_only=1";
												insert_html = long_warning;
												break;
											case 4:
												t_modifiers = "&photo_last_update=0";
												insert_html = long_warning;
												break;
											default:
												break;
										}
										if (t_modifiers ==""){
										 	in_use = "No import modifier, incremental update";
										}
										else {
										
											in_use = "<div>Using import modifier: <span style=\"font-weight: bold; font-size: 1em;\">"+t_modifiers+"</span></div>";
										}
                                        insert_html +=  ["<div>",
                                            "<button id=\"ok_long_warning\" type=\"button\" class=\"btn btn-primary btn-sm\" aria-label=\"Close\">Ok</button>",
                                            "<button type=\"button\" class=\"btn btn-danger btn-sm\" data-bs-dismiss=\"modal\" aria-label=\"Close\">Cancel</button>",
                                            "</div>"].join("");
                                        raiseModal("Warning", insert_html);
                                        $("#customizeModal #ok_long_warning").click(function (e) {  
                                            e.preventDefault();
                                            var t_task = [ "<div id=\"thinker\" class=\"spinner-border text-primary\" role=\"status\"> <span class=\"sr-only\">Loading...</span> </div>",
                                                                "&nbsp;"+in_use
                                                            ].join("");
                                            
                                            var html = $("#dialog-import").html();
                                            raiseModal("RETS Server "+' . $server_id . '+"- Import Activity", html);
                                            
                                            $("#customizeModal #processing").html(t_task);
                                            $("#customizeModal #import-message_text").html(importwindow);
                                
                                            $("#trets_import_output").attr("src","ajax.php?action=addon_transparentRETS_loadRETS" + t_modifiers);  
                                            $("#trets_import_output").on("load", function () {
                                                $("#thinker").replaceWith("Import Completed for ' . $num . ' RETS classes");
                                                document.getElementById("trets_import_output").contentWindow.scrollTo(0,document.getElementById("trets_import_output").contentWindow.document.body.scrollHeight);
                                            }); 
                                        });
										return false;
									}
										
                                    
								</script>';
                } else {
                    //$display.='<form action="index.php?server_id='.$_GET['server_id'].'&amp;action=addon_transparentRETS_config_server" method="post"><fieldset><input type="hidden" name="detectClass" value="1" /><input type="submit" value="Get class information from RETS server" /></fieldset></form>';

                    $display .= '<div id="detectClassDiv">
								<form action="#" id="detectResourcesForm" method="post">
									<fieldset>
									<input type="hidden" name="detectClass" value="1" />
									<input type="button" class="trets_std_button" onclick="RundetectRETSClasses();return false;" value="Get class information from RETS server" />
									</fieldset>
								</form>
							</div>';
                    $display .= '<script type="text/javascript">
									function RundetectRETSClasses(){
										ShowPleaseWait();
											$("#detectClassDiv").load("ajax.php?action=addon_transparentRETS_detectRETSClasses&server_id=' . $_GET['server_id'] . '",function(){
											HidePleaseWait();
										});
									};
								</script>';
                }
            } else {
                $display .= '<div class="rets_error">You need to select a resource on the previous tab</div>';
            }
        } else {
            $display .= '<div class="rets_error">You need to select a server</div>';
        }

        return $display;
    }

    public function config_servers_resource_screen()
    {
        global  $config;
        $display = '';

        if (isset($_GET['server_id']) && is_numeric($_GET['server_id'])) {

            //Check to see if we already know the servers resources
            $sql = 'SELECT resource_name, resource_id,resource_property 
				FROM transparentrets_resources 
				WHERE account_id = ' . $_GET['server_id'];
            $resources = $this->conn->Execute($sql);
            if ($resources->RecordCount() > 0) {
                $display .= '<form action="index.php?server_id=' . $_GET['server_id'] . '&amp;action=addon_transparentRETS_config_server" id="resource_config_form" method="post">
							<fieldset>
								<label onmouseover="domTT_activate(this, event, \'caption\', \'Property Resource\', \'content\',\'Select the &quot;Property&quot; Resource from your RETS Server. This should be automatically selected for you if the server is RETS compliant.\', \'trail\', \'x\');" for="id_prop_resource">
									Select the Property Resource:
								</label>
								<select id="id_prop_resource" name="prop_resource">
									<option value="">Select A Resource</option>';

                while (!$resources->EOF) {
                    $display .= '<option value="' . $resources->fields['resource_id'] . '"';

                    if ($resources->fields['resource_property'] == true) {
                        $display .= ' selected="selected" ';
                    }

                    $display .= '>' . $resources->fields['resource_name'] . '</option>';
                    $resources->MoveNext();
                }
                $display .= '	</select>
								<input type="button" class="trets_std_button" onclick="save_resource_config();return false;" value="Save Resource" />
							</fieldset>
						</form>';
                $display .= '<script type="text/javascript">
							
							function save_resource_config(){
							ShowPleaseWait();
								$.getJSON("ajax.php?action=addon_transparentRETS_save_resource_config&server_id=' . $_GET['server_id'] . '&prop_resource="+$("#id_prop_resource").val(), function(data) {
									if(data.error == "1"){
										status_error(data.error_msg);
									}else{
										status_msg(data.status_msg);

									}
									HidePleaseWait();
								});
							};
							
							</script>';
            } else {
                $display .= '<div id="detectResourcesDiv">
								<form action="#" id="detectResourcesForm" method="post">
									<fieldset>
										<input type="hidden" name="detectRETSResources" value="1" />
										<input type="button" class="trets_std_button" onclick="RundetectRETSResources();return false;" value="Detect RETS Server Resources" />
									</fieldset>
								</form>
							</div>';
                $display .= '
					<script type="text/javascript">
						
						function RundetectRETSResources(){
							ShowPleaseWait();
							$("#detectResourcesDiv").load("ajax.php?action=addon_transparentRETS_detectRETSResources&server_id=' . $_GET['server_id'] . '",function(){
							HidePleaseWait();
							});

						};
						
					</script>';
            }
        } else {
            $display .= '<div class="rets_error">You need to select a server</div>';
        }

        return $display;
    }

    public function update_class_settings($id, $import, $import_class)
    {
        global $config;
        $sql = 'UPDATE transparentrets_classes 
			SET class_import = ' . intval($import) . ', 
				class_id_import = ' . intval($import_class) . ' 
			WHERE class_id = ' . intval($id);
        $class = $this->conn->Execute($sql);

        if (!$class) {
            $this->log_error($sql);
        }
    }

    public function config_imports()
    {
        global $config;
        $display = '';

        if (isset($_GET['class_id']) && is_numeric($_GET['class_id'])) {
            $class_id = $_GET['class_id'];
        } else {
            $class_id = '';
        }
        //Save Basic Config if posted.
        if (isset($_POST['update_settings_class_id'])) {
            $this->update_class_settings($_POST['update_settings_class_id'], $_POST['IMPORT_CLASS'], $_POST['RETS_Open-Realty_Class']);
            $display .= '
				<script type="text/javascript">
				status_msg("Server Updated");
				</script>';
        }

        if (isset($_GET['p']) || isset($_POST['detectmetadata'])) {
            if (isset($_GET['p'])) {
                $pane_view = $_GET['p'];
            } elseif (isset($_POST['detectmetadata'])) {
                $pane_view = 'FM';
            }

            $output = '';

            switch ($pane_view) {
                case 'CS':
                    //pane #1 Class Settings
                    if (isset($class_id) && is_numeric($class_id)) {
                        $sql = "SELECT * FROM transparentrets_classes WHERE class_id = " . $class_id;
                        $classes = $this->conn->Execute($sql);

                        if (!$classes) {
                            $this->log_error($sql);
                        }
                        if ($classes->RecordCount() == 1) {
                            $output .= $this->config_class_import_screen(
                                $classes->fields['class_id'],
                                $classes->fields['class_name'],
                                $classes->fields['class_import'],
                                $classes->fields['resource_id'],
                                $classes->fields['class_id_import']
                            );
                        } else {
                            $output .= '<div class="rets_error">Class Not Found</div>' . BR;
                        }
                    } else {
                        $output .= '<div class="rets_info">Select a RETS class</div>' . BR;
                    }
                    break;

                case 'FM':
                    //pane #2 Field Map
                    if (isset($class_id) && is_numeric($class_id)) {
                        $sql = "SELECT * 
							FROM transparentrets_classes 
							WHERE class_id = " . $class_id;
                        $classes = $this->conn->Execute($sql);

                        if (!$classes) {
                            $this->log_error($sql);
                        }
                        if ($classes->RecordCount() == 1 && $classes->fields['class_import'] == 1 && $classes->fields['class_id_import'] != '') {
                            //Check to see if we should get metadata
                            //Check to see if metadata was retrieved.
                            $sql = 'SELECT * 
								FROM transparentrets_metadata 
								WHERE class_id = ' . $class_id;
                            $metadata = $this->conn->Execute($sql);

                            if (!$metadata) {
                                $this->log_error($sql);
                            }
                            if ($metadata->RecordCount() > 0) {
                                if (isset($_POST['automap_or_fields_existing'])    && $_POST['automap_or_fields_existing'] == 1) {
                                    //$output.=$this->autocreate_orfields($_GET['class_id'], FALSE);
                                    //$this->flag_class_map_modified($_GET['class_id']);
                                } elseif (isset($_GET['autocreate']) && is_numeric($_GET['autocreate'])) {
                                    //		$output.=$this->autocreate_orfield($_GET['autocreate']);
                                    //		$this->flag_class_map_modified($_GET['class_id']);
                                } elseif (isset($_GET['updatefield']) && is_numeric($_GET['updatefield'])) {
                                } elseif (isset($_POST['multiselect'])) {
                                    foreach ($_POST['multiselect'] as $acfield) {
                                        //			$output.=$this->autocreate_orfield($acfield);
                                        //			$this->flag_class_map_modified($_GET['class_id']);
                                    }
                                }
                                //Metadata Found show field map
                                $output .= $this->show_field_map($_GET['class_id']);
                            } else {
                                $output .= '<form action="index.php?class_id=' . $class_id . '&amp;action=addon_transparentRETS_config_imports" method="post">
											<input type="hidden" value="1" name="detectmetadata" />
											<input type="button" class="trets_std_button" onclick="RunDetectMetaData();return false;" value="Get Metadata from server" />
										</form>
										<script type="text/javascript">
											function RunDetectMetaData(myclass,field){
												ShowPleaseWait();
												$.ajax({
											        url: "ajax.php?action=addon_transparentRETS_check_metadata_updates",
											        data: {
											            resource_id: ' . $classes->fields['resource_id'] . ',
											            class_id:  ' . $classes->fields['class_id'] . '
											        },
											        type: "GET",
											        dataType: "html",
											        success: function (data) {
											            $("#ui-id-2").html(data);
											        },
											        error: function (xhr, status) {
											            alert("Sorry, there was a problem!");
											            HidePleaseWait(); 
											        },
											        complete: function (xhr, status) {
											        	HidePleaseWait(); 
											        }
											    });
											};
										</script>';
                            }
                        } else {
                            $output .= '<div class="rets_error">Select a class marked for Import</div>';
                        }
                    } else {
                        $output .= '<div class="rets_info">Select a RETS class</div>';
                    }
                    break;

                case 'IC':
                    //pane #3 Import Criteria
                    if (isset($_GET['class_id']) && is_numeric($_GET['class_id'])) {
                        $sql = "SELECT * 
							FROM transparentrets_classes 
							WHERE class_id = " . $_GET['class_id'];
                        $classes = $this->conn->Execute($sql);

                        if (!$classes) {
                            $this->log_error($sql);
                        }
                        if ($classes->RecordCount() == 1 && $classes->fields['class_import'] == 1 && $classes->fields['class_id_import'] != '') {
                            //Check to see if metadata was retrieved.
                            $sql = 'SELECT * 
								FROM transparentrets_metadata 
								WHERE class_id = ' . $_GET['class_id'];
                            $metadata = $this->conn->Execute($sql);

                            if (!$metadata) {
                                $this->log_error($sql);
                            }
                            if ($metadata->RecordCount() > 0) {
                                //if (isset($_POST['save_search']))
                                //{
                                //	$output.=$this->save_search_criteria($_GET['class_id']);
                                //}
                                //Metadata Found show field map
                                $output .= $this->show_search_criteria($_GET['class_id']);
                            } else {
                                $output .= 'You need to get MetaData First';
                            }
                        } else {
                            $output .= '<div class="rets_error">Select a class marked for Import</div>';
                        }
                    } else {
                        $output .= '<div class="rets_info">Select a RETS class</div>';
                    }
                    break;

                case 'UC':
                    //pane #4 Update Criteria
                    if (isset($_GET['class_id']) && is_numeric($_GET['class_id'])) {
                        $sql = "SELECT * 
							FROM transparentrets_classes 
							WHERE class_id = " . $_GET['class_id'];
                        $classes = $this->conn->Execute($sql);

                        if (!$classes) {
                            $this->log_error($sql);
                        }
                        if ($classes->RecordCount() == 1 && $classes->fields['class_import'] == 1 && $classes->fields['class_id_import'] != '') {
                            //Check to see if metadata was retrieved.
                            $sql = 'SELECT * FROM transparentrets_metadata 
								WHERE class_id = ' . $_GET['class_id'];
                            $metadata = $this->conn->Execute($sql);

                            if (!$metadata) {
                                $this->log_error($sql);
                            }
                            if ($metadata->RecordCount() > 0) {
                                //Metadata Found show field map
                                $output .= $this->show_update_criteria($_GET['class_id']);
                            } else {
                                $output .= 'You need to get metadata first.';
                            }
                        } else {
                            $output .= '<div class="rets_error">Select a class markd for Import</div>' . BR;
                        }
                    } else {
                        $output .= '<div class="rets_info">Select a RETS class</div>' . BR;
                    }
                    break;
                case 'DR':
                    $output = '<div>' . $this->debug_transaction($_GET['class_id']) . '</div>';
                    break;
                default:

                    break;
            }
            echo $output;
            die;
        }
        $display .= '<script type="text/javascript">
        $(document).ready(function () {
    
            //Enable ajax based tags
            $(\'[data-bs-toggle="tabajax"]\').click(function (e) {
                e.preventDefault();
                var $this = $(this),
                    loadurl = $this.attr("href"),
                    targ = $this.attr("data-bs-target");
                //if ($(targ).html() == "") {
                    $.get(loadurl, function (data) {
                        $(targ).html(data);
                    });
                //}
                $this.tab("show");
            });
            $("#CS-tab").click();
        });
        </script>';

        $display .= '<div class="card card-frame mb-4">
        <div class="card-body py-2">';

        $display .= $this->config_servers_show_class_select();

        $display .= BR;

        $display .= '<div class="nav-wrapper position-relative end-0">
        <ul
         class="nav nav-pills nav-fill p-1"
         id="nav-tab"
         role="tablist"
        >

            <li class="nav-item"><a
                 class="nav-link mb-0 px-0 py-1 active"
                 id="CS-tab"
                 data-bs-toggle="tabajax"
                 data-bs-target="#CS"
                 href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=CS&amp;class_id=' . $class_id . '"
                 role="tab"
                 aria-controls="CS"
                 aria-selected="true"
                >Class Settings</a>
            </li>
                <li class="nav-item"><a
                class="nav-link mb-0 px-0 py-1"
                id="FM-tab"
                data-bs-toggle="tabajax"
                data-bs-target="#FM"
                href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=FM&amp;class_id=' . $class_id . '"
                role="tab"
                aria-controls="FM"
                aria-selected="false"
                >Field Map</a>
            </li>
            </li>
                <li class="nav-item"><a
                class="nav-link mb-0 px-0 py-1"
                id="IC-tab"
                data-bs-toggle="tabajax"
                data-bs-target="#IC"
                href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=IC&amp;class_id=' . $class_id . '"
                role="tab"
                aria-controls="IC"
                aria-selected="false"
                >Import Criteria</a>
            </li>
            </li>
                <li class="nav-item"><a
                class="nav-link mb-0 px-0 py-1"
                id="UC-tab"
                data-bs-toggle="tabajax"
                data-bs-target="#UC"
                href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=UC&amp;class_id=' . $class_id . '"
                role="tab"
                aria-controls="UC"
                aria-selected="false"
                >Update Criteria</a>
            </li>  
            </li>
                <li class="nav-item"><a
                class="nav-link mb-0 px-0 py-1"
                id="DR-tab"
                data-bs-toggle="tabajax"
                data-bs-target="#DR"
                href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=DR&amp;class_id=' . $class_id . '"
                role="tab"
                aria-controls="DR"
                aria-selected="false"
                >Debug RETS</a>
            </li>  
        </ul>
        </div>
		<div
		 class="tab-content"
		 id="nav-tabContent"
		>
			<div
			 class="tab-pane fade show active"
			 id="CS"
			 role="tabpanel"
			 aria-labelledby="CS-tab"
			></div>
			<div
			 class="tab-pane fade"
			 id="FM"
			 role="tabpanel"
			 aria-labelledby="FM-tab"
			></div>
            <div
			 class="tab-pane fade"
			 id="IC"
			 role="tabpanel"
			 aria-labelledby="IC-tab"
			></div>
            <div
			 class="tab-pane fade"
			 id="UC"
			 role="tabpanel"
			 aria-labelledby="UC-tab"
			></div>
            <div
			 class="tab-pane fade"
			 id="DR"
			 role="tabpanel"
			 aria-labelledby="DR-tab"
			></div>
		</div>';

        // $display .= '<div id="css-tabs">
        // 			<ul>' . BR;

        // $display .= '<li><a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=CS&amp;class_id=' . $class_id . '" id ="CS">Class Settings</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=FM&amp;class_id=' . $class_id . '" id ="FM">Field Map</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=IC&amp;class_id=' . $class_id . '" id ="IC">Import Criteria</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=UC&amp;class_id=' . $class_id . '" id ="UC">Update Criteria</a></li>
        // 			<li><a href="' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=DR&amp;class_id=' . $class_id . '" id ="DR">Debug RETS</a></li>' . BR;

        // $display .= '</ul>' . BR;

        //End Tabs
        // $display .= '</div>' . BR . '<!--END -->' . BR;

        $display .= '</div></div>';
        return $display;
    }

    public function save_listing_title($class, $title)
    {
        global $config;
        $display = '';
        $sql = 'UPDATE transparentrets_classes SET listing_title = ' . $this->conn->qstr($title)
            . ' WHERE class_id = ' . intval($class);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }
        header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Listing Title Saved']);
        return $display;
    }

    public function save_default_agentid($class, $agent_id)
    {
        global $config;
        $display = '';
        $sql = 'UPDATE transparentrets_classes SET default_agent = ' . intval($agent_id)
            . ' WHERE class_id = ' . intval($class);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }
        header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Default Agent Saved']);
        return $display;
    }

    public function save_agentid_field($class, $metadata_id)
    {
        global $config;
        $display = '';
        $sql = 'UPDATE transparentrets_metadata SET metadata_agentid_field = 0 WHERE class_id = ' . intval(
            $class
        );
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        $sql = 'UPDATE transparentrets_metadata SET metadata_agentid_field = 1 WHERE metadata_id = '
            . intval($metadata_id);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }
        header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Agent ID Field Saved']);
        return $display;
    }

    public function save_photoupdate_field($class, $metadata_id)
    {
        global  $config;
        $display = '';
        $sql = 'UPDATE transparentrets_metadata SET metadata_photoupdate_field = 0 WHERE class_id = '
            . intval($class);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }
        if ($metadata_id > 0) {
            $sql = 'UPDATE transparentrets_metadata SET metadata_photoupdate_field = 1 WHERE metadata_id = '
                . intval($metadata_id);
            $recordSet = $this->conn->Execute($sql);
            if (!$recordSet) {
                $this->log_error($sql);
            }
        }

        header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Photo Last Modified Field Saved']);
        return $display;
    }

    public function save_listingupdate_field($class, $metadata_id)
    {
        global $config;
        $display = '';
        $sql = 'UPDATE transparentrets_metadata SET metadata_listingsupdate_field = 0 WHERE class_id = '
            . intval($class);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        $sql =
            'UPDATE transparentrets_metadata SET metadata_listingsupdate_field = 1 WHERE metadata_id = '
            . intval($metadata_id);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }
        header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Listing Last Modified Field Saved']);
        return $display;
    }
    // OR Specific (can be changed)
    public function show_update_criteria($class)
    {
        global $config;
        $display = '';
        //Get update and agent Id fileds.
        $listingupdate_field = '';
        $photoupdate_field = '';
        $agentid_field = '';
        $fields = [];
        $datetime_fields = [];
        $sql = 'SELECT metadata_id,metadata_fieldname,metadata_longname,metadata_listingsupdate_field,metadata_photoupdate_field,metadata_agentid_field,metadata_fieldtype 
				FROM transparentrets_metadata 
				WHERE class_id=' . intval($class);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        while (!$recordSet->EOF) {
            if ($recordSet->fields('metadata_listingsupdate_field') == 1) {
                $listingupdate_field = $recordSet->fields('metadata_id');
            }

            if ($recordSet->fields('metadata_photoupdate_field') == 1) {
                $photoupdate_field = $recordSet->fields('metadata_id');
            }

            if ($recordSet->fields('metadata_agentid_field') == 1) {
                $agentid_field = $recordSet->fields('metadata_id');
            }

            if ($recordSet->fields('metadata_fieldtype') == 'DateTime' || $recordSet->fields('metadata_fieldtype') == 'Date') {
                $datetime_fields[$recordSet->fields('metadata_id')]
                    = $recordSet->fields('metadata_fieldname') . ' (' . $recordSet->fields('metadata_longname') . ')';
            }

            $fields[$recordSet->fields('metadata_id')] = $recordSet->fields('metadata_fieldname') . ' (' . $recordSet->fields('metadata_longname') . ')';
            $recordSet->MoveNext();
        }

        asort($fields);
        asort($datetime_fields);
        $display .= '<div style="padding: 0px 10px;">The following settings are NOT optional.</div>';
        $display .= "\r\n" . '<form id="form_listingupdate_field" action="index.php?class_id=' . $class . '&amp;action=addon_transparentRETS_config_imports" method="post">
								<fieldset>
									<table width="100%">
										<tr>
											<td style="width:20%">
												<label onmouseover="domTT_activate(this, event, \'caption\', \'Listing Update Field\', \'content\',\'This is the field on the RETS Server which holds the DateTime value of when the listing data was last modified.\', \'trail\', \'x\');" for="id_listingupdate_field">
													Listing Last Modified:
												</label>
											</td>
											<td style="width:80%;">
												<input type="button" class="trets_std_button" onclick="save_listingupdate_field();return false;" value="Save" />
												<select name="listingupdate_field" id="id_listingupdate_field">
													<option value="">Select a field</option>';

        foreach ($datetime_fields as $id => $fv) {
            if ($id == $listingupdate_field) {
                $display .= '<option value="' . $id . '" selected="selected">' . $fv . '</option>';
            } else {
                $display .= '<option value="' . $id . '">' . $fv . '</option>';
            }
        }

        $display .= '							</select>
											</td>
										</tr>
									</table>
								</fieldset>
							</form>' . "\r\n";
        //Photo modified
        $display .= '<form id="form_photoupdate_field" action="index.php?class_id=' . $class . '&amp;action=addon_transparentRETS_config_imports" method="post">
						<fieldset>
							<table width="100%">
								<tr>
									<td style="width:20%;">
										<label onmouseover="domTT_activate(this, event, \'caption\', \'Photo Update Field\', \'content\',\'This is the field on the RETS Server which holds the DateTime value of when the listing\\\'s were last modified. Not all MLS or RETS Server Vendors Use this field.\', \'trail\', \'x\');" for="id_photoupdate_field">
											Photo Last Modified:
										</label>
									</td>
									<td style="width:80%;">
										<input type="button" class="trets_std_button" onclick="save_photoupdate_field();return false;"value="Save" />
										<select name="photoupdate_field" id="id_photoupdate_field">
											<option value="">No Photo Update Field Avaliable</option>';

        foreach ($datetime_fields as $id => $fv) {
            if ($id == $photoupdate_field) {
                $display .= '<option value="' . $id . '" selected="selected">' . $fv . '</option>';
            } else {
                $display .= '<option value="' . $id . '">' . $fv . '</option>';
            }
        }

        $display .= '					</select>
									</td>
								</tr>
							</table>
						</fieldset>
					</form>' . "\r\n";
        //Agent ID
        $display .= '<form id="form_agentid_field" action="index.php?class_id=' . $class . '&amp;action=addon_transparentRETS_config_imports" method="post">
						<fieldset>
						<table width="100%">
						<tr>
							<td style="width:20%;">
								<label onmouseover="domTT_activate(this, event, \'caption\', \'Agent ID Field\', \'content\',\'This is the field on the RETS Server which holds the Agents MLS ID. This field is used to map listings to Open-Realty&reg; users. Create a field in the user template in Open-Realty called &quot;RETS_AGENT_ID&quot; and enter your MLS ID.\', \'trail\', \'x\');" for="id_agentid_field">
									Agent ID:
								</label>
							</td>
							<td style="width:80%;">
								<input type="button" class="trets_std_button" onclick="save_agentid_field();return false;" value="Save" /> 
								<select name="agentid_field" id="id_agentid_field">
									<option value="">Select a field</option>';

        foreach ($fields as $id => $fv) {
            if ($id == $agentid_field) {
                $display .= '<option value="' . $id . '" selected="selected">' . $fv . '</option>';
            } else {
                $display .= '<option value="' . $id . '">' . $fv . '</option>';
            }
        }

        $display .= '			</select>
							</td>
						</tr>
					</table>
				</fieldset>
			</form>' . "\r\n";
        //Get Default Agent/Admin ID
        $agents = [];
        $sql = 'SELECT userdb_user_name,userdb_id 
					FROM ' . $config['table_prefix']
            . 'userdb WHERE userdb_is_agent = \'yes\' OR userdb_is_admin = \'yes\'';
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        while (!$recordSet->EOF) {
            $agents[$recordSet->fields('userdb_id')] = $recordSet->fields('userdb_user_name');
            $recordSet->MoveNext();
        }

        $sql = 'SELECT default_agent,listing_title 
				FROM transparentrets_classes 
				WHERE class_id = '
            . intval($class);
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        $default_agent = $recordSet->fields('default_agent');
        $display .= '<form id="form_default_agentid" action="index.php?class_id=' . $class . '&amp;action=addon_transparentRETS_config_imports" method="post">
						<fieldset>
							<table width="100%">
								<tr>
									<td style="width:20%;">
										<label onmouseover="domTT_activate(this, event, \'caption\', \'Default Open-Realty&reg; Agent\', \'content\',\'This is the default user in Open-Realty&reg; to map listings to when no RETS_AGENT_ID in Open-Realty can be matched to this listings Agent ID on the RETS Server.\', \'trail\', \'x\');" for="id_default_agentid">
											Default Agent:
										</label>
									</td>
									<td style="width:80%;">
										<input type="button" class="trets_std_button" onclick="save_default_agentid();return false;" value="Save" />
										<select name="default_agentid" id="id_default_agentid">
											<option value="">Select a field</option>';

        foreach ($agents as $id => $fv) {
            if ($id == $default_agent) {
                $display .= '<option value="' . $id . '" selected="selected">' . $fv . '</option>';
            } else {
                $display .= '<option value="' . $id . '">' . $fv . '</option>';
            }
        }

        $display .= '					</select>
									</td>
								</tr>
							</table>
						</fieldset>
					</form>' . "\r\n";

        //Show Listing Title
        $default_agent = $recordSet->fields('listing_title');
        $display .= '<form id="form_listing_title" action="index.php?class_id=' . $class . '&amp;action=addon_transparentRETS_config_imports" method="post">
						<fieldset>
							<table width="100%">
								<tr>
									<td style="width:20%;">
										<label onmouseover="domTT_activate(this, event, \'caption\', \'Listing Title\', \'content\',\'This is the Title that will be assigned to the listings in Open-Realty&reg;. You can use RETS Fields in your title by inclosing the EXACT fieldname in  brackets {}. Example &quot;Listing #{MLNumber}&quot;.\', \'trail\', \'x\');" for="id_listing_title">
											Listing Title:
										</label>
									</td>
									<td style="width:80%;">
										<input type="button"  class="trets_std_button" onclick="save_listing_title();return false;" value="Save" />
										<input name="listing_title" id="id_listing_title" type="text" value="' . $default_agent . '" />
									</td>
									
								</tr>
							</table>
						</fieldset>
					</form>' . "\r\n";

        $display .= '<script type="text/javascript">
				function save_listingupdate_field(){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_save_listingupdate_field&class_id=' . $class . '&listingupdate_field="+$("#id_listingupdate_field").val(), function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);

						}
						HidePleaseWait();
					});

				};
				function save_photoupdate_field(){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_save_photoupdate_field&class_id=' . $class . '&photoupdate_field="+$("#id_photoupdate_field").val(), function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);

						}
						HidePleaseWait();
					});

				};
				function save_agentid_field(){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_save_agentid_field&class_id=' . $class . '&agentid_field="+$("#id_agentid_field").val(), function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);

						}
						HidePleaseWait();
					});

				};
				function save_default_agentid(){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_save_default_agentid&class_id=' . $class . '&default_agentid="+$("#id_default_agentid").val(), function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);

						}
						HidePleaseWait();
					});

				};
				function save_listing_title(){
					ShowPleaseWait();
					$.post("ajax.php?action=addon_transparentRETS_save_listing_title",{
						class_id: ' . $class . ', 
						listing_title: $("#id_listing_title").val()
					},
					function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);
						}
						HidePleaseWait();
					},"json");
				};
			</script>';
        return $display;
    }

    public function get_metadata_orfield($metadata_id)
    {
        global $config;
        $field = 0;

        if (intval($metadata_id) > 0) {
            $sql = 'SELECT metadata_orfield FROM  transparentrets_metadata WHERE metadata_id = ' . intval($metadata_id);
            $recordSet = $this->conn->Execute($sql);

            if (!$recordSet) {
                $this->log_error($sql);
            }
            $field = $recordSet->fields('metadata_orfield');
        }

        return $field;
    }

    public function set_metadata_orfield($metadata_id, $orfield_id)
    {
        global $config;
        $display = '';

        if (intval($metadata_id) > 0) {
            $sql = 'UPDATE transparentrets_metadata SET metadata_orfield = ' . intval($orfield_id)
                . ' WHERE metadata_id = ' . intval($metadata_id);
            $recordSet = $this->conn->Execute($sql);

            if (!$recordSet) {
                $this->log_error($sql);
            }
            $display .= '<div class="rets_info">Field Saved<div>';
        }

        return $display;
    }

    public function autocreate_orfields($class_id, $add_all = false)
    {
        global $config, $api;
        $display = '';
        $sql = "SELECT class_id, metadata_fieldname, metadata_longname,metadata_shortname,metadata_fieldtype,metadata_lookupname,metadata_interpretation,metadata_id 
			FROM transparentrets_metadata 
			WHERE class_id = "
            . intval($class_id);
        $frets = $this->conn->Execute($sql);

        if (!$frets) {
            $this->log_error($sql);
        }

        while (!$frets->EOF) {
            //Get OR Pclass
            $sql =
                'SELECT class_id_import, resource_id 
				FROM transparentrets_classes 
				WHERE class_id = ' . $frets->fields['class_id'];
            $orc = $this->conn->Execute($sql);

            if (!$orc) {
                $this->log_error($sql);
            }

            if ($orc->RecordCount() != 1) {
                $display .= '<Improper Open-Realty property class settings detected, aborting autocreate.';
            }

            //Create Field
            $edit_field = $frets->fields['metadata_fieldname'];

            switch ($frets->fields['metadata_fieldtype']) {
                case 'Int':
                case 'Tiny':
                case 'Small':
                case 'Long':
                    $field_type = 'number';
                    break;
                case 'Decimal':
                    $field_type = 'decimal';
                    break;
                case 'Date':
                    $field_type = 'date';
                    break;
                case 'Boolean':
                    $field_type = 'checkbox';
                    break;
                default:
                    $field_type = 'text';
                    break;
            }

            switch ($frets->fields['metadata_interpretation']) {
                case 'Currency':
                    $field_type = 'price';
                    break;
                case 'Lookup':
                    $field_type = 'select';
                    break;
                case 'LookupMulti':
                case 'LookupBitstring':
                case 'LookupBitmask':
                    $field_type = 'select-multiple';
                    break;
            }

            if (!empty($frets->fields['metadata_longname'])) {
                $field_caption = addslashes($frets->fields['metadata_longname']);
                $search_label = addslashes($frets->fields['metadata_longname']);
            } elseif (!empty($frets->fields['metadata_shortname'])) {
                $field_caption = addslashes($frets->fields['metadata_shortname']);
                $search_label = addslashes($frets->fields['metadata_shortname']);
            } else {
                $field_caption = addslashes($frets->fields['metadata_fieldname']);
                $search_label = addslashes($frets->fields['metadata_fieldname']);
            }

            //Get Lookup Elements
            if ($field_type == 'select-multiple' || $field_type == 'select') {
                $sql =
                    'SELECT metadata_longvalue,metadata_shortvalue FROM transparentrets_lookuptype WHERE metadata_lookupname = '
                    . $this->conn->qstr($frets->fields['metadata_lookupname']) . ' AND resource_id = '
                    . $orc->fields['resource_id'];
                $recordSet = $this->conn->Execute($sql);

                if (!$recordSet) {
                    $this->log_error($sql);
                }

                $field_elms = [];

                while (!$recordSet->EOF) {
                    $field_elms[] = $recordSet->fields('metadata_longvalue');
                    $recordSet->MoveNext();
                }
                $field_elements = array_unique($field_elms);
            } else {
                $field_elements = [];
            }

            //Check to see if field already exist
            $sql = 'SELECT listingsformelements_id FROM ' . $config['table_prefix']
                . 'listingsformelements WHERE listingsformelements_field_name = ' . $this->conn->qstr(
                    $edit_field
                );
            $recordSet = $this->conn->Execute($sql);

            if (!$recordSet) {
                $this->log_error($sql);
            }

            $num = $recordSet->RecordCount();
            $field_id = 0;

            if ($num > 0) {
                $field_id = $recordSet->fields('listingsformelements_id');

                $result = $api->load_local_api(
                    'fields__assign_class',
                    ['field_id' => $field_id, 'class' => $orc->fields['class_id_import']]
                );
            } elseif ($add_all) {
                $result = $api->load_local_api(
                    'fields__create',
                    [
                        'resource' => 'listing', 'class' => [$orc->fields['class_id_import']], 'field_type' => $field_type, 'field_name' => $edit_field, 'field_caption' => $field_caption, 'default_text' => '',
                        'field_elements' => $field_elements, 'rank' => 0, 'search_rank' => 0, 'search_result_rank' => 0, 'required' => false, 'location' => 'center', 'display_on_browse' => false,
                        'search_step' => 0, 'display_priv' => 0, 'field_length' => 0, 'tool_tip' => '', 'search_label' => $search_label, 'search_type' => '', 'searchable' => false,
                    ]
                );
                $field_id = $result['field_id'];
            }

            if ($field_id > 0) {
                $sql = 'UPDATE transparentrets_metadata SET metadata_orfield = ' . intval($field_id)
                    . ' WHERE metadata_id = ' . intval($frets->fields['metadata_id']);
                $recordSet = $this->conn->Execute($sql);
                if (!$recordSet) {
                    $this->log_error($sql);
                }
            }
            $frets->MoveNext();
        }
        header('Content-type: application/json');
        return json_encode(['error' => "0", 'status_msg' => 'Fields Mapped.']);
    }

    public function autocreate_orfield($metadata_id)
    {
        global $config, $api;
        $display = '';

        $sql = "SELECT class_id, metadata_fieldname, metadata_longname,metadata_shortname,metadata_fieldtype,metadata_lookupname,metadata_interpretation 
			FROM transparentrets_metadata 
			WHERE metadata_id = "
            . intval($metadata_id);
        $frets = $this->conn->Execute($sql);

        if (!$frets) {
            $this->log_error($sql);
        }

        if ($frets->RecordCount() != 1) {
            header('Content-type: application/json');
            $display .= json_encode(['error' => "1", 'error_msg' => 'Improper field settings detected, aborting autocreate']);
        } else {
            //Get OR Pclass
            $sql = 'SELECT class_id_import, resource_id 
					FROM transparentrets_classes 
					WHERE class_id = ' . $frets->fields['class_id'];
            $orc = $this->conn->Execute($sql);

            if (!$orc) {
                $this->log_error($sql);
            }

            if ($orc->RecordCount() != 1) {
                header('Content-type: application/json');
                $display .= json_encode(['error' => "1", 'error_msg' => 'Improper Open-Realty property class settings detected, aborting autocreate']);
            }
            $edit_field = $frets->fields['metadata_fieldname'];

            switch ($frets->fields['metadata_fieldtype']) {
                case 'Int':
                case 'Tiny':
                case 'Small':
                case 'Long':
                    $field_type = 'number';
                    break;
                case 'Decimal':
                    $field_type = 'decimal';
                    break;
                case 'Date':
                    $field_type = 'date';
                    break;
                case 'Boolean':
                    $field_type = 'checkbox';
                    break;
                default:
                    $field_type = 'text';
                    break;
            }

            switch ($frets->fields['metadata_interpretation']) {
                case 'Currency':
                    $field_type = 'price';
                    break;
                case 'Lookup':
                    $field_type = 'select';
                    break;
                case 'LookupMulti':
                case 'LookupBitstring':
                case 'LookupBitmask':
                    $field_type = 'select-multiple';
                    break;
            }

            if (!empty($frets->fields['metadata_longname'])) {
                $field_caption = addslashes($frets->fields['metadata_longname']);
                $search_label = addslashes($frets->fields['metadata_longname']);
            } elseif (!empty($frets->fields['metadata_shortname'])) {
                $field_caption = addslashes($frets->fields['metadata_shortname']);
                $search_label = addslashes($frets->fields['metadata_shortname']);
            } else {
                $field_caption = addslashes($frets->fields['metadata_fieldname']);
                $search_label = addslashes($frets->fields['metadata_fieldname']);
            }

            //Get Lookup Elements
            if ($field_type == 'select-multiple' || $field_type == 'select') {
                $sql =
                    'SELECT metadata_longvalue,metadata_shortvalue FROM transparentrets_lookuptype WHERE metadata_lookupname = '
                    . $this->conn->qstr($frets->fields['metadata_lookupname']) . ' AND resource_id = '
                    . $orc->fields['resource_id'];
                $recordSet = $this->conn->Execute($sql);

                if (!$recordSet) {
                    $this->log_error($sql);
                }

                $field_elms = [];

                while (!$recordSet->EOF) {
                    $field_elms[] = $recordSet->fields('metadata_longvalue');
                    $recordSet->MoveNext();
                }
                $field_elements = array_unique($field_elms);
            } else {
                $field_elements = [];
            }


            //Check to see if field already exist
            $sql = 'SELECT listingsformelements_id FROM ' . $config['table_prefix']
                . 'listingsformelements WHERE listingsformelements_field_name = ' . $this->conn->qstr($edit_field);
            $recordSet = $this->conn->Execute($sql);

            if (!$recordSet) {
                $this->log_error($sql);
            }

            $num = $recordSet->RecordCount();
            $field_id = 0;
            if ($num > 0) {
                $field_id = $recordSet->fields('listingsformelements_id');
                $result = $api->load_local_api(
                    'fields__assign_class',
                    ['field_id' => $field_id, 'class' => $orc->fields['class_id_import']]
                );
                if ($result['error'] != false) {
                    header('Content-type: application/json');
                    $display .= json_encode(['error' => "1", 'error_msg' => 'Field ' . $edit_field . ' (' . $field_type . ') not assigned to class.' . print_r($result, true)]);
                } else {
                    $field_id = $result['field_id'];
                    header('Content-type: application/json');
                    $display .= json_encode(['error' => "0", 'status_msg' => 'Field ' . $edit_field . ' (' . $field_type . ') assigned to class.' . print_r($result, true)]);
                }

                /*$sql=
                'SELECT classformelements_id FROM '.$config['table_prefix_no_lang'].'classformelements WHERE listingsformelements_id = '.intval($field_id).' AND class_id = '
                .intval($orc->fields['class_id_import']);
                $recordSet=$this->conn->Execute($sql);

                if (!$recordSet) {
                    $this->log_error($sql);
                }
                if ($recordSet->RecordCount() == 0) {
                    $sql=
                    'INSERT INTO '.$config['table_prefix_no_lang'].'classformelements (class_id,listingsformelements_id) VALUES ('
                    .intval($orc->fields['class_id_import']).','.intval($field_id).')';
                    $recordSet=$this->conn->Execute($sql);

                    if (!$recordSet) {
                        $this->log_error($sql);
                    }
                    header('Content-type: application/json');
                    $display .= json_encode(array('error' => "0",'status_msg' => 'Field '.$edit_field.' ('.$field_type.') added to property class.'));

                } else {
                    header('Content-type: application/json');
                    $display .= json_encode(array('error' => "1",'error_msg' => 'Field '.$edit_field.' ('.$field_type.') already part of this class.'));

                }
                */
            } else {
                $result = $api->load_local_api(
                    'fields__create',
                    [
                        'resource' => 'listing', 'class' => [$orc->fields['class_id_import']], 'field_type' => $field_type, 'field_name' => $edit_field, 'field_caption' => $field_caption, 'default_text' => '',
                        'field_elements' => $field_elements, 'rank' => 0, 'search_rank' => 0, 'search_result_rank' => 0, 'required' => false, 'location' => 'center', 'display_on_browse' => false,
                        'search_step' => 0, 'display_priv' => 0, 'field_length' => 0, 'tool_tip' => '', 'search_label' => $search_label, 'search_type' => '', 'searchable' => false,
                    ]
                );
                if ($result['error'] != false) {
                    header('Content-type: application/json');
                    $display .= json_encode(['error' => "1", 'error_msg' => 'Field ' . $edit_field . ' (' . $field_type . ') not added.']);
                } else {
                    $field_id = $result['field_id'];
                    header('Content-type: application/json');
                    $display .= json_encode(['error' => "0", 'status_msg' => 'Field ' . $edit_field . ' (' . $field_type . ') added.']);
                }
            }
            if ($field_id > 0) {
                $sql = 'UPDATE transparentrets_metadata SET metadata_orfield = ' . intval($field_id)
                    . ' WHERE metadata_id = ' . intval($metadata_id);
                $recordSet = $this->conn->Execute($sql);
                if (!$recordSet) {
                    $this->log_error($sql);
                }
            }
        }

        return $display;
    }

    public function app_field_unmap($fieldid, $classid)
    {
        global $config, $api;
        //echo 'Removing listingsformelements_id '.$fieldid.' from class '.$classid;
        //Get Field Name
        $sql = 'SELECT listingsformelements_field_name 
				FROM ' . $config['table_prefix'] . 'listingsformelements 
				WHERE listingsformelements_id = ' . $fieldid;
        $recordSet = $this->conn->Execute($sql);
        if (!$recordSet) {
            $this->log_error($sql);
        }
        $field_name = $recordSet->fields('listingsformelements_field_name');
        $api->load_local_api('log__log_create_entry', ['log_type' => 'CRIT', 'log_api_command' => 'function->delete_listing_field', 'log_message' => 'TRETS Unmapping Listing Field ' . $field_name . '(FID:' . $fieldid . ' CID:' . $classid . ')']);
        // Delete from classform elements.
        $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'classformelements 
				WHERE listingsformelements_id = ' . $fieldid . ' 
				AND class_id = ' . $classid;
        $recordSet = $this->conn->Execute($sql);
        if (!$recordSet) {
            $this->log_error($sql);
        }

        // Remove fields from any listings that are not in this class.
        $pclass_list = '';

        $sql = 'SELECT DISTINCT(listingsdb_id) 
				FROM ' . $config['table_prefix'] . 'listingsdb 
				WHERE listingsdb_pclass_id 
				IN (SELECT class_id FROM  ' . $config['table_prefix_no_lang'] . 'classformelements WHERE listingsformelements_id = ' . $fieldid . ')';
        $recordSet = $this->conn->Execute($sql);
        if (!$recordSet) {
            $this->log_error($sql);
        }
        while (!$recordSet->EOF) {
            if (empty($pclass_list)) {
                $pclass_list .= $recordSet->fields('listingsdb_id');
            } else {
                $pclass_list .= ',' . $recordSet->fields('listingsdb_id');
            }
            $recordSet->Movenext();
        }
        if ($pclass_list == '') {
            $pclass_list = 0;
        }
        $sql = 'DELETE FROM ' . $config['table_prefix'] . 'listingsdbelements 
				WHERE listingsdbelements_field_name = ' . $this->conn->qstr($field_name) . ' 
				AND listingsdb_id NOT IN (' . $pclass_list . ')';
        $recordSet = $this->conn->Execute($sql);
        if (!$recordSet) {
            $this->log_error($sql);
        }
        // Check to see if field is no longer used
        $sql = 'SELECT class_id 
				FROM ' . $config['table_prefix_no_lang'] . 'classformelements 
				WHERE listingsformelements_id = ' . $fieldid;
        $recordSet = $this->conn->Execute($sql);
        if (!$recordSet) {
            $this->log_error($sql);
        }
        $num = $recordSet->RecordCount();
        if ($num === 0) {
            $sql = 'DELETE FROM ' . $config['table_prefix'] . 'listingsformelements 
					WHERE listingsformelements_id = ' . $fieldid;
            $recordSet = $this->conn->Execute($sql);
            if (!$recordSet) {
                $this->log_error($sql);
            }
        }
    }

    public function show_field_map($class)
    {
        global $config;
        //GET Open-Realty Class ID
        $sql = "SELECT class_id_import 
			FROM transparentrets_classes 
			WHERE class_id = " . intval($class);
        $rclass = $this->conn->Execute($sql);

        if (!$rclass) {
            $this->log_error($sql);
        }

        $orclass = intval($rclass->fields['class_id_import']);
        $display = '';
        $display .= '
			<script type="text/javascript">
				$(document).ready(function(){
					setColor();
				});
				
				function setColor() {
					var color =	$("select").find("option:selected").attr("value");
					$("select").css("background", color); 
				}  

				function automap_field(myclass,field){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_automapfield&field_id="+field+"&class_id="+myclass, function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
							HidePleaseWait();
						}else{
							status_msg(data.status_msg);
							$("#ui-id-2").load("' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&p=FM&class_id="+myclass,function(){
								HidePleaseWait();
							});
						}
					});

				};
				
				function updateField(myclass,field,orfield){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_updateField&field_id="+field+"&class_id="+myclass+"&orfield="+orfield, function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
						}else{
							status_msg(data.status_msg);
						}
						HidePleaseWait();
					});
				};
				
				function automap_selected_fields(myclass){
					ShowPleaseWait();
				    $(".fieldmap_multiselect:checked").each(function() {
				    	var id = $(this).val();
						$.getJSON("ajax.php", { action: "addon_transparentRETS_automapfield", field_id: id, class_id: myclass});
				     });

				     $("#ui-id-2").load("' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&p=FM&class_id="+myclass,function(){
				     	HidePleaseWait();
				     });
				};
				
				function unmap_selected_fields(myclass){
					ShowPleaseWait();
				    $(".fieldmap_multiselect:checked").each(function() {
				    	var id = $(this).val();
						$.getJSON("ajax.php", { action: "addon_transparentRETS_auto_unmapfield", field_id: id, class_id: myclass});
				     });

				     $("#ui-id-2").load("' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&p=FM&class_id="+myclass,function(){
				     	HidePleaseWait();
				     });
				};
				
				function automap_existing_fields(myclass){
					ShowPleaseWait();
					$.getJSON("ajax.php?action=addon_transparentRETS_automapexistingfields&class_id="+myclass, function(data) {
						if(data.error == "1"){
							status_error(data.error_msg);
							HidePleaseWait();
						}else{
							status_msg(data.status_msg);
							$("#ui-id-2").load("' . $config['baseurl'] . '/admin/index.php?action=addon_transparentRETS_config_imports&p=FM&class_id="+myclass,function(){
							HidePleaseWait();
							});
						}
					});

				};

				function selectallfields(){
					 $(".fieldmap_multiselect").attr("checked", "checked");
					  $("#select_all").attr("onclick", "unselectallfields(); return false;");
					 $("#select_all").attr("value", "UNSELECT All Fields");
				}
				function unselectallfields(){
					 $(".fieldmap_multiselect").removeAttr("checked");
					 $("#select_all").attr("onclick", "selectallfields(); return false;");
					 $("#select_all").attr("value", "Select All Fields");
				}
				
				function readFile(file) {                                                       
					var reader = new FileReader();
					reader.onload = readSuccess;                                            
					reader.readAsText(file);                                       
				}
				function readSuccess(evt) { 
					var data = $.csv.toObjects(evt.target.result);
					if(typeof(data[0]) === "undefined") {
						return null;
					}
					if(data[0].constructor === Object) {
						for(var row in data) {
							for(var item in data[row]) {
								if (item =="Mapped = X" && ( data[row][item] == "X" || data[row][item] =="x")){
									var cbox = data[row]["Field Number"];
									$("#fn_"+cbox).prop("checked", true);
								}
							}
						}
                        
                        raiseModal("Settings Loaded", "Your selections have been loaded. Please make sure to verify your selected (checked) fields on the left before clicking: [ Automap Selected Fields ] below.");
						
					}
				};
			</script>';

        if ($orclass == 0) {
            $display .= '<div class="rets_error">You Must have the selected RETS resource class mapped to a Open-Realty property class</fiv>';
        } else {
            $display .= '<div style="text-align: right; padding-right: 4px; padding-bottom: 4px;">
							<label id="csv_export" style="margin-left: 35%;">
								<img src="' . $config['baseurl'] . '/addons/transparentRETS/icons/export.svg"  alt="Export/download CSV">
								Export CSV
								<input id="fileexport" type="button" value="Export CSV" >
							</label>
							
							<label id="csv_import" style="">
								<img src="' . $config['baseurl'] . '/addons/transparentRETS/icons/import.svg"  alt="Import/Upload CSV">
								Import CSV
								<input type="file" value="Upload CSV" id="fileinput" accept=".csv" />
							</label>	
						</div>
						<form id="form_field_map" name="form_field_map" action="index.php?class_id=' . $class . '&amp;action=addon_transparentRETS_config_imports" method="post">
							<fieldset>
							<table width="100%">
								<tr>
								<th></th>
									<th></th>
									<th>RETS Field</th>
									<th>Open-Realty&reg; Field</th>
									<th >Field Actions</th>
								</tr>
								<tr>
									<td colspan="3">
										<input type="button" id="select_all" class="trets_std_button" value="Select All Fields" onclick="selectallfields(); return false;" />
									</td>
									<td ></td>
									<td><a href="#" class="trets_std_button" onClick="automap_existing_fields(' . $class . ');return false;">AutoMap Existing Fields</a></td>
								</tr>';
            //See of second field name is all numeric and determine ordering, skip first field as interealty has teh sysid first all others are numeric
            $sql = "SELECT metadata_fieldname 
				FROM transparentrets_metadata 
				WHERE class_id = " . intval($class);
            $frets = $this->conn->Execute($sql);
            if (!$frets) {
                $this->log_error($sql);
            }
            $frets->MoveNext();
            if (is_numeric($frets->fields['metadata_fieldname'])) {
                $sql = "SELECT metadata_id,metadata_keyfield, metadata_fieldname, metadata_orfield,metadata_longname,metadata_shortname 
					FROM transparentrets_metadata 
					WHERE class_id = " . intval($class) . "  
					ORDER BY metadata_fieldname+0";
            } else {
                $sql = "SELECT metadata_id,metadata_keyfield, metadata_fieldname, metadata_orfield,metadata_longname,metadata_shortname 
					FROM transparentrets_metadata 
					WHERE class_id = " . intval($class) . "  
					ORDER BY metadata_fieldname";
            }

            $frets = $this->conn->Execute($sql);

            if (!$frets) {
                $this->log_error($sql);
            }


            $sql = "SELECT listingsformelements_id, listingsformelements_field_name, listingsformelements_field_caption
		 		FROM " . $config['table_prefix'] . "listingsformelements 
		 		WHERE listingsformelements_id 
		 		IN (
		 				SELECT listingsformelements_id 
		 				FROM " . $config['table_prefix_no_lang'] . "classformelements 
		 				WHERE class_id = " . $orclass . "
		 			);";
            $fopenr = $this->conn->Execute($sql);

            if (!$fopenr) {
                $this->log_error($sql);
            }

            $orfnames = [];
            $orfcaptions = [];

            while (!$fopenr->EOF) {
                $orfnames[$fopenr->fields['listingsformelements_id']] = $fopenr->fields['listingsformelements_field_name'];
                $orfcaptions[$fopenr->fields['listingsformelements_id']] = $fopenr->fields['listingsformelements_field_caption'];
                $fopenr->MoveNext();
            }
            /*
             <label onmouseover="domTT_activate(this, event, \'caption\', \'Class Name\', \'content\', \'The name of the RETS Property Class. You can not modify this.\', \'trail\', \'x\');" for="RETS_CLASS_NAME">RETS Class: </label>
             <input type="text" id="RETS_CLASS_NAME" name="RETS_CLASS_NAME" value="'.$name.'" disabled="disabled" />
             */
            $x = 1;
            while (!$frets->EOF) {
                $frets_id = $frets->fields['metadata_orfield'];
                //Build Map Row for this field.
                $display .= '<tr>
								<td >' . $x . '</td>
								<td >
									<input type="checkbox" class="fieldmap_multiselect" id="fn_' . $x . '" name="multiselect[]" value="' . $frets->fields['metadata_id'] . '" />
								</td>	
								<td>
									<label ';
                if ($frets->fields['metadata_keyfield'] == 1) {
                    $display .= ' style="color:red;"';
                }
                $display .= ' onmouseover="domTT_activate(this, event, \'caption\', \''
                    . $frets->fields['metadata_fieldname'] . '\', \'content\',\'SystemName: ' .    addslashes(htmlentities(trim($frets->fields['metadata_fieldname'])))
                    . '<br />LongName: '    . addslashes(htmlentities(trim($frets->fields['metadata_longname'])))
                    . '<br />ShortName: ' . addslashes(htmlentities(trim($frets->fields['metadata_shortname'])));

                if ($frets->fields['metadata_keyfield'] == 1) {
                    $display .= '<br /> THIS IS THE KEY FIELD. IT MUST BE MAPPED';
                }
                $display .= '.\', \'trail\', \'x\');" for="id_' . $frets->fields['metadata_id'] . '">';

                $display .= '<a name="' . $frets->fields['metadata_fieldname'] . '">' . $frets->fields['metadata_fieldname'] . '</a>
					<br />(<i>' . $frets->fields['metadata_longname'] . '</i>)</label></td>' . "\r\n";

                $display .= '<td align="center"><select id="id_' . $frets->fields['metadata_id']
                    . '" name="' . $frets->fields['metadata_id'] . '">';
                $display .= '<option value="0">NOT MAPPED</option>';

                foreach ($orfnames as $fid => $fval) {
                    $display .= '<option value="' . $fid . '"';

                    if ($fid == $frets_id) {
                        $display .= ' selected="selected" ';
                    }
                    $display .= '>' . $fval . ' (' . $orfcaptions[$fid] . ')</option>';
                }

                $display .= '</select></td>';
                //Show Auto Create Map and Save Map Buttons.


                //<li><a href="'.$config['baseurl'].'/admin/index.php?action=addon_transparentRETS_config_imports&amp;p=FM&amp;class_id='.$class_id.'" id ="FM">Field Map</a></li>

                $display .= '<td>
								<input type="button"  value="AutoMap" onClick="automap_field(' . $class . ',' . $frets->fields['metadata_id'] . ');return false;" />
								<input type="button"  value="Save Map" onClick="updateField(' . $class . ',' . $frets->fields['metadata_id'] . ',$(\'#id_' . $frets->fields['metadata_id'] . '\').val());return false;;" />
							</td>
						</tr>' . "\r\n";
                $x++;
                $frets->MoveNext();
            }
            //Show Map All
            $display .= '<tr>
							<td colspan="3">
								<input type="button" class="trets_std_button" onClick="automap_selected_fields(' . $class . ');return false;" value="AutoMap Selected Fields" />
							</td>
							<td >
								<input type="button" class="trets_std_button red" onClick="unmap_selected_fields(' . $class . ');return false;" value="UNMAP Selected Fields" />
							</td>
						</tr>
					</table>
					</fieldset>
				</form>
				<div id="dialog-maps" class="d-none" title="Confirm">
					<p id="maps-message_text"></p>
				</div>' . "\r\n";

            $display .= '<script type="text/javascript">
							$(document).ready(function(){
								document.getElementById("fileinput").onchange = function(e) {
    								readFile(e.target.files[0]);
								};
								
								try {
   									$("#fileexport").click(function () {
										pclass = "' . $class . '";
										var insert_html = [
					        				"This will create a CSV file you can open/edit with a spreadsheet to make field selections offline after marking all desired fields with an X. ",
											"Any fields you have mapped in TRETS presently will already be labeled with the same \'X\' marker.",
											"<br /><br />",
											"For identification purposes, the RETS KeyField name (in red on the previous page) is denoted in the spreadsheet with enclosing asterisks e.g: **keyfieldname** ",
											"The KeyField is not actually enclosed in asterisks in the RETS metadata, or in Open-Realty.",
											"<br />",
											"<br />",  
											"Once you have made your selections in the spreadsheet, save it as a new CSV file and import it, your selections will be transferred to the checkboxes on the left.", 
											"<br/><br/>",
											"You can only make field selections via this spreadsheet, changing anything else will not affect anything, and may prevent this from working as intended. ",
											"When saving changes in your spreadsheet, you must make sure to use \"Save As\" instead of simply: \"Save\" and make sure CSV is selected as the file type. ",
                                            "<div>",
                                            "<button id=\"create_csv_btn\" type=\"button\" class=\"btn btn-primary btn-sm\" aria-label=\"Close\">Ok</button>",
                                            "<button type=\"button\" class=\"btn btn-danger btn-sm\" data-bs-dismiss=\"modal\" aria-label=\"Close\">Cancel</button>",
                                            "</div>"
										].join("");
										$("#maps-message_text").html(insert_html);
                                        raiseModal("Create CSV?", insert_html);
										
									return false;
									});
								} 
								catch (e) {
    								alert(e);
								}
							});
                            $("#customizeModal").on("click", "#create_csv_btn", function (e) {
                                // do something...
                                e.preventDefault();
                                closeModal();
                                window.location.href = "ajax.php?action=addon_transparentRETS_make_fieldname_csv&class_id="+pclass;
                            });

						</script>';
        }

        return $display;
    }

    public function update_clientapp_lookuptypes($resource_id, $class_id)
    {
        global $config;
        $display = '';
        /*
         CREATE TABLE  `or`.`transparentrets_metadata` (
         `metadata_id` int(11) NOT NULL auto_increment,
         `class_id` int(11) NOT NULL,
         `metadata_fieldname` varchar(32) NOT NULL,
         `metadata_fieldtype` varchar(10) NOT NULL,
         `metadata_default` tinyint(1) NOT NULL,
         `metadata_uniquefield` tinyint(1) NOT NULL,
         `metadata_searchfield` tinyint(1) NOT NULL,
         `metadata_keyfield` tinyint(1) NOT NULL,
         `metadata_lookupname` varchar(32) default NULL,
         `metadata_interpretation` varchar(15) default NULL,
         `metadata_required` int(11) default NULL,
         `metadata_longname` varchar(32) default NULL,
         `metadata_shortname` varchar(24) default NULL,
         `metadata_searchhelpid` varchar(32) NOT NULL,
         `metadata_orfield` int(11) NOT NULL,
         `metadata_listingsupdate_field` tinyint(1) NOT NULL,
         `metadata_photoupdate_field` tinyint(1) NOT NULL,
         `metadata_agentid_field` tinyint(1) NOT NULL,
         PRIMARY KEY  (`metadata_id`)
         ) ENGINE=MyISAM DEFAULT CHARSET=latin1
            */
        $sql = 'SELECT class_id,metadata_fieldname,metadata_lookupname,metadata_orfield 
				FROM transparentrets_metadata
				WHERE metadata_orfield > 0 
				AND	(metadata_interpretation = \'Lookup\' OR metadata_interpretation = \'LookupMulti\' 
					OR metadata_interpretation = \'LookupBitstring\' OR metadata_interpretation = \'LookupBitmask\'
				)';
        $RETSMetaDataTable = $this->conn->Execute($sql);

        if (!$RETSMetaDataTable) {
            $this->log_error($sql);
        }
        //Store Lookups
        $lookups = [];
        while (!$RETSMetaDataTable->EOF) {
            $metadata_lookupname = $RETSMetaDataTable->fields['metadata_lookupname'];
            $metadata_orfield = $RETSMetaDataTable->fields['metadata_orfield'];
            $metadata_classid = $RETSMetaDataTable->fields['class_id'];
            $sql = 'SELECT metadata_lookupname,metadata_value,metadata_longvalue 
				FROM transparentrets_lookuptype 
				WHERE resource_id IN (
					SELECT resource_id 
					FROM transparentrets_classes 
					WHERE class_id=' . $metadata_classid . '
				) 
				AND metadata_lookupname = ' . $this->conn->qstr($metadata_lookupname) . ' 
				ORDER BY metadata_longvalue';
            $LookupMetadata = $this->conn->Execute($sql);

            if (!$LookupMetadata) {
                $this->log_error($sql);
            }
            $lookups[$metadata_orfield][] = '';
            while (!$LookupMetadata->EOF) {
                $lookups[$metadata_orfield][] = $LookupMetadata->fields['metadata_longvalue'];
                $LookupMetadata->MoveNext();
            }
            $RETSMetaDataTable->MoveNext();
        }
        $orfields = array_keys($lookups);


        foreach ($orfields as $orfield) {
            $uniqueArray = $lookups[$orfield];
            $uniqueArray = array_unique($uniqueArray);
            $sql = 'UPDATE ' . $config['table_prefix'] . 'listingsformelements 
					SET listingsformelements_field_elements = ' . $this->conn->qstr(implode('||', $uniqueArray)) . ' 
					WHERE listingsformelements_id = ' . intval($orfield);
            $rs = $this->conn->Execute($sql);

            if (!$rs) {
                $this->log_error($sql);
            }
        }
    }

    public function ca_get_classlist()
    {
        global $config;
        $sql = 'SELECT class_id, class_name 
			FROM ' . $config['table_prefix'] . 'class 
			ORDER BY class_rank';
        $c = $this->conn->execute($sql);

        if (!$c) {
            $this->log_error($sql);
        }
        $class = [];
        while (!$c->EOF) {
            $class[$c->fields['class_id']] = $c->fields['class_name'];
            $c->MoveNext();
        }
        return $class;
    }

    public function get_field_list()
    {
        global $config;
        $sql = 'SELECT listingsformelements_id,listingsformelements_field_name 
			FROM ' . $config['table_prefix'] . 'listingsformelements';
        $orfields = $this->conn->Execute($sql);

        if (!$orfields) {
            $this->log_error($sql);
        }

        $fields_or = [];

        while (!$orfields->EOF) {
            $fields_or[$orfields->fields['listingsformelements_id']] = $orfields->fields['listingsformelements_field_name'];
            $orfields->MoveNext();
        }
        return $fields_or;
    }

    public function get_agentid_maps()
    {
        global $config;
        //Get List of Open-Realty Agent IDS with RETS_AGENT_ID set...
        $or_agent_mapping = [];
        $sql = 'SELECT userdb_id,userdbelements_field_value FROM ' . $config['table_prefix']
            . 'userdbelements WHERE userdbelements_field_name = \'RETS_AGENT_ID\' AND userdbelements_field_value != \'\' AND userdb_id IN (SELECT userdb_id FROM ' . $config['table_prefix'] . 'userdb WHERE userdb_is_agent = \'yes\')';
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }


        while (!$recordSet->EOF) {
            $values = explode('||', $recordSet->fields('userdbelements_field_value'));
            foreach ($values as $v) {
                $or_agent_mapping[$v]    = $recordSet->fields('userdb_id');
            }
            $recordSet->moveNext();
        }
        return $or_agent_mapping;
    }

    public function get_listing_owner_ids($keyArray)
    {
        global $config;
        //GET LIST OF LISTING OWNERS
        $sqlKeyArray = [];
        foreach ($keyArray as $keyvalue) {
            $sqlKeyArray[] = '\'' . $keyvalue . '\'';
        }
        $sql =
            'SELECT userdb_id,listingsdbelements_field_value FROM ' . $config['table_prefix']
            . 'listingsdbelements WHERE listingsdbelements_field_name= ' . $this->conn->qstr(
                $this->mapped_or_fields[$this->keyfield]
            )
            . ' AND listingsdbelements_field_value IN (' . implode(',', $sqlKeyArray)
            . ');';
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        $this->ListingOwnerIDArray = [];

        while (!$recordSet->EOF) {
            $this->ListingOwnerIDArray[$recordSet->fields('listingsdbelements_field_value')]
                = $recordSet->fields('userdb_id');
            $recordSet->MoveNext();
        }
    }

    public function get_listing_ids($keyArray)
    {
        global $config;
        //Get Array of Open-Realty IDS based on Key Arrays.
        $sqlKeyArray = [];
        foreach ($keyArray as $keyvalue) {
            $sqlKeyArray[] = '\'' . $keyvalue . '\'';
        }
        $sql =
            'SELECT listingsdb_id,listingsdbelements_field_value FROM ' . $config['table_prefix']
            . 'listingsdbelements WHERE listingsdbelements_field_name= ' . $this->conn->qstr(
                $this->mapped_or_fields[$this->keyfield]
            )
            . ' AND listingsdbelements_field_value IN (' . implode(',', $sqlKeyArray)
            . ');';
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            $this->log_error($sql);
        }

        $this->ListingIDKeyArray = [];

        while (!$recordSet->EOF) {
            $this->ListingIDKeyArray[$recordSet->fields('listingsdbelements_field_value')]
                = $recordSet->fields('listingsdb_id');
            $recordSet->MoveNext();
        }
    }

    public function cleanup_get_listing_ids_missing($keyArray)
    {
        global $config;
        //Check for missing listings
        $sql = 'SELECT listingsdbelements_field_value FROM ' . $config['table_prefix'] . 'listingsdbelements, '
            . $config['table_prefix'] . 'listingsdb WHERE listingsdbelements_field_name = \''
            . $this->mapped_or_fields[$this->keyfield] . '\' AND ' . $config['table_prefix'] . 'listingsdbelements.listingsdb_id = '
            . $config['table_prefix'] . 'listingsdb.listingsdb_id AND listingsdb_pclass_id = ' . $this->or_class_id;

        $recordSet = $this->conn->execute($sql);

        $mls_ids_in_or = [];
        while (!$recordSet->EOF) {
            $mls_ids_in_or[]
                = stripslashes($recordSet->fields('listingsdbelements_field_value'));
            $recordSet->MoveNext();
        }

        return array_diff($keyArray, $mls_ids_in_or);
    }

    public function cleanup_get_listing_ids($keyArray, $cleanup_debug)
    {
        global $config;
        $mls_key_ids = [];
        foreach ($keyArray as $k) {
            $mls_key_ids[] = "'$k'";
        }
        //Allow listings with empty keys to remain. (Non Rets Listings
        $mls_key_ids[] = "''";

        $mls_id_sql = implode(',', $mls_key_ids);
        if ($cleanup_debug) {
            echo "\r\n keyArrayCleanup " . print_r($keyArray, true) . " \r\n";
        }
        $sub_sql = 'SELECT listingsdb_id 
				FROM ' . $config['table_prefix'] . 'listingsdbelements 
				WHERE listingsdbelements_field_name = ' . $this->conn->qstr($this->mapped_or_fields[$this->keyfield]) . ' 
				AND listingsdbelements_field_value IN (' . $mls_id_sql . ')';

        //Lookup Listing ID in for this property class that are not in this Array.
        $sql = 'SELECT listingsdb_id 
				FROM ' . $config['table_prefix'] . 'listingsdb 
				WHERE listingsdb_pclass_id = ' . $this->or_class_id . ' 
				AND listingsdb_id NOT IN (' . $sub_sql . ')';
        if ($cleanup_debug) {
            echo "\r\n SQL $sql \r\n";
        }
        $recordSet = $this->conn->Execute($sql);

        if (!$recordSet) {
            die($sql);
        }

        $listing_ids = [];
        while (!$recordSet->EOF) {
            $listing_ids[] = $recordSet->fields('listingsdb_id');
            $recordSet->MoveNext();
        }
        if ($cleanup_debug) {
            echo "\r\n ListingsIDS (Array) " . print_r($listing_ids, true) . " \r\n";
        }
        return $listing_ids;
    }

    public function delete_images()
    {
        global $config;
        $deleteImages = [];
        //echo '<pre> Image Array: '.print_r($this->images,TRUE).'</pre>';
        foreach ($this->images as $imagename => $imagevalue) {
            //Delete image from DB if it exists
            $listingid = explode('_', $imagename);
            $image_rank = trim($listingid[1]);
            $listingid = trim($listingid[0]);
            $deleteImages[] = $listingid;
        }
        $deleteImages = array_unique($deleteImages);
        foreach ($deleteImages as $listingid) {
            if (!isset($this->ListingIDKeyArray[$listingid])) {
                $this->log_action(
                    'Image for Listing ' . $listingid . ' not removed becuase no Open-Realty listing exists. Should be handled by cleanup.'
                );
                continue;
            }
            $orlistingid = $this->ListingIDKeyArray[$listingid];
            if ($this->image_location == 1) {
                $sql = "DELETE FROM " . $config['table_prefix'] . "listingsimages 
						WHERE ((listingsdb_id = $orlistingid)
						AND (listingsimages_file_name 
						LIKE 'http://%' 
						OR listingsimages_file_name 
						LIKE 'https://%'))";
                $recordSet2 = $this->conn->Execute($sql);

                if (!$recordSet2) {
                    $this->log_error($sql);
                }
            } else {
                $sql = "SELECT listingsimages_file_name, listingsimages_thumb_file_name 
					FROM " . $config['table_prefix'] . "listingsimages 
					WHERE ((listingsdb_id = $orlistingid) 
					AND (listingsimages_file_name 
					LIKE '%" . $listingid . "_%'))";

                $recordSet = $this->conn->Execute($sql);

                if (!$recordSet) {
                    $this->log_error($sql);
                }

                while (!$recordSet->EOF) {
                    $thumb_file_name = $recordSet->fields('listingsimages_thumb_file_name');
                    $file_name = $recordSet->fields('listingsimages_file_name');
                    $sql_file_name = $this->conn->qstr($file_name);
                    // delete from the db
                    $sql =
                        "DELETE FROM " . $config['table_prefix'] . "listingsimages 
						WHERE ((listingsdb_id = $orlistingid) 
						AND (listingsimages_file_name = $sql_file_name))";
                    $recordSet2 = $this->conn->Execute($sql);

                    if (!$recordSet2) {
                        $this->log_error($sql);
                    }

                    // delete the files themselves
                    // on widows, required php 4.11 or better (I think)
                    @unlink("$config[listings_upload_path]/$file_name");
                    if ($file_name != $thumb_file_name) {
                        @unlink("$config[listings_upload_path]/$thumb_file_name");
                    }
                    $recordSet->MoveNext();
                    //$this->log_action ("Delete Listing Image $file_name");
                }
            }
        }
    }

    public function import_images()
    {
        global $config, $api;

        $display = '';
        $media_data = [];
        foreach ($this->images as $imagename => $imagevalue) {
            //Delete image from DB if it exists
            $listingid = explode('_', $imagename);
            $image_rank = trim($listingid[1]);
            $listingid = trim($listingid[0]);

            if (!isset($this->ListingIDKeyArray[$listingid])) {
                echo 'Image ' . $listingid . '_' . $image_rank . ' not imported becuase no Open-Realty listing exists, should be handled by cleanup.<br />' . "\r\n";
                flush();
                continue;
            }
            $orlistingid = $this->ListingIDKeyArray[$listingid];

            $imgDesc = '';
            if (array_key_exists($imagename, $this->imagesdec)) {
                $imgDesc = $this->imagesdec[$imagename];
            }
            $media_data[$orlistingid][$imagename . '.jpg']['description'] = $imgDesc;
            if (strpos($imagevalue, 'http') === 0) {
                $media_data[$orlistingid][$imagename . '.jpg']['remote'] = true;
            }
            $media_data[$orlistingid][$imagename . '.jpg']['data'] = $imagevalue;
            $media_data[$orlistingid][$imagename . '.jpg']['rank'] = $image_rank;
            if ($this->image_location == 1) {
                $media_data[$orlistingid][$imagename . '.jpg']['remote'] = true;
            }
        }
        foreach ($media_data as $or_parent_id => $mediaobject) {
            $result = $api->load_local_api('media__create', ['media_parent_id' => $or_parent_id, 'media_type' => 'listingsimages', 'media_data' => $mediaobject]);

            if ($result['error'] == true && TRETS_API_DEBUG != true) {
                echo $result['error_msg'];
            }
            if (TRETS_API_DEBUG == true) {
                echo '<pre>' . print_r($result, true) . "</pre>\r\n";
            }
        }
    }

    public function import_listings()
    {
        global $config, $api;
        $display = '';
        $new_listings = [];
        foreach ($this->listings as $listing) {
            //Check for a listing agent

            if (array_key_exists($listing[$this->agentid_field], $this->or_agent_mapping)) {
                $listing_agent = $this->or_agent_mapping[$listing[$this->agentid_field]];
            } else {
                $listing_agent = $this->default_agent;
            }
            //See if listing exists

            $sql =
                'SELECT listingsdb_id FROM ' . $config['table_prefix'] . 'listingsdbelements WHERE listingsdbelements_field_name = '
                . $this->conn->qstr($this->mapped_or_fields[$this->keyfield])
                . ' AND listingsdbelements_field_value = '
                . $this->conn->qstr($listing[$this->keyfield]);
            $recordSet = $this->conn->Execute($sql);

            if (!$recordSet) {
                $this->log_error($sql);
            }
            if ($recordSet->RecordCount() == 1) {
                $new_listing_id = $recordSet->fields('listingsdb_id');
                $display .= 'Updating Listing ' . $listing[$this->keyfield] . '<br>';
                $title = $this->listing_title;

                $notes = 'Listing Added by TransparentRETS on ' . date("m/d/Y");
                $import_fileds = [];
                foreach ($listing as $fieldname => $fieldvalue) {
                    $title = str_replace('{' . $fieldname . '}', $fieldvalue, $title);
                    if (!isset($this->mapped_or_fields[$fieldname])) {
                        continue;
                    }
                    if (
                        $this->fieldtypes[$fieldname] == 'Lookup'
                        || $this->fieldtypes[$fieldname] == 'LookupMulti'
                        || $this->fieldtypes[$fieldname] == 'LookupBitstring'
                        || $this->fieldtypes[$fieldname] == 'LookupBitmask'
                    ) {
                        $fv = explode(',', $fieldvalue);
                        foreach ($fv as $x => $value) {
                            $value = ltrim($value);
                            $fv[$x] = trim($value, '"');
                        }
                        $import_fileds[$this->mapped_or_fields[$fieldname]] = $fv;
                    } else {
                        $import_fileds[$this->mapped_or_fields[$fieldname]] = $fieldvalue;
                    }
                }

                $result = $api->load_local_api('listing__update', ['class_id' => $this->or_class_id, 'listing_id' => $new_listing_id, 'listing_details' => ['title' => $title, 'seotitle' => 'AUTO'], 'listing_agents' => [$listing_agent], 'listing_fields' => $import_fileds, 'listing_media' => [], 'or_int_disable_log' => true, 'or_date_format' => '%Y-%m-%d']);
                if ($result['error'] == true) {
                    die($result['error_msg']);
                }
            } else {
                $display .= 'Adding Listing ' . $listing[$this->keyfield] . '<br>';
                $title = $this->listing_title;

                $notes = 'Listing Added by TransparentRETS on ' . date("m/d/Y");
                $import_fileds = [];
                foreach ($listing as $fieldname => $fieldvalue) {
                    $title = str_replace('{' . $fieldname . '}', $fieldvalue, $title);
                    if (!isset($this->mapped_or_fields[$fieldname])) {
                        continue;
                    }
                    if (
                        $this->fieldtypes[$fieldname] == 'Lookup'
                        || $this->fieldtypes[$fieldname] == 'LookupMulti'
                        || $this->fieldtypes[$fieldname] == 'LookupBitstring'
                        || $this->fieldtypes[$fieldname] == 'LookupBitmask'
                    ) {
                        $fv = explode(',', $fieldvalue);
                        foreach ($fv as $x => $value) {
                            $value = ltrim($value);
                            $fv[$x] = trim($value, '"');
                        }
                        $import_fileds[$this->mapped_or_fields[$fieldname]] = $fv;
                    } else {
                        $import_fileds[$this->mapped_or_fields[$fieldname]] = $fieldvalue;
                    }
                }

                $result = $api->load_local_api('listing__create', ['class_id' => $this->or_class_id, 'listing_details' => ['title' => $title, 'featured' => false, 'active' => true, 'notes' => $notes], 'listing_agents' => [$listing_agent], 'listing_fields' => $import_fileds, 'listing_media' => [], 'or_int_disable_log' => true, 'or_date_format' => '%Y-%m-%d']);
                if ($result['error'] == true) {
                    die($result['error_msg']);
                }
                //THis is a new listing make sure we get photos
                $new_listings[] = $listing[$this->keyfield];
            }
            if (TRETS_API_DEBUG == true) {
                echo '<pre>' . print_r($result, true) . "</pre>\r\n";
            }
        }
        return $new_listings;
    }

    public function cleanappfields()
    {
        global $config;
        $display = '';
        $display .= 'Deleting Open-Realty Fields<br />';
        $sql = 'TRUNCATE TABLE ' . $config['table_prefix'] . 'listingsformelements';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $sql = 'TRUNCATE TABLE ' . $config['table_prefix_no_lang'] . 'classformelements';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        return $display;
    }

    public function truncate_app_tables()
    {
        global $config;
        $display = '';
        $display .= 'Deleting Open-Realty Listings<br />';
        $sql = 'TRUNCATE TABLE ' . $config['table_prefix'] . 'listingsdb';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $sql = 'TRUNCATE TABLE ' . $config['table_prefix'] . 'listingsdbelements';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting Open-Realty Listing Images<br />';
        $sql = 'TRUNCATE TABLE ' . $config['table_prefix'] . 'listingsimages';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        //Delete the images
        //Load Module Permissions
        if ($handle = opendir(dirname(__FILE__) . '/../../images/listing_photos')) {
            while (false !== ($file = readdir($handle))) {
                if (strpos($file, '.jpg') !== false) {
                    unlink(dirname(__FILE__) . '/../../images/listing_photos/' . $file);
                }
            }
            closedir($handle);
        }
        return $display;
    }

    public function make_fieldname_csv($class_id)
    {
        global $conn, $config;

        if ($class_id <= 0) {
            return "Nope!";
        }
        $class_name = $this->get_RETS_classname($class_id);
        $server_id = $this->get_RETS_server_id($class_id);

        //See of second field name is all numeric and determine ordering, skip first field as interealty has teh sysid first all others are numeric
        $sql = "SELECT metadata_fieldname 
				FROM transparentrets_metadata 
				WHERE class_id = " . $class_id;
        $frets = $this->conn->Execute($sql);
        if (!$frets) {
            $this->log_error($sql);
        }
        $frets->MoveNext();

        if (is_numeric($frets->fields['metadata_fieldname'])) {
            $sql = "SELECT metadata_fieldname, metadata_longname, metadata_keyfield, metadata_orfield
				FROM transparentrets_metadata 
				WHERE class_id = " . $class_id . "  
				ORDER BY metadata_fieldname+0";
        } else {
            $sql = "SELECT metadata_fieldname, metadata_longname, metadata_keyfield, metadata_orfield
				FROM transparentrets_metadata 
				WHERE class_id = " . $class_id . "  
				ORDER BY metadata_fieldname";
        }
        $frets = $this->conn->Execute($sql);
        if (!$frets) {
            $this->log_error($sql);
        }

        // setup first row info
        $headings = ['Field Number', 'Field System Name (Field Name)', 'Field Long name (Caption)', 'Mapped = X'];

        //make arrays of row data
        $x = 1;
        $k = 0;
        while (!$frets->EOF) {
            $f_arr[$k]['id'] = $x;
            if ($frets->fields['metadata_keyfield'] == 1) {
                $f_arr[$k]['fname'] = "**" . trim($frets->fields['metadata_fieldname']) . "**";
            } else {
                $f_arr[$k]['fname'] = trim($frets->fields['metadata_fieldname']);
            }
            $f_arr[$k]['fcap'] = trim($frets->fields['metadata_longname']);

            if (trim($frets->fields['metadata_orfield']) != 0) {
                $f_arr[$k]['mapped'] = 'X';
            }

            $x++;
            $k++;
            $frets->MoveNext();
        }

        $filename = 'RETS_Fields_[' . $server_id . ']-(' . $class_id . ')' . $class_name . '_' . date('mdY') . '_' . date('his');

        // Output CSV-specific headers
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=\"$filename.csv\";");

        $fh = fopen('php://output', 'w');

        //insert header row
        $this->fputcsv2($fh, $headings);

        //setup the rest of the data

        foreach ($f_arr as $item) {
            $this->fputcsv2($fh, $item);
            //fwrite($fh, $Delimiter.implode($Delimiter.$Separator.$Delimiter, $item).$Delimiter."\n");
        }

        fclose($fh);
        return;
    }

    public function get_RETS_classname($class_id)
    {
        global $conn, $config;
        $sql = "SELECT visible_name 
				FROM transparentrets_classes
				WHERE class_id = " . intval($class_id);
        $cname = $this->conn->Execute($sql);
        if (!$cname) {
            $this->log_error($sql);
        }
        $visible_name = $cname->fields['visible_name'];

        return $visible_name;
    }

    public function get_RETS_server_id($class_id)
    {
        global $conn, $config;
        $sql = "SELECT account_id
			FROM transparentrets_resources res, transparentrets_classes class
			WHERE res.resource_id = class.resource_id
			AND class.class_id =  " . intval($class_id);
        $sname = $this->conn->Execute($sql);
        if (!$sname) {
            $this->log_error($sql);
        }
        $account_id = $sname->fields['account_id'];

        return $account_id;
    }

    public function fputcsv2($fh, array $fields, $delimiter = ',', $enclosure = '"')
    {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = [];
        foreach ($fields as $field) {
            $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
                $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
            ) : $field;
        }

        fwrite($fh, join($delimiter, $output) . "\n");
    }
}
