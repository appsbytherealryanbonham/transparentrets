<?php

class TransparentRETS_loader
{
    // Class Variables
    protected $last_update;
    protected $class_id;
    protected $class_name;
    protected $or_class_id;
    protected $resource_id;
    protected $default_agent;
    protected $listing_title;
    protected $metadata_version;
    protected $modified_map;
    // Resource Variables
    protected $resource_name;
    // Photo Variables;
    protected $object_name;
    // Account Variables;
    private $account_id;
    private $account;
    private $password;
    private $url;
    private $port;
    private $application;
    private $version;
    private $client_password;
    private $keep_alive;
    private $post_request;
    private $encode_request;
    private $uapassword;
    private $rets_version;
    // Search Values
    protected $search_criteria;
    // Listing Holder
    protected $listings;
    // Open-Realty Fields
    protected $fields_or = [];
    // Metadata Holders
    protected $agentid_field;
    protected $mapped_or_fields;
    protected $fieldtypes = [];
    protected $mapped_fields = [];
    protected $keyfield_datatype;
    protected $searchArgs = [];
    protected $photoupdate_field;
    protected $listingupdate_field;
    protected $image_location = 0;
    protected $image_location_force_https = 0;
    // Open-Realty Agent Map
    protected $or_agent_mapping = [];
    // Image Holder
    protected $images;
    protected $imagesdec;
    protected $ListingOwnerIDArray;
    protected $ListingIDKeyArray;

    protected $keyfield;
    // Batch Sizes
    private $listing_batch_size = 200;
    private $photo_batch_size = 10;
    //Database Connection
    protected $conn;
    public $RETS;


    protected function __construct()
    {
        if (!defined('TRETS_CONN_DEBUG')) {
            define('TRETS_CONN_DEBUG', false);
            define('TRETS_RESPONCE_DEBUG', false);
            define('TRETS_DIGEST_DEBUG', false);
            define('TRETS_DEBUG_TOFILE', false);
            define('TRETS_API_DEBUG', false);
            define('TRETS_FORCE_HTTPS', true);
        }
    }
    public function transparentRETS_current_version()
    {
        $current_version = '2.4.0';
        return $current_version;
    }
    public function cleantables()
    {
        $display = '';
        $display .= 'Deleting TransparnetRETS Classes<br />';
        $sql = 'TRUNCATE TABLE transparentrets_classes';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting TransparnetRETS Resources<br />';
        $sql = 'TRUNCATE TABLE transparentrets_resources';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting TransparnetRETS LookupTypes<br />';
        $sql = 'TRUNCATE TABLE transparentrets_lookuptype';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting TransparnetRETS Metadata<br />';
        $sql = 'TRUNCATE TABLE transparentrets_metadata';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting TransparnetRETS Objects<br />';
        $sql = 'TRUNCATE TABLE transparentrets_objects';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting TransparnetRETS Search<br />';
        $sql = 'TRUNCATE TABLE transparentrets_search';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= 'Deleting TransparnetRETS Search Help<br />';
        $sql = 'TRUNCATE TABLE transparentrets_searchhelp';
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
        $display .= $this->truncate_app_tables();
        return $display;
    }
    public function flag_class_map_modified($class_id)
    {
        $sql = 'UPDATE transparentrets_classes SET modified_map = 1 WHERE class_id = ' . intval($class_id);
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
    }
    public function clearflag_class_map_modified($class_id)
    {
        $sql = 'UPDATE transparentrets_classes SET modified_map = 0 WHERE class_id = ' . intval($class_id);
        $tsql = $this->conn->Execute($sql);
        if (!$tsql) {
            $this->log_error($sql);
        }
    }
    public function check_metadata_updates($resource_id, $class_id, $close_session = false, $force_update = false)
    {
        $display = '';
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display .= $this->log_action('Starting Metadata Update Check') . '<br />';
        //Get Resource and Class Names and versino informations..
        $this->get_table_account($resource_id);
        $sql = 'SELECT resource_name,resources_version,class_version,object_version,searchhelp_version,lookup_version 
			FROM transparentrets_resources 
			WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);

        if (!$resource) {
            $this->log_error($sql);
        }
        $sql = 'SELECT class_name,table_version 
			FROM transparentrets_classes 
			WHERE class_id = ' . $class_id;
        $class = $this->conn->Execute($sql);
        if (!$class) {
            $this->log_error($sql);
        }
        //Get Version Information fro
        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $status = $this->RETS->loginDirect(
            $this->account,
            $this->password,
            $this->url,
            $this->application,
            $this->version,
            $this->client_password,
            $this->port,
            $this->keep_alive,
            $this->post_request,
            $this->encode_request,
            $this->rets_version,
            $this->uapassword
        );
        if (!$status) {
            return 'Login Failed';
        }
        $versions = $this->RETS->get_version($resource->fields['resource_name'], $class->fields['class_name']);
        if (!$versions) {
            if ($this->RETS->LAST_ERROR_CODE != 0) {
                $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
            }
            return $display;
        }
        //Get Version Information from Cache
        if ($force_update == false) {
            $cache_resources_version = $resource->fields['resources_version'];
            $cache_class_version = $resource->fields['class_version'];
            $cache_object_version = $resource->fields['object_version'];
            $cache_searchhelp_version = $resource->fields['searchhelp_version'];
            $cache_lookup_version = $resource->fields['lookup_version'];
            $cache_table_version = $class->fields['table_version'];
        } else {
            $cache_resources_version = '';
            $cache_class_version = '';
            $cache_object_version = '';
            $cache_searchhelp_version = '';
            $cache_lookup_version = '';
            $cache_table_version = '';
        }
        if ($cache_resources_version != $versions['ResourceVersion']) {
            $display .= $this->log_action('Updating Resources from Version: ' . $cache_resources_version . ' to Version: ' . $versions['ResourceVersion']) . '<br />';
            $display .= $this->detectRETSResources($this->account_id);
        } else {
            $display .= $this->log_action('Resource Version Matches') . '<br />';
        }
        if ($cache_class_version != $versions['ClassVersion']) {
            $display .= $this->log_action('Updating Classes from Version: ' . $cache_class_version . ' to Version: ' . $versions['ClassVersion']) . '<br />';
            //Update Classes
            $display .= $this->detectRETSClasses($this->account_id, $resource_id);
        } else {
            $display .= $this->log_action('Class Version Matches') . '<br />';
        }
        if ($cache_object_version != $versions['ObjectVersion']) {
            $display .= $this->log_action('Updating Objects from Version: ' . $cache_object_version . ' to Version: ' . $versions['ObjectVersion']) . '<br />';
            //Update Objects
            $display .= $this->detectRETSObjects($this->account_id, $resource_id);
        } else {
            $display .= $this->log_action('Object Version Matches') . '<br />';
        }
        if ($cache_searchhelp_version != $versions['SearchHelpVersion']) {
            $display .= $this->log_action('Updating SearchHelp from Version: ' . $cache_searchhelp_version . ' to Version: ' . $versions['SearchHelpVersion']) . '<br />';
            //Update Objects
            $display .= $this->detectRETSSearchHelp($this->account_id, $resource_id);
        } else {
            $display .= $this->log_action('SearchHelp Version Matches') . '<br />';
        }
        if ($cache_lookup_version != $versions['LookupVersion']) {
            $display .= $this->log_action('Updating Lookups from Version: ' . $cache_lookup_version . ' to Version: ' . $versions['LookupVersion']) . '<br />';
            //Update LookupType
            $display .= $this->detectRETSLookupType($this->account_id, $resource_id);
            $update_client_lookups = true;
        } else {
            $display .= $this->log_action('Lookup Version Matches') . '<br />';
            $update_client_lookups = false;
        }


        if ($cache_table_version != $versions['TableVersion']) {
            $display .= $this->log_action('Updating Tables from Version: ' . $cache_table_version . ' to Version: ' . $versions['TableVersion']) . '<br />';
            $display .= $this->detectRETSMetadataTable($this->account_id, $resource_id, $class_id);
        } else {
            $display .= $this->log_action('Resource Version Matches') . '<br />';
        }
        if ($update_client_lookups == true) {
            $display .= $this->log_action('Updating Lookup Information in Client Application') . '<br />';
            $this->update_clientapp_lookuptypes($resource_id, $class_id);
        }
        if ($close_session) {
            $this->RETS->rets_logout();
        }
        $display .= $this->log_action('End of Metadata Update Check') . '<br />';
        return $display;
    }

    protected function get_photo_object($resource_id)
    {
        $sql = 'SELECT object_name FROM transparentrets_objects WHERE object_photo = 1 AND resource_id = ' . $resource_id;
        $objects = $this->conn->Execute($sql);
        if (!$objects) {
            $this->log_error($sql);
        }
        return $objects->fields['object_name'];
    }
    protected function get_resource_name($resource_id)
    {
        // Get Resource Name
        $sql = 'SELECT resource_name FROM transparentrets_resources WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);
        if (!$resource) {
            $this->log_error($sql);
        }
        return $resource->fields['resource_name'];
    }

    protected function get_table_class($class_id = null)
    {
        // Get Class Table Info
        $class_sql_where = '';
        if ($class_id != null) {
            $class_sql_where = ' AND class_id = ' . intval($class_id);
        }
        $sql = 'SELECT class_id,resource_id,class_name,class_id_import,default_agent,listing_title,last_update,modified_map, visible_name 
				FROM transparentrets_classes 
				WHERE class_import = 1 ' . $class_sql_where;
        $classtable = $this->conn->Execute($sql);
        if (!$classtable) {
            $this->log_error($sql);
        }
        $classtable_array = [];
        $x = 0;
        while (!$classtable->EOF) {
            $classtable_array[$x]['class_id'] = $classtable->fields['class_id'];
            $classtable_array[$x]['resource_id'] = $classtable->fields['resource_id'];
            $classtable_array[$x]['class_id_import'] = $classtable->fields['class_id_import'];
            //$classtable_array[$x]['metadata_version'] = $classtable->fields['metadata_version'];
            $classtable_array[$x]['default_agent'] = $classtable->fields['default_agent'];
            $classtable_array[$x]['listing_title'] = $classtable->fields['listing_title'];
            $classtable_array[$x]['last_update'] = $classtable->fields['last_update'];
            $classtable_array[$x]['class_name'] = $classtable->fields['class_name'];
            $classtable_array[$x]['modified_map'] = $classtable->fields['modified_map'];
            $classtable_array[$x]['visible_name'] = $classtable->fields['visible_name'];
            $x++;
            $classtable->MoveNext();
        }
        return $classtable_array;
    }
    protected function get_table_account($resource_id = null, $account_id = null)
    {
        if ($account_id != null) {
            $sql = 'SELECT * 
					FROM transparentrets_servers 
					WHERE account_id = ' . $account_id . ';';
        } else {
            $sql = 'SELECT * 
					FROM transparentrets_servers 
					WHERE account_id IN (
						SELECT account_id 
						FROM transparentrets_resources 
						WHERE resource_id = ' . $resource_id . '
					);';
        }

        $account = $this->conn->Execute($sql);
        if (!$account) {
            $this->log_error($sql);
        }
        $account_array = [];
        $this->account_id = $account->fields['account_id'];
        $this->account = $account->fields['account'];
        $this->password = $account->fields['password'];
        $this->url = $account->fields['url'];
        $this->port  = $account->fields['port'];
        $this->application = $account->fields['application'];
        $this->version = $account->fields['version'];
        $this->client_password = $account->fields['client_password'];
        $this->keep_alive = $account->fields['keep_alive'];
        $this->post_request = $account->fields['post_request'];
        $this->encode_request = $account->fields['encode_request'];
        $this->uapassword = $account->fields['uapassword'];
        $this->rets_version = $account->fields['rets_vers'];
        $this->listing_batch_size = $account->fields['listing_batch'];
        $this->photo_batch_size = $account->fields['photo_batch'];
        $this->image_location = $account->fields['remote_photos'];
        $this->image_location_force_https = $account->fields['force_https'];
        //echo get_class($this);
        //echo 'Account ID: "'.$this->account.'"';
    }
    protected function get_search_values($class_id)
    {
        $sql = 'SELECT metadata_id, search_value FROM transparentrets_search WHERE class_id =' . intval($class_id);
        $search = $this->conn->Execute($sql);

        if (!$search) {
            $this->log_error($sql);
        }

        $search_criteria = [];

        while (!$search->EOF) {
            if ($search->fields['search_value'] != '') {
                $search_criteria[$search->fields['metadata_id']] = $search->fields['search_value'];
            }
            $search->moveNext();
        }
        return $search_criteria;
    }

    public function run_debug_transaction($class_id, $query)
    {
        // Function that executes custom RETS queries pasted into the
        // Custom RETS Query form.
        //@apache_setenv('no-gzip', 1);

        // if suPHP this no workey.
        if (function_exists('apache_setenv')) {
            apache_setenv('no-gzip', 1);
        }

        if (extension_loaded('zlib')) {
            ini_set('zlib.output_compression', false);
        }
        @ini_set('zlib.output_compression', 0);
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        require_once(dirname(__FILE__) . "/remapper.php");
        $remapper = new remapper();
        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        //$this->RETS->conn_debug = TRUE;
        //$this->RETS->responce_debug = TRUE;
        $display = '';
        // Check to see if we are importing a single class.
        //echo 'Class ID:'.$class_id;
        $classtable = $this->get_table_class($class_id);
        if (count($classtable) == 0) {
            echo 'No Class Found to Import' . "\r\n";
            die;
        }
        //print_r($classtable);die;
        foreach ($classtable as $class) {
            $this->last_update = $class['last_update'];;
            if (isset($_GET['rets_timezone_offset'])) {
                $offset = $_GET['rets_timezone_offset'];;
                $offset = $offset * 3600;;
                $this->last_update = $this->last_update - $offset;
            }
            $this->class_id = $class['class_id'];
            $this->class_name = $class['class_name'];
            $this->or_class_id = $class['class_id_import'];
            $this->resource_id = $class['resource_id'];
            $this->default_agent = $class['default_agent'];
            $this->listing_title = $class['listing_title'];
            $this->modified_map = $class['modified_map'];
            //$this->metadata_version = $class['metadata_version'];
            // Make sure Open-Realty Class ID is set
            if (!$this->or_class_id > 0) {
                echo 'Fatal Error:Class ' . $this->class_id . ' not mapped to an Open-Realty Class' . "\r\n";
                return;
            }
            //Check for Metadata Updates
            if (isset($_GET['force_metadataupdate']) && $_GET['force_metadataupdate'] == 1) {
                $force_update = true;
            } else {
                $force_update = false;
            }
            echo str_replace("<br />", "\n", $this->check_metadata_updates($this->resource_id, $this->class_id, false, $force_update));
            // Make sure default agent is set
            if (!$this->default_agent > 0) {
                echo 'Fatal Error:No default agent set for class ' . $this->class_id . "\r\n";
                return;
            }
            // Get Resource Name
            $this->resource_name = $this->get_resource_name($this->resource_id);
            // Get Photo Object Name
            $this->object_name = $this->get_photo_object($this->resource_id);
            // Get Account Information
            //$this->get_table_account($this->resource_id);
            //echo get_class($this);
            //echo 'Account ID: "'.$this->account.'"';
            // Ok get search critera
            $this->search_criteria = $this->get_search_values($this->class_id);
            // GET OR Fields
            $this->fields_or = $this->get_field_list();
            // Load metadata
            $this->get_stored_metadata();
            // Make sure keyfield is mapped
            if (!isset($this->mapped_or_fields[$this->keyfield])) {
                echo 'Warning:  RETS Key Field Not Mapped.' . "\r\n";
                //return;
            }
            // Get List of Open-Realty Agent IDS with RETS_AGENT_ID set...
            $this->or_agent_mapping = $this->get_agentid_maps();
            $searchQuery = implode(',', $this->searchArgs);
            //Show start of import in log
            echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Running Debug Query' . "\r\n";
            //echo(str_repeat(' ',256));flush();
            // Make RETS connection

            $update_start_time = microtime(true);

            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );

            if (!$result) {
                echo 'RETS login to ' . $this->url . ' with username "' . $this->account . '" failed' . "\r\n";
                return;
            }

            echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Get List of KeyFields matching search criteria' . "\r\n";
            // Build DMLQ to get list of ALL listing in this class that match search. USED ONLY FOR CLEANUP
            $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [], $query);
            $this->RETS->path = $this->RETS->RETS_login_array['Search'];
            $this->RETS->RETS_CONNECT();

            if ($this->RETS->LAST_ERROR_CODE != 0) {
                echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE . "\r\n";
                $this->RETS->rets_logout();
            } else {
                $display = print_r($this->RETS->response_SXML, true);
                $this->RETS->rets_logout();
                return $display;
            }
            // OK We have a list of MLS Numbers now search on this..
        }
    }

    public function show_rets_criteria_query($class_id)
    {
        //Retrieves and displays the presently set RETS query (Import Criteria)
        // as well as the full DMQL that will be sent to the RETS server.
        $display = '';
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $this->RETS = new RETSENGINE();

        $classtable = $this->get_table_class($class_id);
        if (count($classtable) == 0) {
            echo 'Class is not setup yet. Unable to process' . "\r\n";
            die;
        }

        foreach ($classtable as $class) {
            $this->class_id = $class['class_id'];
            $this->class_name = $class['class_name'];
            $this->resource_id = $class['resource_id'];

            // Get Resource Name
            $this->resource_name = $this->get_resource_name($this->resource_id);

            // Get search critera
            $this->search_criteria = $this->get_search_values($this->class_id);

            // GET OR Fields (if you remove this, it nukes your field map)
            $this->fields_or = $this->get_field_list();

            // Load metadata
            $this->get_stored_metadata();

            // Make sure keyfield is mapped
            if (!isset($this->mapped_or_fields[$this->keyfield])) {
                echo 'Fatal Error: Aborting import, RETS Key Field Not Mapped.<br />' . "\r\n";
                return;
            }
            // Get List of Open-Realty Agent IDS with RETS_AGENT_ID set...
            $this->or_agent_mapping = $this->get_agentid_maps();
            //build the query
            $searchQuery = implode(',', $this->searchArgs);

            $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [$this->keyfield], $searchQuery);

            $query = urldecode($this->RETS->query);

            $display .= "Specific Query (Import Criteria):" . BR . BR . $searchQuery . BR . BR . BR;
            $display .= "FULL DMQL:" . BR . BR . $query;
        }

        return $display;
    }


    public function show_rets_criteria_count($class_id)
    {
        //Retrieves and displays the number of listings that meet the Import Criteria
        //selected for this Property Class
        $display = '';
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $this->RETS = new RETSENGINE();

        $classtable = $this->get_table_class($class_id);
        if (count($classtable) == 0) {
            echo 'Class is not setup ' . "\r\n";
            die;
        }

        foreach ($classtable as $class) {
            $this->class_id = $class['class_id'];
            $this->class_name = $class['class_name'];
            $this->resource_id = $class['resource_id'];

            // Get Resource Name
            $this->resource_name = $this->get_resource_name($this->resource_id);

            // Get search critera
            $this->search_criteria = $this->get_search_values($this->class_id);

            // GET OR Fields (if you remove this, it nukes your field map)
            $this->fields_or = $this->get_field_list();

            // Load metadata
            $this->get_stored_metadata();

            // Make sure keyfield is mapped
            if (!isset($this->mapped_or_fields[$this->keyfield])) {
                echo 'Fatal Error: Aborting import, RETS Key Field Not Mapped.<br />' . "\r\n";
                return;
            }
            // Get List of Open-Realty Agent IDS with RETS_AGENT_ID set...
            $this->or_agent_mapping = $this->get_agentid_maps();
            //build the query
            $searchQuery = implode(',', $this->searchArgs);

            // Get Account Information
            $account = $this->get_table_account($this->resource_id);

            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );

            $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [$this->keyfield], $searchQuery);

            if (!$result) {
                $display = 'RETS login to ' . $this->url . ' with username "' . $this->account . '" failed.<br />' . "\r\n";
                return;
            } else {
                $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                $this->RETS->RETS_CONNECT();
                $keyXML = $this->RETS->response_SXML;

                //Get Data Delimiter
                $delimiter = (int)$keyXML->{'DELIMITER'}['value'];

                $keyArray = [];

                foreach ($keyXML->xpath('//DATA') as $key) {
                    $keyArray[] = trim($key, chr($delimiter));
                }

                $display .= "(" . Count($keyArray) . ")";
                $display .= " listings match your present Import Criteria: ";
            }
        }

        return $display;
    }


    public function run_loader()
    {

        // if suPHP this no workey.
        if (function_exists('apache_setenv')) {
            //apache_setenv('no-gzip', 1);
        }

        if (extension_loaded('zlib')) {
            @ini_set('zlib.output_compression', false);
        }


        // kill any output buffering... who knows what you have set or where
        while (@ob_end_flush());

        @ini_set('implicit_flush', true);
        ob_implicit_flush(true);

        ob_start();

        //Make sure the output is properly announced
        header('Content-Type: text/html; charset=UTF-8');
        header('Cache-Control: no-cache');

        //dump some chars to apache
        echo (str_repeat(' ', 4096));
        ob_flush();
        flush();


        require_once(dirname(__FILE__) . "/trengine/engine.php");
        require_once(dirname(__FILE__) . "/remapper.php");
        $remapper = new remapper();
        $this->RETS = new RETSENGINE();
        $display = '';
        // Check to see if we are importing a single class.
        $class_id = null;
        if (isset($_GET['pclass']) && is_numeric($_GET['pclass'])) {
            $class_id = $_GET['pclass'];
        }
        $classtable = $this->get_table_class($class_id);
        if (count($classtable) == 0) {
            echo 'No Class Found to Import.<br />';
        }
        foreach ($classtable as $class) {
            $this->last_update = $class['last_update'];;
            if (isset($_GET['rets_timezone_offset'])) {
                $offset = $_GET['rets_timezone_offset'];;
                $offset = $offset * 3600;;
                $this->last_update = $this->last_update - $offset;
            }
            $this->class_id = $class['class_id'];
            $this->class_name = $class['class_name'];
            $this->or_class_id = $class['class_id_import'];
            $this->resource_id = $class['resource_id'];
            $this->default_agent = $class['default_agent'];
            $this->listing_title = $class['listing_title'];
            $this->modified_map = $class['modified_map'];
            $this->visible_name = $class['visible_name'];
            //$this->metadata_version = $class['metadata_version'];
            // Make sure Open-Realty Class ID is set
            if (!$this->or_class_id > 0) {
                echo 'Fatal Error:Class ' . $this->class_id . ' not mapped to an Open-Realty Class.<br />';
                return;
            }
            //Check for Metadata Updates
            if (isset($_GET['force_metadataupdate']) && $_GET['force_metadataupdate'] == 1) {
                $force_update = true;
            } else {
                $force_update = false;
            }
            echo $this->check_metadata_updates($this->resource_id, $this->class_id, false, $force_update);
            echo (str_repeat(' ', 128));
            ob_flush();
            flush();

            // Make sure default agent is set
            if (!$this->default_agent > 0) {
                echo 'Fatal Error:No default agent set for class ' . $this->class_id . '.<br />';
                return;
            }
            // Get Resource Name
            $this->resource_name = $this->get_resource_name($this->resource_id);
            // Get Photo Object Name
            $this->object_name = $this->get_photo_object($this->resource_id);
            // Get Account Information
            $account = $this->get_table_account($this->resource_id);
            // Ok get search critera
            $this->search_criteria = $this->get_search_values($this->class_id);
            // GET OR Fields
            $this->fields_or = $this->get_field_list();
            // Load metadata
            $this->get_stored_metadata();
            // Make sure keyfield is mapped
            if (!isset($this->mapped_or_fields[$this->keyfield])) {
                echo 'Fatal Error: Aboritng import, RETS Key Field Not Mapped.<br />' . "\r\n";
                return;
            }
            // Get List of Open-Realty Agent IDS with RETS_AGENT_ID set...
            $this->or_agent_mapping = $this->get_agentid_maps();
            $searchQuery = implode(',', $this->searchArgs);
            //Show start of import in log
            echo $this->log_action('RETS IMPORT ' . $this->account . ' / ID# ' . $this->class_id . ' - (' . $this->class_name . ') ' . $this->visible_name . ': Starting Import') . '<br />' . "\r\n";
            echo (str_repeat(' ', 128));
            ob_flush();
            flush();
            // Make RETS connection

            $update_start_time = microtime(true);

            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );

            if (!$result) {
                echo 'RETS login to ' . $this->url . ' with username "' . $this->account . '" failed.<br />' . "\r\n";
                return;
            }

            echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Get List of KeyFields matching search criteria.<br />' . "\r\n";
            echo (str_repeat(' ', 128));
            ob_flush();
            flush();

            // Build DMLQ to get list of ALL listing in this class that match search. USED ONLY FOR CLEANUP
            $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [$this->keyfield], $searchQuery);

            $this->RETS->path = $this->RETS->RETS_login_array['Search'];
            $this->RETS->RETS_CONNECT();

            if ($this->RETS->LAST_ERROR_CODE != 0) {
                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                if (!isset($_GET['skipcleanup'])) {
                    // Get List of Listings that exist in OR but not in the MLS
                    $cleanup_debug = false;
                    $listing_ids = $this->cleanup_get_listing_ids([], $cleanup_debug);

                    if (count($listing_ids) == 0) {
                        echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1 - No Record Cleanup: No listings to remove durring cleanup<br />' . "\r\n";
                    } else {
                        foreach ($listing_ids as $listing_id) {
                            $status = $this->delete_listing($listing_id);
                            if (!$status) {
                                echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': <font color="red">Listing ' . $listing_id . ' not deleted.</font><br />' . "\r\n";
                                echo (str_repeat(' ', 128));
                                ob_flush();
                                flush();
                            } else {
                                echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Listing ' . $listing_id . ' deleted.<br />' . "\r\n";
                                echo (str_repeat(' ', 128));
                                ob_flush();
                                flush();
                            }
                        }
                    }
                }

                $this->RETS->rets_logout();
                continue;
            }
            // OK We have a list of MLS Numbers now search on this..
            $keyXML = $this->RETS->response_SXML;
            //Offset Code test
            //echo '<pre>'.print_r($keyXML,TRUE).'</pre>';
            //Check for MaxRows
            $maxrow = $keyXML->xpath('//MAXROWS');
            $maxrowCount = count($maxrow);
            $offset = 1;
            if ($maxrowCount === 1) {
                $keyArrayCleanup = [];
                $failedOffset = 0;
                while ($maxrowCount == 1) {
                    //We Hit a server with Max Rows set on Key Field BAD BAD BAD
                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: WARNING YOUR RETS SERVER ENFORCED A LIMIT ON SEARCHS THAT SELECT ONLY THE RESOURCES KEYFIELD. THIS IS EXTREMLY BAD AS THERE IS NO WAY TO GAURANTEE ALL LISTINGS ARE RECIEVED.. TALK TO YOUR MLS ABOUT THIS.') . '<br />' . "\r\n";
                    //Ok so RERUN THE QUERY WITH OFFSET=1
                    $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [$this->keyfield], $searchQuery, $offset);
                    $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                    $this->RETS->RETS_CONNECT();

                    if ($this->RETS->LAST_ERROR_CODE != 0 && $this->RETS->LAST_ERROR_CODE != 20208) {
                        echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                        $this->RETS->rets_logout();
                        echo (str_repeat(' ', 128));
                        ob_flush();
                        flush();
                        die;
                    }
                    // OK We have a list of MLS Numbers now search on this..
                    $keyXML = $this->RETS->response_SXML;
                    //Offset Code test
                    //echo '<pre>'.print_r($keyXML,TRUE).'</pre>';
                    //Check for MaxRows
                    $maxrow = $keyXML->xpath('//MAXROWS');
                    $maxrowCount = count($maxrow);
                    // Get Field Delim
                    $delimiter = (int)$keyXML->{'DELIMITER'}['value'];
                    foreach ($keyXML->xpath('//DATA') as $key) {
                        $keyvalue = trim($key, chr($delimiter));
                        if (!in_array($keyvalue, $keyArrayCleanup)) {
                            $keyArrayCleanup[] = $keyvalue;
                        }
                    }
                    $LastOffset = $offset;
                    $offset = count($keyArrayCleanup) + 1;
                    //If we get no more listings in two attempts abort
                    if ($offset == $LastOffset) {
                        $failedOffset++;
                        if ($failedOffset > 1) {
                            echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: YOUR SERVER ENFORCED A LIMIT HOWEVER IT IS NOT FOLLOWING OFFSET RULES. VERY BAD!!! YOU WILL NOT GET ALL DATA!!.') . '<br />' . "\r\n";
                            echo (str_repeat(' ', 128));
                            ob_flush();
                            flush();
                            $maxrowCount = 0;
                        }
                    }
                }
            } else {
                // Get Field Delim
                $delimiter = (int)$keyXML->{'DELIMITER'}['value'];

                $keyArrayCleanup = [];

                foreach ($keyXML->xpath('//DATA') as $key) {
                    $keyArrayCleanup[] = trim($key, chr($delimiter));
                }
            }


            // Get list of all listings in this class that match search that need updating.
            if ($this->last_update > 0 && (!isset($_GET['listing_last_update']) ||  (isset($_GET['listing_last_update']) && $_GET['listing_last_update'] > 0)) && $this->modified_map == 0) {
                // Set Local Timezone to GMT
                $local_tz = date_default_timezone_get();
                date_default_timezone_set('GMT');

                if (isset($_GET['listing_last_update'])) {
                    $searchQuery .= ',(' . $this->listingupdate_field . '=' . substr(date('c', intval($_GET['listing_last_update'])), 0, -6) . '+)';
                } else {
                    $searchQuery .= ',(' . $this->listingupdate_field . '=' . substr(date('c', $this->last_update), 0, -6) . '+)';
                }
                // Set Timezone back to normal
                date_default_timezone_set($local_tz);
                $this->RETS->build_DMQL2_query(
                    $this->resource_name,
                    $this->class_name,
                    [$this->keyfield],
                    $searchQuery
                );
                $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                $this->RETS->RETS_CONNECT();

                if ($this->RETS->LAST_ERROR_CODE != 0 && $this->RETS->LAST_ERROR_CODE != 20208 && $this->RETS->LAST_ERROR_CODE != 20201) {
                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 2: RETS FATAL ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                    die;
                } else {
                    // OK We have a list of MLS Numbers now search on this..
                    $keyXML = $this->RETS->response_SXML;
                    $maxrow = $keyXML->xpath('//MAXROWS');
                    $maxrowCount = count($maxrow);
                    $offset = 1;
                    if ($maxrowCount === 1) {
                        $keyArray = [];
                        $failedOffset = 0;
                        while ($maxrowCount == 1) {
                            //We Hit a server with Max Rows set on Key Field BAD BAD BAD

                            echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 2: WARNING YOUR RETS SERVER ENFORCED A LIMIT ON SEARCHS THAT SELECT ONLY THE RESOURCES KEYFIELD. THIS IS EXTREMLY BAD AS THERE IS NO WAY TO GAURANTEE ALL LISTINGS ARE RECIEVED.. TALK TO YOUR MLS ABOUT THIS.') . '<br />' . "\r\n";
                            //Ok so RERUN THE QUERY WITH OFFSET=1
                            $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [$this->keyfield], $searchQuery, $offset);
                            $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                            $this->RETS->RETS_CONNECT();

                            if ($this->RETS->LAST_ERROR_CODE != 0 && $this->RETS->LAST_ERROR_CODE != 20208) {
                                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 2: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                                $this->RETS->rets_logout();
                                die;
                            }
                            // OK We have a list of MLS Numbers now search on this..
                            $keyXML = $this->RETS->response_SXML;
                            //Offset Code test
                            //echo '<pre>'.print_r($keyXML,TRUE).'</pre>';
                            //Check for MaxRows
                            $maxrow = $keyXML->xpath('//MAXROWS');
                            $maxrowCount = count($maxrow);
                            // Get Field Delim
                            $delimiter = (int)$keyXML->{'DELIMITER'}['value'];
                            foreach ($keyXML->xpath('//DATA') as $key) {
                                $keyvalue = trim($key, chr($delimiter));
                                if (!in_array($keyvalue, $keyArray)) {
                                    $keyArray[] = $keyvalue;
                                }
                            }
                            $LastOffset = $offset;
                            $offset = count($keyArray) + 1;
                            //If we get no more listings in two attempts abort
                            if ($offset == $LastOffset) {
                                $failedOffset++;
                                if ($failedOffset > 1) {
                                    $maxrowCount = 0;
                                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 2: YOUR SERVER ENFORCED A LIMIT HOWEVER IT IS NOT FOLLOWING OFFSET RULES. VERY BAD!!! YOU WILL NOT GET ALL DATA!!.') . '<br />' . "\r\n";
                                }
                            }
                        }
                    } else {
                        // Get Field Delim
                        $delimiter = (int)$keyXML->{'DELIMITER'}['value'];

                        $keyArray = [];

                        foreach ($keyXML->xpath('//DATA') as $key) {
                            $keyArray[] = trim($key, chr($delimiter));
                        }
                    }
                }
            } else {
                // Inital grab use the full array, this saves us a useless get from the server for the same IDs we already have.
                $keyArray = $keyArrayCleanup;
            }

            echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': ' . count($keyArray) . ' listings found.') . '<br />' . "\r\n";
            // Ok We have an array of keys so now split them into chuncks of 200
            $keysMArray = array_chunk($keyArray, $this->listing_batch_size);
            echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': ' . count($keysMArray) . ' batch jobs created.<br />' . "\r\n";
            echo (str_repeat(' ', 128));
            ob_flush();
            flush();
            $searchFields = array_flip($this->mapped_fields);
            // make sure Agent ID Field is in the array.
            if (!in_array($this->agentid_field, $searchFields)) {
                if ($this->agentid_field == '') {
                    echo $this->log_action('ERROR: Skipping Class, No Agent ID Field Selected on Update Criteria Tab.') . '<br />' . "\r\n";
                }
                $searchFields[] = $this->agentid_field;
            }
            //New Listings Array - Merge this with photo KeyArray to make sure we get photos for listings that have chagned statuses.
            $photos_new_listings = [];
            if (!isset($_GET['photoupdate_only']) || $_GET['photoupdate_only'] != 1) {
                foreach ($keysMArray as $batch_id => $keys) {
                    echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Batch ' . ($batch_id + 1) . ' of ' . count($keysMArray) . ' started.<br />' . "\r\n";
                    echo (str_repeat(' ', 128));
                    ob_flush();
                    flush();
                    // Build Search String
                    if ($this->keyfield_datatype == 'Character' || strpos($this->url, 'interealty') !== false) {
                        $searchQuery = '(' . $this->keyfield . '=' . implode(',', $keys) . ')';
                    } else {
                        foreach ($keys as $k => $key) {
                            $keys[$k] = '(' . $this->keyfield . '=' . $key . ')';
                        }
                        $searchQuery = '(' . implode('|', $keys) . ')';
                    }

                    $this->RETS->build_DMQL2_query(
                        $this->resource_name,
                        $this->class_name,
                        $searchFields,
                        $searchQuery
                    );
                    $this->RETS->RETS_CONNECT();
                    //Handle 20203 Errors when user do not have access to a field..
                    //Bad non standard code but saves time.
                    while ($this->RETS->LAST_ERROR_CODE == 20203) {
                        $field_found = preg_match('/User does not have access to Field named (.*?)\./', $this->RETS->LAST_ERROR_MESSAGE, $field_matches);
                        if ($field_found == 1) {
                            $field_name = trim($field_matches[1]);
                            if (isset($this->mapped_fields[$field_name])) {
                                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 3: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 3: Auto Unmapping of Field ' . $field_name) . '<br />' . "\r\n";
                                echo (str_repeat(' ', 128));
                                ob_flush();
                                flush();
                                //Get App Field ID for use in the UnMap App Field Call
                                $sql = 'SELECT metadata_orfield FROM transparentrets_metadata WHERE metadata_fieldname = ' . $this->conn->qstr($field_name) . ' AND class_id = ' . $this->class_id;
                                $recordSet = $this->conn->Execute($sql);

                                if (!$recordSet) {
                                    $this->log_error($sql);
                                }
                                $AppFieldID = $recordSet->fields('metadata_orfield');

                                //UnMap Field in App
                                $this->app_field_unmap($AppFieldID, $this->or_class_id);
                                //Unmap Field in TransparnetRETS
                                $sql = 'UPDATE transparentrets_metadata SET metadata_orfield = 0 WHERE metadata_fieldname = ' . $this->conn->qstr($field_name) . ' AND class_id = ' . $this->class_id;
                                $recordSet = $this->conn->Execute($sql);

                                if (!$recordSet) {
                                    $this->log_error($sql);
                                }

                                //Remove Mapping from Memory
                                unset($this->mapped_fields[$field_name]);
                                //Build New SearchField List
                                $searchFields = array_flip($this->mapped_fields);
                                // make sure Agent ID Field is in the array.
                                // TODO: Make sure Agent ID Field is set, or we end up with a corrupted Select.
                                if (!in_array($this->agentid_field, $searchFields)) {
                                    $searchFields[] = $this->agentid_field;
                                }

                                $this->RETS->build_DMQL2_query(
                                    $this->resource_name,
                                    $this->class_name,
                                    $searchFields,
                                    $searchQuery
                                );
                                $this->RETS->RETS_CONNECT();
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if ($this->RETS->LAST_ERROR_CODE != 0 && $this->RETS->LAST_ERROR_CODE != 20208 && $this->RETS->LAST_ERROR_CODE != 20201) {
                        echo $this->log_action('RETS IMPORT FATAL ERROR ' . $this->account . ' / ' . $this->class_name . ': Step 3: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />';
                        die;
                    }

                    $listingXML = $this->RETS->response_SXML;
                    // Get Field Delim
                    $delimiter = (int)$listingXML->{'DELIMITER'}['value'];
                    // Get Column List
                    $retscols = [];

                    foreach ($listingXML->xpath('//COLUMNS') as $key) {
                        $retscols = explode(chr($delimiter), trim($key, chr($delimiter)));
                    }

                    $this->listings = [];

                    foreach ($listingXML->xpath('//DATA') as $x => $key) {
                        $listing = explode(chr($delimiter), $key);
                        // Drop first and last elements as tehy will be null. Can not trim though incase the first or last field values are empty as trim will remove them as it takes more then one delimiter off.
                        array_shift($listing);
                        array_pop($listing);
                        foreach ($listing as $y => $lfield) {
                            $this->listings[$x][$retscols[$y]] = $lfield;
                        }
                    }
                    //Remapper
                    $this->listings = $remapper->remap($this->listings, $this->class_id);
                    // Import Listings

                    $new_listings = $this->import_listings();
                    $photos_new_listings = array_merge($photos_new_listings, $new_listings);
                }
            }

            if (!isset($_GET['listingupdate_only']) || $_GET['listingupdate_only'] != 1) {
                // Deal with Image Updates
                // Get KeyField Values for listings with a modified photo date > last update time if useing a photo update field
                //TODO: Make sure if photo_last_updat=0 that we use the full array of listings we already have instead of querying the server again.
                //if ($this->last_update > 0 && (!isset($_GET['listing_last_update']) ||  (isset($_GET['listing_last_update']) && $_GET['listing_last_update'] > 0)) && $this->modified_map==0) {
                if (($this->photoupdate_field != '' && $this->last_update > 0) || (isset($_GET['photo_last_update']) && $this->photoupdate_field != '' && $_GET['photo_last_update'] > 0)) {
                    // Get list of all listings in this class that match search that need updating.
                    $local_tz = date_default_timezone_get();
                    date_default_timezone_set('GMT');

                    $searchQuery = implode(',', $this->searchArgs);

                    if (isset($_GET['photo_last_update'])) {
                        $searchQuery .= ',(' . $this->photoupdate_field . '=' . substr(date('c', intval($_GET['photo_last_update'])), 0, -6) . '+)';
                    } else {
                        $searchQuery .= ',(' . $this->photoupdate_field . '=' . substr(date('c', $this->last_update), 0, -6) . '+)';
                    }

                    date_default_timezone_set($local_tz);
                    $this->RETS->build_DMQL2_query(
                        $this->resource_name,
                        $this->class_name,
                        [$this->keyfield],
                        $searchQuery
                    );
                    $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                    $this->RETS->RETS_CONNECT();

                    if ($this->RETS->LAST_ERROR_CODE != 0) {
                        echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 4: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                        $keyArray = [];
                    } else {
                        // OK We have a list of MLS Numbers now search on this..
                        $keyXML = $this->RETS->response_SXML;
                        $maxrow = $keyXML->xpath('//MAXROWS');
                        $maxrowCount = count($maxrow);
                        $offset = 1;
                        if ($maxrowCount === 1) {
                            $keyArray = [];
                            $failedOffset = 0;
                            while ($maxrowCount == 1) {
                                //We Hit a server with Max Rows set on Key Field BAD BAD BAD
                                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 4: WARNING YOUR RETS SERVER ENFORCED A LIMIT ON SEARCHS THAT SELECT ONLY THE RESOURCES KEYFIELD. THIS IS EXTREMLY BAD AS THERE IS NO WAY TO GAURANTEE ALL LISTINGS ARE RECIEVED.. TALK TO YOUR MLS ABOUT THIS.') . '<br />' . "\r\n";
                                //Ok so RERUN THE QUERY WITH OFFSET=1
                                $this->RETS->build_DMQL2_query($this->resource_name, $this->class_name, [$this->keyfield], $searchQuery, $offset);
                                $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                                $this->RETS->RETS_CONNECT();

                                if ($this->RETS->LAST_ERROR_CODE != 0) {
                                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 4: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                                    $this->RETS->rets_logout();
                                    continue;
                                }
                                // OK We have a list of MLS Numbers now search on this..
                                $keyXML = $this->RETS->response_SXML;
                                //Offset Code test
                                //echo '<pre>'.print_r($keyXML,TRUE).'</pre>';
                                //Check for MaxRows
                                $maxrow = $keyXML->xpath('//MAXROWS');
                                $maxrowCount = count($maxrow);
                                // Get Field Delim
                                $delimiter = (int)$keyXML->{'DELIMITER'}['value'];
                                foreach ($keyXML->xpath('//DATA') as $key) {
                                    $keyvalue = trim($key, chr($delimiter));
                                    if (!in_array($keyvalue, $keyArray)) {
                                        $keyArray[] = $keyvalue;
                                    }
                                }
                                $LastOffset = $offset;
                                $offset = count($keyArray) + 1;
                                //If we get no more listings in two attempts abort
                                if ($offset == $LastOffset) {
                                    $failedOffset++;
                                    if ($failedOffset > 1) {
                                        $maxrowCount = 0;
                                        echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 4: YOUR SERVER ENFORCED A LIMIT HOWEVER IT IS NOT FOLLOWING OFFSET RULES. VERY BAD!!! YOU WILL NOT GET ALL DATA!!.') . '<br />' . "\r\n";
                                    }
                                }
                            }
                        } else {
                            // Get Field Delim
                            $delimiter = (int)$keyXML->{'DELIMITER'}['value'];

                            $keyArray = [];

                            foreach ($keyXML->xpath('//DATA') as $key) {
                                $keyArray[] = trim($key, chr($delimiter));
                            }
                        }
                    }
                }
                $keyArray = array_merge($photos_new_listings, $keyArray);
                if (count($keyArray) == 0) {
                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': No images to update.') . '<br />' . "\r\n";
                    echo (str_repeat(' ', 128));
                    ob_flush();
                    flush();
                } else {
                    $this->get_listing_ids($keyArray);
                    $this->get_listing_owner_ids($keyArray);

                    $photobatch = array_chunk($keyArray, $this->photo_batch_size);

                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': ' . count($photobatch)
                        . ' photo batch jobs created.') . '<br />' . "\r\n";
                    echo (str_repeat(' ', 128));
                    ob_flush();
                    flush();
                    foreach ($photobatch as $pbcount => $batch) {
                        echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Batch '
                            . ($pbcount + 1) . ' of ' . count($photobatch) . ' for photos started.<br />' . "\r\n";
                        echo (str_repeat(' ', 128));
                        ob_flush();
                        flush();
                        $resourceSet = [];

                        foreach ($batch as $retsid) {
                            $resourceSet[] = $retsid . ':*';
                        }
                        $this->images = [];
                        $this->imagesdec = [];
                        $getObjres = $this->RETS->getObjects($this->resource_name, $this->object_name, $resourceSet, $this->image_location, $this->image_location_force_https);
                        $this->images = $getObjres[0];
                        $this->imagesdec = $getObjres[1];
                        unset($getObjres);
                        $this->delete_images();
                        $this->import_images();
                    }
                }
            } else {
                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Skipping Photo Updates as directed.') . '<br />' . "\r\n";
                echo (str_repeat(' ', 128));
                ob_flush();
                flush();
            }
            // Run Cleanup code
            if (!isset($_GET['skipcleanup']) && !empty($keyArrayCleanup)) {
                // Get List of Listings that exist in OR but not in the MLS
                $cleanup_debug = false;
                $listing_ids = $this->cleanup_get_listing_ids($keyArrayCleanup, $cleanup_debug);

                if (count($listing_ids) == 0) {
                    echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 5: No listings to remove durring cleanup<br />' . "\r\n";
                } else {
                    foreach ($listing_ids as $listing_id) {
                        $status = $this->delete_listing($listing_id);
                        if (!$status) {
                            echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': <font color="red">Listing ' . $listing_id . ' not deleted.</font><br />' . "\r\n";
                        } else {
                            echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Listing ' . $listing_id . ' deleted.<br />' . "\r\n";
                        }
                    }
                }
                echo (str_repeat(' ', 128));
                ob_flush();
                flush();
                // Check for missing listings
                $missing_ids_all = $this->cleanup_get_listing_ids_missing($keyArrayCleanup);

                $cleanup_batch = array_chunk($missing_ids_all, $this->listing_batch_size);
                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': ' . count($cleanup_batch) . ' cleanup batch jobs created.') . '<br />' . "\r\n";
                echo (str_repeat(' ', 128));
                ob_flush();
                flush();
                $searchFields = array_flip($this->mapped_fields);
                // make sure Agent ID Field is in the array.
                if (!in_array($this->agentid_field, $searchFields)) {
                    $searchFields[] = $this->agentid_field;
                }

                foreach ($cleanup_batch as $batch_id => $keys) {
                    echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Cleanup Batch ' . ($batch_id + 1) . ' of ' . count($cleanup_batch) . ' started.<br />' . "\r\n";
                    echo (str_repeat(' ', 128));
                    ob_flush();
                    flush();
                    // Build Search String
                    if ($this->keyfield_datatype == 'Character' || strpos($this->url, 'interealty') !== false) {
                        $searchQuery = '(' . $this->keyfield . '=' . implode(',', $keys) . ')';
                    } else {
                        foreach ($keys as $k => $key) {
                            $keys[$k] = '(' . $this->keyfield . '=' . $key . ')';
                        }
                        $searchQuery = '(' . implode('|', $keys) . ')';
                    }
                    $this->RETS->path = $this->RETS->RETS_login_array['Search'];
                    $this->RETS->build_DMQL2_query(
                        $this->resource_name,
                        $this->class_name,
                        $searchFields,
                        $searchQuery
                    );
                    $this->RETS->RETS_CONNECT();

                    if ($this->RETS->LAST_ERROR_CODE != 0) {
                        echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 6: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE) . '<br />' . "\r\n";
                        continue;
                    }

                    $listingXML = $this->RETS->response_SXML;
                    // Get Field Delim
                    $delimiter = (string)$listingXML->{'DELIMITER'}['value'];
                    // Get Column List
                    $retscols = [];

                    foreach ($listingXML->xpath('//COLUMNS') as $key) {
                        $retscols = explode(chr($delimiter), trim($key, chr($delimiter)));
                    }

                    $this->listings = [];

                    foreach ($listingXML->xpath('//DATA') as $x => $key) {
                        $listing = explode(chr($delimiter), $key);
                        // Drop first and last elements as tehy will be null. Can not trim though incase the first or last field values are empty as trim will remove them as it takes more then one delimiter off.
                        array_shift($listing);
                        array_pop($listing);
                        foreach ($listing as $y => $lfield) {
                            $this->listings[$x][$retscols[$y]] = $lfield;
                        }
                    }
                    //Remapper
                    $this->listings = $remapper->remap($this->listings, $this->class_id);
                    // Import Listings
                    $this->import_listings();
                }
                // Get images for cleanup listings
                if (count($missing_ids_all) == 0) {
                    echo $this->log_action('RETS IMPORT ' . $this->account . ': No images to update.') . '<br />' . "\r\n";
                } else {
                    $this->get_listing_ids($missing_ids_all);
                    $this->get_listing_owner_ids($missing_ids_all);
                    $photobatch = array_chunk($missing_ids_all, $this->photo_batch_size);
                    echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': ' . count($photobatch)
                        . ' photo batch jobs created.') . '<br />' . "\r\n";
                    echo (str_repeat(' ', 128));
                    ob_flush();
                    flush();
                    foreach ($photobatch as $pbcount => $batch) {
                        echo 'RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Cleanup Batch ' . ($pbcount + 1) . ' of ' . count($photobatch) . ' for photos started.<br />' . "\r\n";


                        echo (str_repeat(' ', 1));
                        ob_flush();
                        flush();
                        $resourceSet = [];

                        foreach ($batch as $retsid) {
                            $resourceSet[] = $retsid . ':*';
                        }
                        $this->images = [];
                        $getObjres = $this->RETS->getObjects($this->resource_name, $this->object_name, $resourceSet, $this->image_location, $this->image_location_force_https);
                        $this->images = $getObjres[0];
                        $this->imagesdec = $getObjres[1];
                        unset($getObjres);
                        $this->delete_images();
                        $this->import_images();
                    }
                }
            } else {
                echo $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Cleanup Skipped by User.') . '<br />';
            }
            // Skip udpate timestamp if updating only photos or data.

            if ((isset($_GET['force_timestamp_update']) && $_GET['force_timestamp_update'] == 1) || ((!isset($_GET['listingupdate_only']) || $_GET['listingupdate_only'] != 1) && (!isset($_GET['photoupdate_only']) || $_GET['photoupdate_only'] != 1))) {
                // Done with this class update last modified time.
                $sql = 'UPDATE transparentrets_classes 
					SET last_update =' . $this->conn->qstr($update_start_time)
                    . ' WHERE class_id = ' . $this->class_id;
                $recordSet = $this->conn->Execute($sql);

                if (!$recordSet) {
                    $this->log_error($sql);
                }
            }

            $this->clearflag_class_map_modified($this->class_id);
            echo $this->log_action('RETS IMPORT ' . $this->account . ' / ID# ' . $this->class_id . ' - (' . $this->class_name . ') ' . $this->visible_name . ': Import Completed') . '<br />';

            $this->RETS->rets_logout();
        }

        echo 'FINISHED!!';
        ob_end_flush();

        //turn buffering back on just in case. if it's not needed PHP will kill it anyway.
        ob_start();
    }
    protected function get_stored_metadata()
    {
        // Get Field Metadata-table info
        $display = '';
        $sql = 'SELECT metadata_id,metadata_fieldname,metadata_fieldtype,metadata_orfield,metadata_keyfield,metadata_interpretation,metadata_searchfield,metadata_default,metadata_listingsupdate_field,metadata_photoupdate_field,metadata_agentid_field 
				FROM transparentrets_metadata 
				WHERE class_id = ' . $this->class_id;
        $metadata = $this->conn->Execute($sql);
        if (!$metadata) {
            $this->log_error($sql);
        }
        $this->mapped_or_fields = [];
        $this->mapped_fields = [];
        $this->keyfield = '';
        $this->keyfield_datatype = '';
        $this->listingupdate_field = '';
        $this->photoupdate_field = '';
        $this->agentid_field = '';
        $this->searchArgs = [];

        while (!$metadata->EOF) {
            if ($metadata->fields['metadata_orfield'] > 0) {
                $this->mapped_fields[$metadata->fields['metadata_fieldname']] = $metadata->fields['metadata_orfield'];
            }

            if ($metadata->fields['metadata_interpretation'] == 'Lookup' || $metadata->fields['metadata_interpretation'] == 'LookupMulti' || $metadata->fields['metadata_interpretation'] == 'LookupBitstring' || $metadata->fields['metadata_interpretation'] == 'LookupBitmask') {
                $this->fieldtypes[$metadata->fields['metadata_fieldname']] = $metadata->fields['metadata_interpretation'];
            } else {
                $this->fieldtypes[$metadata->fields['metadata_fieldname']] = $metadata->fields['metadata_fieldtype'];
            }

            if ($metadata->fields['metadata_keyfield'] == 1) {
                $this->keyfield = $metadata->fields['metadata_fieldname'];
                $this->keyfield_datatype = $metadata->fields['metadata_fieldtype'];
            }
            /*metadata_listingsupdate_field BOOLEAN NOT NULL,
                metadata_photoupdate_field BOOLEAN NOT NULL,
            metadata_agentid_field BOOLEAN NOT NULL,*/
            if ($metadata->fields['metadata_listingsupdate_field'] == 1) {
                $this->listingupdate_field = $metadata->fields['metadata_fieldname'];
            }

            if ($metadata->fields['metadata_photoupdate_field'] == 1) {
                $this->photoupdate_field = $metadata->fields['metadata_fieldname'];
            }

            if ($metadata->fields['metadata_agentid_field'] == 1) {
                $this->agentid_field = $metadata->fields['metadata_fieldname'];
            }
            // Build Search Criteria
            if (isset($this->search_criteria[$metadata->fields['metadata_id']])) {
                // This field is searched on so build the search query.
                // Lookup
                // LookupMulti
                // LookupBitstring
                // LookupBitmask
                if ($metadata->fields['metadata_interpretation'] == 'Lookup' || $metadata->fields['metadata_interpretation'] == 'LookupMulti' || $metadata->fields['metadata_interpretation'] == 'LookupBitstring' || $metadata->fields['metadata_interpretation'] == 'LookupBitmask') {
                    $this->searchArgs[] = '(' . $metadata->fields['metadata_fieldname'] . '=|' . $this->search_criteria[$metadata->fields['metadata_id']] . ')';
                } else {
                    if (strpos($this->search_criteria[$metadata->fields['metadata_id']], '|') !== false) {
                        $svals = explode('|', $this->search_criteria[$metadata->fields['metadata_id']]);
                        $string_array = [];
                        foreach ($svals as $val) {
                            if ($metadata->fields['metadata_fieldname'] == 'Int' || $metadata->fields['metadata_fieldname'] == 'Decimal') {
                                $string_array[] =    '(' . $metadata->fields['metadata_fieldname'] . '=' . $val . ')';
                            } else {
                                $string_array[] =    '(' . $metadata->fields['metadata_fieldname'] . '="' . $val . '")';
                            }
                        }
                        $string_search = implode('|', $string_array);
                        $this->searchArgs[] = '(' . $string_search . ')';
                    } else {
                        $this->searchArgs[] = '(' . $metadata->fields['metadata_fieldname'] . '=' . $this->search_criteria[$metadata->fields['metadata_id']] . ')';
                    }
                }
            }
            $metadata->MoveNext();
        }

        foreach ($this->mapped_fields as $field => $fid) {
            if (!array_key_exists($fid, $this->fields_or)) {
                //Cleat Field Mapping
                $rsql = 'UPDATE transparentrets_metadata SET metadata_orfield = 0 WHERE metadata_fieldname = ' . $this->conn->qstr($field) . ' AND class_id = ' . $this->class_id;
                $rrec = $this->conn->Execute($rsql);
                if (!$rrec) {
                    $this->log_error($rsql);
                }
                $display .= $this->log_action('Unmapped RETS Field ' . $field . ' becuase field it was mapped to was removed.') . '<br />';
            } else {
                $this->mapped_or_fields[$field] = $this->fields_or[$fid];
            }
        }
        //Check for Advanced Search Criteria
        $sql = 'SELECT adv_search FROM transparentrets_classes WHERE class_id= ' . $this->class_id;
        $arec = $this->conn->Execute($sql);
        if (!$arec) {
            $this->log_error($sql);
        }
        $adv_search = $arec->fields['adv_search'];
        if (trim($adv_search) != '') {
            $this->searchArgs = [$adv_search];
        }
    }
    public function detectRETSObjects($account_id, $resource_id)
    {
        if (!is_numeric($account_id) || !is_numeric($resource_id)) {
            die('BAD REQUEST');
        }
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display = '';
        $this->get_table_account($resource_id);
        $sql = 'SELECT resource_name FROM transparentrets_resources WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);

        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $skipLogout = false;
        if (!$this->RETS->loggedon) {
            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );
        } else {
            $skipLogout = true;
            $result = true;
        }

        if ($result) {
            $objects = [];
            $objects = $this->RETS->get_objects($resource->fields['resource_name']);
            $version = $this->RETS->get_version($resource->fields['resource_name']);
            if ($version === false) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            if (count($objects) == 0) {
                //$objects.='No Objects Found';
                return 'No Objects Found';
            } else {
                //Check if we are updating the objects.
                /*
                CREATE TABLE  `or`.`transparentrets_objects` (
                `object_id` int(11) NOT NULL auto_increment,
                `object_name` varchar(255) NOT NULL,
                `object_photo` tinyint(1) NOT NULL,
                `resource_id` int(11) NOT NULL,
                PRIMARY KEY  (`object_id`)
                ) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1
                */
                $sql = 'SELECT object_name,object_id FROM transparentrets_objects WHERE resource_id = ' . $resource_id;
                $csql = $this->conn->Execute($sql);
                if (!$csql) {
                    $this->log_error($sql);
                }
                if ($csql->RecordCount() > 0) {
                    $cached_obj = [];
                    while (!$csql->EOF) {
                        $cached_obj[$csql->fields['object_id']] = $csql->fields['object_name'];
                        $csql->MoveNext();
                    }
                    //Look for removed resources
                    $removed_obj = array_diff($cached_obj, $objects);
                    foreach ($removed_obj as $id => $name) {
                        $sql = 'DELETE FROM transparentrets_objects WHERE object_id = ' . $id;
                        $dres = $this->conn->Execute($sql);
                        if (!$dres) {
                            $this->log_error($sql);
                        }
                    }
                    //Look for elements to add.
                    $add_obj = array_diff($objects, $cached_obj);
                    foreach ($add_obj as $name) {
                        $sql =
                            'INSERT INTO transparentrets_objects (object_name,resource_id,object_photo)
						VALUES (' . $this->conn->qstr($name) . ',' . $resource_id . ',0)';
                        $dres = $this->conn->Execute($sql);
                        if (!$dres) {
                            $this->log_error($sql);
                        }
                    }
                    //Update Resouce Version
                    $sql = 'UPDATE transparentrets_resources SET object_version = \'' . $version['ObjectVersion'] . '\' WHERE resource_id = ' . $resource_id;
                    $cver = $this->conn->Execute($sql);
                    if (!$cver) {
                        $this->log_error($sql);
                    }
                } else {
                    $sql =
                        'INSERT INTO transparentrets_objects (object_name,resource_id,object_photo) VALUES ';
                    $sqlA = [];

                    foreach ($objects as $cname) {
                        if (trim($cname) == '') {
                            continue;
                        }
                        if (trim(strtolower($cname)) == 'photo') {
                            $sqlA[] = '(' . $this->conn->qstr($cname) . ',' . $resource_id . ',1)';
                        } else {
                            $sqlA[] = '(' . $this->conn->qstr($cname) . ',' . $resource_id . ',0)';
                        }
                    }

                    $sql .= implode(',', $sqlA);
                    $objectSQL = $this->conn->Execute($sql);
                    if ($objectSQL) {
                        $display .= '<div class="rets_info">Objects Detected</div>';
                    } else {
                        $this->log_error($sql);
                    }
                    //Update Resouce Version
                    $sql = 'UPDATE transparentrets_resources SET object_version = \'' . $version['ObjectVersion'] . '\' WHERE resource_id = ' . $resource_id;
                    $cver = $this->conn->Execute($sql);
                    if (!$cver) {
                        $this->log_error($sql);
                    }
                }
            }
            if ($skipLogout != true) {
                $this->RETS->rets_logout();
            }
        } else {
            $display .= '<div class="rets_error">' . $this->log_action('ERROR (' . $this->RETS->LAST_ERROR_CODE . ') - ' . $this->RETS->LAST_ERROR_MESSAGE) . '</div>';
        }

        return $display;
    }
    protected function detectRETSMetadataTable($account_id, $resource_id, $class_id)
    {
        if (!is_numeric($account_id) || !is_numeric($resource_id)) {
            die('BAD REQUEST');
        }
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display = '';

        $this->get_table_account($resource_id);
        $sql = 'SELECT resource_name FROM transparentrets_resources WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);
        $sql = 'SELECT class_name FROM transparentrets_classes WHERE class_id = ' . $class_id;
        $class = $this->conn->Execute($sql);

        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $skipLogout = false;
        if (!$this->RETS->loggedon) {
            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );
        } else {
            $result = true;
            $skipLogout = true;
        }

        if ($result) {
            //Get Key Field for this resource
            $keyfield = $this->RETS->detect_key_field($resource->fields['resource_name']);
            if ($keyfield === false) {
                echo 'Key Field Not Set. This is a critical error with the RETS Server\'s setup contact your MLS.';
                die;
            }
            $metadata_table = [];
            $metadata_table = $this->RETS->get_metadatatable($resource->fields['resource_name'], $class->fields['class_name']);
            $version = $this->RETS->get_version($resource->fields['resource_name'], $class->fields['class_name']);
            if ($version === false) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            //Check if we are updating the resources();

            $sql = 'SELECT * FROM transparentrets_metadata WHERE class_id = ' . $class_id;
            $mtsql = $this->conn->Execute($sql);
            if (!$mtsql) {
                $this->log_error($sql);
            }
            if (count($metadata_table) > 0) {
                if ($mtsql->RecordCount() > 0) {
                    //Doing a Metadata Update get a list of system names on the name server
                    $RETS_sys_names = [];
                    foreach ($metadata_table as $mt) {
                        $RETS_sys_names[] = $mt['SystemName'];
                    }
                    $Local_sys_names = [];
                    while (!$mtsql->EOF) {
                        $Local_sys_names[$mtsql->fields['metadata_id']] = $mtsql->fields['metadata_fieldname'];
                        $mtsql->MoveNext();
                    }
                    $old_fields = array_diff($Local_sys_names, $RETS_sys_names);
                    //Delete Old Fields from our RETS metadata
                    foreach ($old_fields as $fkey => $fvalue) {
                        $sql = 'DELETE FROM transparentrets_metadata WHERE metadata_id =' . $fkey;
                        $fsql = $this->conn->Execute($sql);
                        if (!$fsql) {
                            $this->log_error($sql);
                        }
                    }

                    $new_fields = array_diff($RETS_sys_names, $Local_sys_names);
                    foreach ($metadata_table as $mt) {
                        if (in_array($mt['SystemName'], $new_fields)) {
                            //See if this is the key field
                            if ($mt['SystemName'] == $keyfield) {
                                $mt['keyfield'] = 1;
                            } else {
                                $mt['keyfield'] = 0;
                            }
                            $sql = 'INSERT INTO transparentrets_metadata (class_id,metadata_fieldname,metadata_fieldtype,metadata_default,
							metadata_uniquefield,metadata_searchfield,metadata_keyfield,metadata_lookupname,metadata_interpretation,metadata_required
							,metadata_longname,metadata_shortname,metadata_searchhelpid,metadata_orfield,metadata_listingsupdate_field,
							metadata_photoupdate_field,metadata_agentid_field) VALUES
							(' . intval($class_id) . ',' . $this->conn->qstr($mt['SystemName']) . ',' . $this->conn->qstr($mt['DataType']) . ',' . intval($mt['Default']) . ',
							' . intval($mt['Unique']) . ',' . intval($mt['Searchable']) . ',' . intval($mt['keyfield']) . ',' . $this->conn->qstr($mt['LookupName']) . '
							,' . $this->conn->qstr($mt['Interpretation']) . ',' . intval($mt['Required']) . ',' . $this->conn->qstr($mt['LongName']) . '
							,' . $this->conn->qstr($mt['ShortName']) . ',' . intval($mt['SearchHelpID']) . ',0,0,0,0
							)';
                            $insertmd = $this->conn->Execute($sql);
                            if (!$insertmd) {
                                $this->log_error($sql);
                            }
                        }
                    }
                    $common_fields = array_intersect($RETS_sys_names, $Local_sys_names);
                    foreach ($metadata_table as $mt) {
                        if (in_array($mt['SystemName'], $common_fields)) {
                            //See if this is the key field
                            if ($mt['SystemName'] == $keyfield) {
                                $mt['keyfield'] = 1;
                            } else {
                                $mt['keyfield'] = 0;
                            }
                            $sql = 'UPDATE transparentrets_metadata SET
									metadata_fieldtype = ' . $this->conn->qstr($mt['DataType']) . ',
									metadata_default = ' . intval($mt['Default']) . ',
									metadata_uniquefield = ' . intval($mt['Unique']) . ',
									metadata_searchfield = ' . intval($mt['Searchable']) . ',
									metadata_keyfield = ' . intval($mt['keyfield']) . ',
									metadata_lookupname = ' . $this->conn->qstr($mt['LookupName']) . ',
									metadata_interpretation = ' . $this->conn->qstr($mt['Interpretation']) . ',
									metadata_required = ' . intval($mt['Required']) . ',
									metadata_longname = ' . $this->conn->qstr($mt['LongName']) . ',
									metadata_shortname = ' . $this->conn->qstr($mt['ShortName']) . ',
									metadata_searchhelpid = ' . intval($mt['SearchHelpID']) . '
									WHERE metadata_fieldname = ' . $this->conn->qstr($mt['SystemName']) . '
									AND class_id = ' . intval($class_id) . ';';
                            $insertmd = $this->conn->Execute($sql);
                            if (!$insertmd) {
                                $this->log_error($sql);
                            }
                        }
                    }
                } else {
                    foreach ($metadata_table as $mt) {
                        //See if this is the key field
                        if ($mt['SystemName'] == $keyfield) {
                            $mt['keyfield'] = 1;
                        } else {
                            $mt['keyfield'] = 0;
                        }
                        $sql = 'INSERT INTO transparentrets_metadata (class_id,metadata_fieldname,metadata_fieldtype,metadata_default,
						metadata_uniquefield,metadata_searchfield,metadata_keyfield,metadata_lookupname,metadata_interpretation,metadata_required
						,metadata_longname,metadata_shortname,metadata_searchhelpid,metadata_orfield,metadata_listingsupdate_field,
						metadata_photoupdate_field,metadata_agentid_field) VALUES
						(' . intval($class_id) . ',' . $this->conn->qstr($mt['SystemName']) . ',' . $this->conn->qstr($mt['DataType']) . ',' . intval($mt['Default']) . ',
						' . intval($mt['Unique']) . ',' . intval($mt['Searchable']) . ',' . intval($mt['keyfield']) . ',' . $this->conn->qstr($mt['LookupName']) . '
						,' . $this->conn->qstr($mt['Interpretation']) . ',' . intval($mt['Required']) . ',' . $this->conn->qstr($mt['LongName']) . '
						,' . $this->conn->qstr($mt['ShortName']) . ',' . intval($mt['SearchHelpID']) . ',0,0,0,0
						)';
                        $insertmd = $this->conn->Execute($sql);
                        if (!$insertmd) {
                            $this->log_error($sql);
                        }
                    }
                }
            }
            #Update Class Version

            $sql = 'UPDATE transparentrets_classes SET table_version = \'' . $version['TableVersion'] . '\' WHERE class_id = ' . $class_id;
            $cver = $this->conn->Execute($sql);
            if (!$cver) {
                $this->log_error($sql);
            }
            if ($skipLogout != true) {
                $this->RETS->rets_logout();
            }
        } else {
            $display .= '<div class="rets_error">' . $this->log_action('ERROR (' . $this->RETS->LAST_ERROR_CODE . ') - ' . $this->RETS->LAST_ERROR_MESSAGE) . '</div>';
        }

        return $display;
    }
    protected function detectRETSSearchHelp($account_id, $resource_id)
    {
        if (!is_numeric($account_id) || !is_numeric($resource_id)) {
            die('BAD REQUEST');
        }
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display = '';

        $this->get_table_account($resource_id);
        $sql = 'SELECT resource_name FROM transparentrets_resources WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);

        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $skipLogout = false;
        if (!$this->RETS->loggedon) {
            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );
        } else {
            $skipLogout = true;
            $result = true;
        }

        if ($result) {
            $search_help = [];
            $search_help = $this->RETS->get_searchhelp($resource->fields['resource_name']);
            $version = $this->RETS->get_version($resource->fields['resource_name']);
            if ($version === false) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            //Check if we are updating the resources();
            $sql = 'SELECT metadata_searchhelpid FROM transparentrets_searchhelp WHERE resource_id = ' . $resource_id;
            $csql = $this->conn->Execute($sql);
            if (!$csql) {
                $this->log_error($sql);
            }
            if (count($search_help) > 0) {
                if ($csql->RecordCount() > 0) {
                    //Delete Existing Search Help and re download.
                    $sql = 'DELETE FROM transparentrets_searchhelp WHERE resource_id = ' . $resource_id;
                    $csql = $this->conn->Execute($sql);
                    if (!$csql) {
                        $this->log_error($sql);
                    }

                    $sql =
                        'INSERT INTO transparentrets_searchhelp (metadata_searchhelpid,metadata_searchhelpvalue,resource_id) VALUES ';
                    $sqlA = [];

                    foreach ($search_help as $id => $value) {
                        $sqlA[] = '(' . $this->conn->qstr($id) . ',' . $this->conn->qstr($value) . ',' . $resource_id . ')';
                    }

                    $sql .= implode(',', $sqlA);
                    $classSQL = $this->conn->Execute($sql);
                    if (!$classSQL) {
                        $this->log_error($sql);
                    }
                } else {
                    $sql =
                        'INSERT INTO transparentrets_searchhelp (metadata_searchhelpid,metadata_searchhelpvalue,resource_id) VALUES ';
                    $sqlA = [];

                    foreach ($search_help as $id => $value) {
                        $sqlA[] = '(' . $this->conn->qstr($id) . ',' . $this->conn->qstr($value) . ',' . $resource_id . ')';
                    }

                    $sql .= implode(',', $sqlA);
                    $classSQL = $this->conn->Execute($sql);
                    if ($classSQL) {
                        $display .= '<div class="rets_info">SearchHelp Detected</div>';
                    } else {
                        $this->log_error($sql);
                    }
                }
            }
            #Update Class Version
            $sql = 'UPDATE transparentrets_resources SET searchhelp_version = \'' . $version['SearchHelpVersion'] . '\' WHERE resource_id = ' . $resource_id;
            $cver = $this->conn->Execute($sql);
            if (!$cver) {
                $this->log_error($sql);
            }
            if ($skipLogout != true) {
                $this->RETS->rets_logout();
            }
        } else {
            $display .= '<div class="rets_error">' . $this->log_action('ERROR (' . $this->RETS->LAST_ERROR_CODE . ') - ' . $this->RETS->LAST_ERROR_MESSAGE) . '</div>';
        }

        return $display;
    }
    protected function detectRETSLookupType($account_id, $resource_id)
    {
        if (!is_numeric($account_id) || !is_numeric($resource_id)) {
            die('BAD REQUEST');
        }
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display = '';

        $this->resource_id;
        $sql = 'SELECT resource_name FROM transparentrets_resources WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);

        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $skipLogout = false;
        if (!$this->RETS->loggedon) {
            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );
        } else {
            $skipLogout = true;
            $result = true;
        }

        if ($result) {
            $lookuptype = [];
            $lookuptype = $this->RETS->get_lookuptype($resource->fields['resource_name']);
            $version = $this->RETS->get_version($resource->fields['resource_name']);
            if ($version === false) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            $sql = 'SELECT lookuptype_id FROM transparentrets_lookuptype WHERE resource_id = ' . $resource_id;
            $csql = $this->conn->Execute($sql);
            if (!$csql) {
                $this->log_error($sql);
            }
            if (count($lookuptype) > 0) {
                if ($csql->RecordCount() > 0) {
                    //Delete Existing Search Help and re download.
                    $display .= $this->log_action('Removing old LookupType') . '<br />';
                    $sql = 'DELETE FROM transparentrets_lookuptype WHERE resource_id = ' . $resource_id;
                    $csql = $this->conn->Execute($sql);
                    if (!$csql) {
                        $this->log_error($sql);
                    }
                    $display .= $this->log_action('Adding new LookupType') . '<br />';
                    $sql =
                        'INSERT INTO transparentrets_lookuptype (resource_id,metadata_lookupname,metadata_value,metadata_longvalue,metadata_shortvalue) VALUES ';
                    $sqlA = [];

                    foreach ($lookuptype as $id => $lt) {
                        $sqlA[] = '(' . $resource_id . ',' . $this->conn->qstr($lt['name']) . ',' . $this->conn->qstr($lt['value']) . ',' . $this->conn->qstr($lt['lvalue']) . ',' . $this->conn->qstr($lt['svalue']) . ')';
                    }

                    $sql .= implode(',', $sqlA);
                    $classSQL = $this->conn->Execute($sql);
                    if (!$classSQL) {
                        $this->log_error($sql);
                    }
                    $display .= $this->log_action('LookupType Update Complete') . '<br />';
                } else {
                    $sql =
                        'INSERT INTO transparentrets_lookuptype (resource_id,metadata_lookupname,metadata_value,metadata_longvalue,metadata_shortvalue) VALUES ';
                    $sqlA = [];

                    foreach ($lookuptype as $id => $lt) {
                        $sqlA[] = '(' . $resource_id . ',' . $this->conn->qstr($lt['name']) . ',' . $this->conn->qstr($lt['value']) . ',' . $this->conn->qstr($lt['lvalue']) . ',' . $this->conn->qstr($lt['svalue']) . ')';
                    }

                    $sql .= implode(',', $sqlA);
                    $classSQL = $this->conn->Execute($sql);
                    if ($classSQL) {
                        $display .= $this->log_action('LookupType correctly detected') . '<br />';
                    } else {
                        $this->log_error($sql);
                    }
                }
            }
            #Update Class Version
            $sql = 'UPDATE transparentrets_resources SET lookup_version = \'' . $version['LookupVersion'] . '\' WHERE resource_id = ' . $resource_id;
            $cver = $this->conn->Execute($sql);
            if (!$cver) {
                $this->log_error($sql);
            }
            if ($skipLogout != true) {
                $this->RETS->rets_logout();
            }
        } else {
            $display .= '<div class="rets_error">' . $this->log_action('ERROR (' . $this->RETS->LAST_ERROR_CODE . ') - ' . $this->RETS->LAST_ERROR_MESSAGE) . '</div>';
        }

        return $display;
    }
    public function detectRETSClasses($account_id, $resource_id)
    {
        if (!is_numeric($account_id) || !is_numeric($resource_id)) {
            die('BAD REQUEST');
        }
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display = '';

        $this->get_table_account($resource_id);
        $sql = 'SELECT resource_name FROM transparentrets_resources WHERE resource_id = ' . $resource_id;
        $resource = $this->conn->Execute($sql);

        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $skipLogout = false;
        if (!$this->RETS->loggedon) {
            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );
        } else {
            $result = true;
            $skipLogout = true;
        }

        if ($result) {
            $classes = [];
            $classes = $this->RETS->get_classes($resource->fields['resource_name']);
            if ($classes === false) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Detect Classes: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            $version = $this->RETS->get_version($resource->fields['resource_name']);
            if ($version === false) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: Detect Classes - Get Version: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            if (count($classes) == 0) {
                $display .= $this->log_action('No Classes Found durring detection') . '<br />';
            } else {
                $classes_inverse = array_flip($classes);
                //Check if we are updating the resources();
                $sql = 'SELECT class_name,class_id FROM transparentrets_classes WHERE resource_id = ' . $resource_id;
                $csql = $this->conn->Execute($sql);
                if (!$csql) {
                    $this->log_error($sql);
                }
                if ($csql->RecordCount() > 0) {
                    $cached_cls = [];
                    while (!$csql->EOF) {
                        $cached_cls[$csql->fields['class_id']] = $csql->fields['class_name'];
                        $csql->MoveNext();
                    }
                    //Look for removed resources
                    $removed_cls = array_diff($cached_cls, $classes_inverse);
                    $display .= $this->log_action('Removing Classes: ' . implode(',', $removed_cls)) . '<br />';
                    foreach ($removed_cls as $id => $name) {
                        $sql = 'DELETE FROM transparentrets_classes WHERE class_id = ' . $id;
                        $dres = $this->conn->Execute($sql);
                        if (!$dres) {
                            $this->log_error($sql);
                        }
                    }
                    //Look for elements to add.
                    $add_cls = array_diff($classes_inverse, $cached_cls);
                    $display .= $this->log_action('Adding Classes: ' . implode(',', $add_cls)) . '<br />';
                    foreach ($add_cls as $vname => $name) {
                        $sql =
                            'INSERT INTO transparentrets_classes (class_name,visible_name,resource_id,class_import,
						class_id_import,table_version,default_agent,listing_title,last_update)
						VALUES (' . $this->conn->qstr($name) . ',' . $this->conn->qstr($vname) . ',' . $resource_id . ',0,0,0,0,\'\',0)';
                        $dres = $this->conn->Execute($sql);
                        if (!$dres) {
                            $this->log_error($sql);
                        }
                    }
                    //Update Resouce Version
                    $sql = 'UPDATE transparentrets_resources SET class_version = \'' . $version['ClassVersion'] . '\' WHERE resource_id = ' . $resource_id;
                    $cver = $this->conn->Execute($sql);
                    if (!$cver) {
                        $this->log_error($sql);
                    }
                    $display .= $this->log_action('Class Update Complete') . '<br />';
                } else {
                    $sql =
                        'INSERT INTO transparentrets_classes (class_name,visible_name,resource_id,class_import,
						class_id_import,table_version,default_agent,listing_title,last_update) VALUES ';
                    $sqlA = [];

                    foreach ($classes as $cname => $vname) {
                        if (trim($cname) == '') {
                            continue;
                        }
                        $sqlA[] = '(' . $this->conn->qstr($cname) . ',' . $this->conn->qstr($vname) . ',' . $resource_id . ',0,0,0,0,\'\',0)';
                    }

                    $sql .= implode(',', $sqlA);
                    $classSQL = $this->conn->Execute($sql);
                    if ($classSQL) {
                        $display .= $this->log_action('Classes Correctly Detected') . '<br />';
                    } else {
                        $this->log_error($sql);
                    }
                    #Update Class Version
                    $sql = 'UPDATE transparentrets_resources SET class_version = \'' . $version['ClassVersion'] . '\' WHERE resource_id = ' . $resource_id;
                    $cver = $this->conn->Execute($sql);
                    if (!$cver) {
                        $this->log_error($sql);
                    }
                    $display .= $this->log_action('Class Version Updated') . '<br />';
                }
            }
            if ($skipLogout != true) {
                $this->RETS->rets_logout();
            }
        } else {
            $display .= '<div class="rets_error">' . $this->log_action('ERROR (' . $this->RETS->LAST_ERROR_CODE . ') - ' . $this->RETS->LAST_ERROR_MESSAGE) . '</div>';
        }

        return $display;
    }

    public function detectRETSResources($account_id)
    {
        if (!is_numeric($account_id)) {
            die('BAD REQUEST');
        }
        require_once(dirname(__FILE__) . "/trengine/engine.php");
        $display = '';
        $this->get_table_account(null, $account_id);

        if ($this->RETS == '') {
            $this->RETS = new RETSENGINE();
        }
        $skipLogout = false;
        if (!$this->RETS->loggedon) {
            $result = $this->RETS->loginDirect(
                $this->account,
                $this->password,
                $this->url,
                $this->application,
                $this->version,
                $this->client_password,
                $this->port,
                $this->keep_alive,
                $this->post_request,
                $this->encode_request,
                $this->rets_version,
                $this->uapassword
            );
        } else {
            $result = true;
            $skipLogout = true;
        }

        if ($result) {
            $resources = [];
            $resources = $this->RETS->get_resources();
            $version = $this->RETS->get_version();
            if (!$version) {
                if ($this->RETS->LAST_ERROR_CODE != 0) {
                    $display .= $this->log_action('RETS IMPORT ' . $this->account . ' / ' . $this->class_name . ': Step 1: RETS ERROR: ' . $this->RETS->LAST_ERROR_CODE . ' - ' . $this->RETS->LAST_ERROR_MESSAGE);
                }
                return $display;
            }
            if (count($resources) == 0) {
                $display .= $this->log_action('No Resources Found durring detection') . '<br />';
            } else {
                //Check if we are updating the resources();
                $sql = 'SELECT resource_name,resource_id 
						FROM transparentrets_resources 
						WHERE account_id = ' . $account_id;
                $rsql = $this->conn->Execute($sql);
                if ($rsql->RecordCount() > 0) {
                    $cached_res = [];
                    while (!$rsql->EOF) {
                        $cached_res[$rsql->fields['resource_id']] = $rsql->fields['resource_name'];
                        $rsql->MoveNext();
                    }
                    //Look for removed resources
                    $removed_res = array_diff($cached_res, $resources);
                    $display .= $this->log_action('Removing Resources:' . implode(',', $removed_res)) . '<br />';
                    foreach ($removed_res as $id => $name) {
                        $sql = 'DELETE FROM transparentrets_resources 
								WHERE resource_id = ' . $id;
                        $dres = $this->conn->Execute($sql);
                        if (!$dres) {
                            $this->log_error($sql);
                        }
                    }
                    //Look for elements to add.
                    $add_res = array_diff($resources, $cached_res);
                    $display .= $this->log_action('Adding Resources:' . implode(',', $add_res)) . '<br />';
                    foreach ($add_res as $id => $name) {
                        $sql = 'INSERT INTO transparentrets_resources (resource_name,account_id,resource_property,resources_version)
								VALUES (' . $this->conn->qstr($name) . ',' . $account_id . ',0,\'' . $version['ResourceVersion'] . '\')';
                        $ares = $this->conn->Execute($sql);
                        if (!$ares) {
                            $this->log_error($sql);
                        }
                    }
                    //Update Resouce Version
                    $sql = 'UPDATE transparentrets_resources 
							SET resources_version = \'' . $version['ResourceVersion'] . '\' 
							WHERE account_id = ' . $account_id;
                    $cver = $this->conn->Execute($sql);
                    if (!$cver) {
                        $this->log_error($sql);
                    }
                    $display .= $this->log_action('Resources Update Complete') . '<br />';
                } else {
                    $resourceFound = false;
                    $sql =
                        'INSERT INTO transparentrets_resources (resource_name,account_id,resource_property,resources_version) 
						VALUES ';
                    $sqlA = [];

                    foreach ($resources as $rname) {
                        if (trim($rname) == '') {
                            continue;
                        }
                        if (strtolower(trim($rname)) == 'property') {
                            $sqlA[] = '(' . $this->conn->qstr($rname) . ',' . $account_id . ',1,\'' . $version['ResourceVersion'] . '\')';
                            //We know this is the Property Resource so go get the classes to save a step.
                            $resourceFound = true;
                        } else {
                            $sqlA[] = '(' . $this->conn->qstr($rname) . ',' . $account_id . ',0,\'' . $version['ResourceVersion'] . '\')';
                        }
                    }

                    $sql .= implode(',', $sqlA);
                    $resourceSQL = $this->conn->Execute($sql);
                    if ($resourceSQL) {
                        $display .= $this->log_action('Resources Correctly Detected') . '<br />';
                        if ($resourceFound) {
                            //We know this is the Property Resource so go get the classes to save a step.
                            $sql =
                                'SELECT resource_id 
								FROM transparentrets_resources 
								WHERE account_id = ' . $account_id . ' 
								AND resource_property = 1';
                            $resourceSQL = $this->conn->Execute($sql);
                            $display .= $this->detectRETSClasses($account_id, $resourceSQL->fields['resource_id']);
                            $display .= $this->detectRETSObjects($account_id, $resourceSQL->fields['resource_id']);
                        }
                    } else {
                        $this->log_error($sql);
                    }
                }
            }
            if ($skipLogout != true) {
                $this->RETS->rets_logout();
            }
        } else {
            $display .= '<div class="rets_error">' . $this->log_action('ERROR (' . $this->RETS->LAST_ERROR_CODE . ') - ' . $this->RETS->LAST_ERROR_MESSAGE) . '</div>';
        }

        return $display;
    }
    public function config_servers_show_server_select()
    {
        $display = '';
        $display .= '<form action="index.php" method="get" id="select_server">
					<fieldset class="select_bar">
						<span>Select Server:</span> <select name="server_id"  onChange="document.getElementById(\'select_server\').submit();">';

        $sql = "SELECT account, account_id 
		FROM transparentrets_servers";
        $servers = $this->conn->Execute($sql);

        if (!$servers) {
            $this->log_error($sql);
        }

        $x = 1;

        $display .= '<option value="">None Selected</option>';

        while (!$servers->EOF) {
            $display .= '<option value="' . $servers->fields['account_id'] . '" ';

            if (isset($_GET['server_id']) && $_GET['server_id'] == $servers->fields['account_id']) {
                $display .= ' selected="selected" ';
            }

            $display .= '>' . $servers->fields['account'] . ' (' . $servers->fields['account_id']
                . ')</option>';
            $servers->MoveNext();

            $x++;
        }

        $display .= '</select>
					<input type="hidden" name="action" value="addon_transparentRETS_config_server" />
					<!--
						<input type="submit" class="trets_std_button" value="Modify Server" />
					-->	';

        $display .= '<a href="index.php?action=addon_transparentRETS_config_server" class="trets_std_button">Add New Server</a>';


        $display .= '<div class="page_name_div"> ' . $this->page_img . '	<span>' . $this->page_name . '</span></div>
					</fieldset>
				</form>';
        return $display;
    }

    public function config_servers_show_server_settings_form(
        $type = 'INSERT',
        $account = '',
        $password = '',
        $url = '',
        $client_pass = '',
        $port = 80,
        $app = 'TransparentRETS',
        $version = '1.0',
        $keepalive = 0,
        $encode = 1,
        $postreq = 1,
        $ua_pass = '',
        $listing_batch = 200,
        $photo_batch = 10,
        $rets_ver = 'RETS/1.5',
        $remote_photos = 0,
        $force_https = 0
    ) {
        $display = '';
        if ($type == 'EDIT') {
            $display .= '<form action="index.php?action=addon_transparentRETS_config_server&amp;server_id=' . $_GET['server_id'] . '" method="post">';
        } else {
            $display .= '<form action="index.php?action=addon_transparentRETS_config_server" method="post">';
        }

        $display .= '<fieldset>
					<fieldset>
						<legend>RETS Login Information</legend>
					<table>
						<tr>
							<td>
								<label onmouseover="domTT_activate(this, event, \'caption\', \'Account Name\', \'content\', \'The name of the RETS account.\', \'trail\', \'x\');" for="RETS_SERVER_ACCOUNT">Account Name: </label>
							</td>
							<td>
								<input type="text" style="width:400px;" id="RETS_SERVER_ACCOUNT" name="RETS_SERVER_ACCOUNT" value="' . $account . '" />
							</td>
						</tr>
						<tr>
							<td>
								<label onmouseover="domTT_activate(this, event, \'caption\', \'Account Password\', \'content\', \'The password of RETS account.\', \'trail\', \'x\');" for="RETS_SERVER_PASSWORD">Account Password:</label>
							</td>
							<td>
								<input type="text" name="RETS_SERVER_PASSWORD" id="RETS_SERVER_PASSWORD" value="' . $password . '" />
							</td>
						</tr>
						<tr>
							<td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Server URL\', \'content\', \'The URL of the RETS server. Example. &quot;http://demo.crt.realtors.org:6103/rets/login&quot;.\', \'trail\', \'x\');" for="RETS_SERVER_URL">Server URL:</label>
		</td><td><input type="text" style="width:400px;" name="RETS_SERVER_URL" id="RETS_SERVER_URL" value="' . $url . '" /></td></tr>
		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Default Port\', \'content\', \'The port to use if the RETS server is not specific during its reply to a login. The RETS specification identifies 6103 as the default port, but in practice, many providers use 80.\', \'trail\', \'x\');" for="RETS_DEFAULT_PORT">Default Port:</label>
		</td><td><input type="text" style="width:60px;" name="RETS_DEFAULT_PORT" id="RETS_DEFAULT_PORT" value="' . $port . '" /></td></tr>
		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'RETS Version\', \'content\', \'The RETS Version of the Server you are connecting to.\', \'trail\', \'x\');" for="RETS_VERSION">RETS Version:</label>
		</td><td><select name="RETS_VERSION" id="RETS_VERSION">';
        $RETS_VersionArray = ['RETS/1.5', 'RETS/1.7', 'RETS/1.7.2'];
        foreach ($RETS_VersionArray as $rv) {
            if ($rv == $rets_ver) {
                $display .= '<option selected="selected" value="' . $rv . '">' . $rv . '</option>';
            } else {
                $display .= '<option value="' . $rv . '">' . $rv . '</option>';
            }
        }

        $display .= '
		</select></td></tr>
		</table></fieldset>
		<fieldset><legend>RETS Optional Authentications</legend>
		<table>

		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Client Password\', \'content\', \'The  RETS 1.5 client password required by some RETS servers. You normally will not need to set this.\', \'trail\', \'x\');" for="RETS_CLIENT_PASSWORD">Client Password:</label>
			</td><td><input type="text" name="RETS_CLIENT_PASSWORD" id="RETS_CLIENT_PASSWORD" value="' . $client_pass . '" /></td></tr>
			<tr><td>
			<label onmouseover="domTT_activate(this, event, \'caption\', \'User Agent Password\', \'content\', \'The RETS 1.7 User Agent Password required by some RETS servers. You normally will not need to set this.\', \'trail\', \'x\');" for="RETS_UA_PASSWORD">User Agent Password:</label>
			</td><td><input type="text" name="RETS_UA_PASSWORD" id="RETS_UA_PASSWORD" value="' . $ua_pass . '" /></td></tr>

		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'User-Agent\', \'content\', \'The User-Agent Prodcut Name/Version of this client for use within the HTTP protocol. Some vendors require you to set this to a specific product for authentication. Defaults to &quot;TransparentRETS/1.0&quot;.\', \'trail\', \'x\');" for="APPLICATION">User-Agent:</label>
		</td><td><input type="text" name="APPLICATION" id="APPLICATION" value="' . $app . '" /> /

		<input type="text" style="width:60px;" name="VERSION" id="VERSION" value="' . $version . '" /></td></tr>
		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Remote Photos\', \'content\', \'Only link to the photos on the RETS Server, instead of downloading them all to your local server.. Not all RETS servers support this capability. If you are not sure, leave it as &quot;false&quot;. Defaults to &quot;false&quot;. \', \'trail\', \'x\');">Use Remote Photos:</label>
		</td><td><input type="radio" id="REMOTE_PHOTOS" name="REMOTE_PHOTOS" value="1"';

        if ($remote_photos == 1) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="REMOTE_PHOTOS">TRUE</label> <input type="radio" name="REMOTE_PHOTOS" id="REMOTE_PHOTOS" value="0"';

        if ($remote_photos == 0) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="REMOTE_PHOTOS">FALSE</label></td></tr>
        
        <tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Force HTTPS\', \'content\', \'Force Remove Photos to use https links istead of http. Not all RETS servers support this capability. If you are not sure, leave it as &quot;false&quot;. Defaults to &quot;false&quot;. \', \'trail\', \'x\');">Force HTTPS for Remote Photos:</label>
		</td><td><input type="radio" id="FORCE_HTTPS" name="FORCE_HTTPS" value="1"';

        if ($force_https == 1) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="FORCE_HTTPS">TRUE</label> <input type="radio" name="FORCE_HTTPS" id="FORCE_HTTPS" value="0"';

        if ($force_https == 0) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="FORCE_HTTPS">FALSE</label></td></tr>
        
		</table></fieldset>
		<fieldset><legend>Advanced Configurations (DO NOT CHANGE WITHOUT DIRECTIONS)</legend>
		<table>
			<tr>
				<td>
					<label onmouseover="domTT_activate(this, event, \'caption\', \'Keep Alive\', \'content\', \'Specify whether the RETS server supports keep-alive tcp sockets. Setting this to &quot;true&quot; will result in avoiding setting up a new tcp socket between this package and the RETS server for each communication and will result in higher performance. Not all RETS servers support this capability. If you are not sure, leave it as &quot;false&quot;. Defaults to &quot;false&quot;. \', \'trail\', \'x\');">Keep Alive:</label>
				</td>
				<td>
				<input type="radio" id="KEEP_ALIVE_SERVER_TRUE" name="KEEP_ALIVE_SERVER" value="1"';

        if ($keepalive == 1) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="KEEP_ALIVE_SERVER_TRUE">TRUE</label> <input type="radio" name="KEEP_ALIVE_SERVER" id="KEEP_ALIVE_SERVER_FALSE"value="0"';

        if ($keepalive == 0) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="KEEP_ALIVE_SERVER_FALSE">FALSE</label></td></tr>
		<tr>
			<td>
				<label onmouseover="domTT_activate(this, event, \'caption\', \'Encode Requests\', \'content\', \'Specify whether the RETS server supports requests that are URL Encoded. Typically this is set to &quot;true&quot;. If you use the default value and have problems executing queries that have values with spaces (like area=&quot;Hidden Oaks&quot;) then try setting this to &quot;false&quot;. Defaults to &quot;true&quot;. \', \'trail\', \'x\');" for="ENCODE_REQUESTS">Encode Requests:</label>
			</td>
			<td>
				<input type="radio" id="ENCODE_REQUESTS_TRUE" name="ENCODE_REQUESTS" value="1"';

        if ($encode == 1) {
            $display .= ' checked="checked" ';
        }

        $display
            .= ' /><label for="ENCODE_REQUESTS_TRUE">TRUE</label> <input type="radio" name="ENCODE_REQUESTS" id="ENCODE_REQUESTS_FALSE"value="0"';

        if ($encode == 0) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="ENCODE_REQUESTS_FALSE">FALSE</label></td></tr>
		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Post Request\', \'content\', \'Specify whether the you want to use POST or GET style HTTP requests. Some older servers have a problem with long GET style requests and even construct Digest Authentication incorrectly with GET. If you are trying to return more than 60 fields for any one listing and the package fails, but bringing back 10 fields works, then you might want to leave this set to &quot;true&quot;. Defaults to &quot;true&quot;.\', \'trail\', \'x\');" for="POST_REQUESTS">Post Request:</label>
		</td><td><input type="radio" id="POST_REQUESTS_TRUE" name="POST_REQUESTS" value="1"';

        if ($postreq == 1) {
            $display .= ' checked="checked" ';
        }

        $display
            .= ' /><label for="POST_REQUESTS_TRUE">TRUE</label> <input type="radio" name="POST_REQUESTS" id="POST_REQUESTS_FALSE" value="0"';

        if ($postreq == 0) {
            $display .= ' checked="checked" ';
        }

        $display .= ' /><label for="POST_REQUESTS_FALSE">FALSE</label></td></tr>

		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Listing Batch\', \'content\', \'The number of listings we request data for at a time. This defaults to 200 and should only be changed if directed to do so by support staff.\', \'trail\', \'x\');" for="LISTING_BATCH">Listing Batch Size:</label>
		</td><td><input type="text" style="width:40px;" name="LISTING_BATCH" id="LISTING_BATCH" value="' . $listing_batch . '" /></td></tr>
		<tr><td>
		<label onmouseover="domTT_activate(this, event, \'caption\', \'Photo Batch\', \'content\', \'The number of listings we request photos for at a time. This defaults to 10 and should only be changed if directed to do so by support staff.\', \'trail\', \'x\');" for="PHOTO_BATCH">Photo Batch Size:</label>
		</td><td><input type="text" style="width:40px;" name="PHOTO_BATCH" id="PHOTO_BATCH" value="' . $photo_batch . '" /></td></tr>

		</table></fieldset><center>';

        if ($type == 'EDIT') {
            $display .= '<input type="hidden" name="type" value="EDIT" />
						<input type="submit" class="trets_std_button" value="Save Server" />	
						 <a href="index.php?action=addon_transparentRETS_config_server&delete_aid=' . $_GET['server_id'] . '" class="trets_std_button red" >Remove Server</a>';
        } else {
            $display .= '<input type="hidden" name="type" value="INSERT" /><input type="submit" value="Insert Server" />	';
        }

        $display .= '</center>

	</fieldset>
	</form>';
        return $display;
    }

    public function update_media($object_id, $resource_id)
    {
        //Save Media Object
        $sql = 'UPDATE transparentrets_objects 
				SET object_photo = 0 
				WHERE resource_id = ' . intval($resource_id);
        $media = $this->conn->Execute($sql);
        if (!$media) {
            $this->log_error($sql);
        }
        $sql = 'UPDATE transparentrets_objects 
				SET object_photo = 1 
				WHERE resource_id = ' . intval($resource_id) . ' 
				AND object_id = ' . intval($object_id);
        $media = $this->conn->Execute($sql);
        if (!$media) {
            $this->log_error($sql);
        }
        header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Media Resource Saved.']);
        return $display;
    }

    public function update_server(
        $account,
        $password,
        $url,
        $port,
        $application,
        $version,
        $client_password,
        $keep_alive,
        $encode_request,
        $post_request,
        $uapass,
        $retsver,
        $listingbatch,
        $photobatch,
        $remote_photos,
        $force_https
    ) {
        if (isset($_GET['server_id']) && intval($_GET['server_id'] > 0)) {
            $sql = 'UPDATE transparentrets_servers
		SET account = ' . $this->conn->qstr($account) . ',
            password = ' . $this->conn->qstr($password) . ',
            url = ' . $this->conn->qstr($url) . ',
            port = ' . intval($port) . ',
            application = ' . $this->conn->qstr($application) . ',
            version = ' . $this->conn->qstr($version) . ',
            client_password = ' . $this->conn->qstr($client_password) . ',
            keep_alive = ' . intval($keep_alive) . ',
            encode_request = ' . intval($encode_request) . ',
            post_request = ' . intval($post_request) . ' ,
            uapassword = 	' . $this->conn->qstr($uapass) . ',
            rets_vers = ' . $this->conn->qstr($retsver) . ',
            listing_batch =	' . intval($listingbatch) . ',
            photo_batch = 	' . intval($photobatch) . ',
            remote_photos = ' . intval($remote_photos) . ',
            force_https = ' . intval($force_https) . '
            WHERE account_id = ' . intval($_GET['server_id']);

            $servers = $this->conn->Execute($sql);
            if (!$servers) {
                $this->log_error($sql);
            }
        }
    }
    public function insert_server(
        $account,
        $password,
        $url,
        $port,
        $application,
        $version,
        $client_password,
        $keep_alive,
        $encode_request,
        $post_request,
        $uapass,
        $retsver,
        $listingbatch,
        $photobatch,
        $remote_photos
    ) {
        $sql =
            'INSERT INTO transparentrets_servers
			(account,password,url,port,application,version,client_password,keep_alive,encode_request,post_request,uapassword
			,rets_vers,listing_batch,photo_batch,remote_photos)
			VALUES (
	'
            . $this->conn->qstr($account) . ',
	' . $this->conn->qstr($password) . ',
	' . $this->conn->qstr($url) . ',
	' . intval($port) . ',
	'
            . $this->conn->qstr($application) . ',
	' . $this->conn->qstr($version) . ',
	' . $this->conn->qstr($client_password) . ',
	'
            . intval($keep_alive) . ',
	' . intval($encode_request) . ',
	' . intval($post_request) . ',
	' . $this->conn->qstr($uapass) . ',
	' . $this->conn->qstr($retsver) . ',
	' . intval($listingbatch) . ',
	' . intval($photobatch) . ',
	' . intval($remote_photos) . '
	);';
        $servers = $this->conn->Execute($sql);

        if (!$servers) {
            $this->log_error($sql);
        }

        return $this->conn->Insert_ID();
    }

    public function delete_server($accountid)
    {
        $display = '';
        $sql = [];
        $sql['lookuptype'] =
            'DELETE FROM transparentrets_lookuptype WHERE resource_id IN (SELECT resource_id FROM transparentrets_resources WHERE account_id = ' . $accountid . ')';
        $sql['objects'] =
            'DELETE FROM transparentrets_objects WHERE resource_id IN (SELECT resource_id FROM transparentrets_resources WHERE account_id = ' . $accountid . ')';
        $sql['metadata'] =
            'DELETE FROM transparentrets_metadata WHERE class_id IN (SELECT class_id FROM transparentrets_classes WHERE resource_id IN (SELECT resource_id FROM transparentrets_resources WHERE account_id = ' . $accountid . '))';
        $sql['search'] =
            'DELETE FROM transparentrets_search WHERE class_id IN (SELECT class_id FROM transparentrets_classes WHERE resource_id IN (SELECT resource_id FROM transparentrets_resources WHERE account_id = ' . $accountid . '))';
        $sql['classes'] =
            'DELETE FROM transparentrets_classes WHERE resource_id IN (SELECT resource_id FROM transparentrets_resources WHERE account_id = ' . $accountid . ');';
        $sql['resources'] = 'DELETE FROM transparentrets_resources WHERE account_id = ' . $accountid . ';';
        $sql['account'] = 'DELETE FROM transparentrets_servers WHERE account_id = ' . $accountid . ';';

        foreach ($sql as $m => $s) {
            $rs = $this->conn->Execute($s);
            if (!$rs) {
                $this->log_error($s);
            } else {
                $display .= 'Removing ' . $m . ' success.<br />';
            }
        }

        return $display;
    }
    public function save_search_criteria($class)
    {
        $display = '';

        //First Remove old search criteria.
        $sql = "DELETE FROM transparentrets_search 
			WHERE class_id = " . intval($class);
        $frets = $this->conn->Execute($sql);

        if (!$frets) {
            $this->log_error($sql);
        }

        $sql = "SELECT metadata_id,metadata_fieldname 
			FROM transparentrets_metadata WHERE class_id = " . intval($class) . " 
			AND metadata_searchfield = 1 
			ORDER BY metadata_fieldname";
        $frets = $this->conn->Execute($sql);

        if (!$frets) {
            $this->log_error($sql);
        }

        $sqlA = [];

        while (!$frets->EOF) {
            if (
                isset($_POST[$frets->fields['metadata_fieldname']])
                && $_POST[$frets->fields['metadata_fieldname']] != ''
            ) {
                if (is_array($_POST[$frets->fields['metadata_fieldname']])) {
                    $sqlA[] = '(' . intval($class) . ',' . intval($frets->fields['metadata_id']) . ','
                        . $this->conn->qstr(implode(',', $_POST[$frets->fields['metadata_fieldname']]))
                        . ')';
                } else {
                    $sqlA[] = '(' . intval($class) . ',' . intval($frets->fields['metadata_id']) . ','
                        . $this->conn->qstr($_POST[$frets->fields['metadata_fieldname']]) . ')';
                }
            }
            $frets->MoveNext();
        }

        if (count($sqlA) > 0) {
            $sql = 'INSERT INTO transparentrets_search (class_id,metadata_id,search_value) 
			VALUES ' . implode(',', $sqlA);
            $s_insert = $this->conn->Execute($sql);
            if (!$s_insert) {
                $this->log_error($sql);
            }
        }
        //Save Advanced Search
        if (isset($_POST['transparentrets_adv_search'])) {
            $sql = 'UPDATE transparentrets_classes 
				SET adv_search = ' . $this->conn->qstr($_POST['transparentrets_adv_search']) . ' 
				WHERE class_id =' . intval($class);
            $s_update = $this->conn->Execute($sql);
            if (!$s_update) {
                $this->log_error($sql);
            }
        }
        header('Content-type: application/json');
        $display .= json_encode(['error' => "0", 'status_msg' => 'Import Criteria Saved.']);
        return $display;
    }

    public function show_search_criteria($class)
    {
        $display = '';
        $sql = 'SELECT resource_id,adv_search 
			FROM transparentrets_classes 
			WHERE class_id = ' . intval($class);
        $classtable = $this->conn->Execute($sql);

        if (!$classtable) {
            $this->log_error($sql);
        }

        $resource = $classtable->fields['resource_id'];
        //Get Saved Search Criteria
        $sql = 'SELECT metadata_id,search_value 
			FROM transparentrets_search 
			WHERE class_id = ' . intval(
            $class
        );
        $searchcrit = $this->conn->Execute($sql);

        if (!$searchcrit) {
            $this->log_error($sql);
        }

        $scriteria = [];

        while (!$searchcrit->EOF) {
            $valuearray = explode(',', $searchcrit->fields['search_value']);

            if (count($valuearray) > 1) {
                $scriteria[$searchcrit->fields['metadata_id']] = $valuearray;
            } else {
                $scriteria[$searchcrit->fields['metadata_id']] = $searchcrit->fields['search_value'];
            }
            $searchcrit->moveNExt();
        }

        //Get Account URL
        //Get Account Information
        $sql =
            'SELECT url 
			FROM transparentrets_servers 
			WHERE account_id 
			IN (SELECT account_id 
				FROM transparentrets_resources 
				WHERE resource_id = ' . $resource . ');';
        $account = $this->conn->Execute($sql);

        if (!$account) {
            $this->log_error($sql);
        }

        //For Rapatoni issue warnging.
        if (strpos($account->fields['url'], 'raprets.com') !== false) {
            $display .= '<div class="rets_info">
							<p>
								Minimum Query Requirements
								Rapattoni has implementing minimum query requirements for Search transactions. 
								This means that you must select at a minimum one of the following sets of search criteria. 
								The options for minimum seargh requirements are:
							</p>
		<ul>
			<li>APN and County</li>
			<li>MLNumber</li>
			<li>Approved and Status</li>
			<li>Area and Status</li>
			<li>Region and Status</li>
			<li>Area, Region and Status</li>
			<li>City and Status</li>
			<li>Longitude, Latitude and Status</li>
			<li>SearchPrice and Status</li>
			<li>StreetName and Status</li>
			<li>StreetNumber and Status</li>
			<li>StreetName, StreetNumber and Status</li>
			<li>SubdivisionNumber and Status</li>
			<li>ZipCode and Status</li>
		</ul>
		<p>
			<strong>IMPORTANT:</strong> There will be situations when one or more of the sets will not be available on 
			a given RETS server as the Metadata-Table makeup can differ from one MLS to another
		</p>
		</div>';
        }

        //Get Fields
        $sql =
            "SELECT metadata_id, metadata_fieldname, metadata_longname,metadata_shortname,metadata_searchhelpid,metadata_interpretation,metadata_fieldtype,metadata_lookupname,metadata_required 
			FROM transparentrets_metadata 
			WHERE class_id = " . intval($class) . " 
			AND metadata_searchfield = 1 
			ORDER BY metadata_fieldname";
        $frets = $this->conn->Execute($sql);

        if (!$frets) {
            $this->log_error($sql);
        }

        //Get Lookups
        $sql =
            'SELECT metadata_lookupname,metadata_value,metadata_longvalue 
			FROM transparentrets_lookuptype 
			WHERE resource_id = ' . $resource . ' 
			ORDER BY metadata_longvalue';;
        $lookup = $this->conn->Execute($sql);

        if (!$lookup) {
            $this->log_error($sql);
        }

        $lookups = [];

        while (!$lookup->EOF) {
            $lookups[$lookup->fields['metadata_lookupname']][$lookup->fields['metadata_value']]
                = $lookup->fields['metadata_longvalue'];
            $lookup->MoveNext();
        }
        $display .= '<form id="form_search_criteria" action="#" method="post">
					Use Ctrl + click to make multiple selections. <br /><br />
					Note: &nbsp; Every selection you make filters the number of listings that will be imported. 
					You must make at least one selection, more, if your RETS server enforces minimum criteria. 
					For most typical use, locate the "Status" or similar field and select "Active" and that will set your Import Criteria to mean: Import all Active listings for this class
					<br /><br />
					Including Status: "Sold" or "expired" may significantly increase the number of listings imported. 
					Many services do not make non-active or similar listings available due to the extremely large amount of information involved and the sizable server resources required.   
					
					<fieldset>
					
						<table width="100%" id="import_criteria">
						<tr>
							<th>RETS Field</th>
							<th>Search Criteria</th>
						</tr>';

        while (!$frets->EOF) {
            //Get Search Help
            if ($frets->fields['metadata_searchhelpid'] != 0) {
                /*
                 * CREATE TABLE  `or`.`transparentrets_searchhelp` (
                `searchhelp_id` int(11) NOT NULL auto_increment,
                `metadata_searchhelpid` varchar(32) NOT NULL,
                `metadata_searchhelpvalue` varchar(255) NOT NULL,
                `resource_id` int(11) NOT NULL,
                PRIMARY KEY  (`searchhelp_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=latin1
                */
                $sh_sql = 'SELECT metadata_searchhelpvalue 
							FROM transparentrets_searchhelp 
							WHERE metadata_searchhelpid = ' . intval($frets->fields['metadata_searchhelpid']);
                $sh_rs = $this->conn->Execute($sh_sql);
                if (!$sh_rs) {
                    $this->log_error($sh_sql);
                }
                $shelp = $sh_rs->fields['metadata_searchhelpvalue'];
            } else {
                $shelp = 'No Search Help Provided';
            }
            $display .= '<tr>
						<td>
							<label onmouseover="domTT_activate(this, event, \'caption\', \'' . $frets->fields['metadata_fieldname'] . '\', \'content\',\'LongName: '
                . addslashes(htmlentities(trim($frets->fields['metadata_longname'])))
                . '<br />ShortName: ' . addslashes(htmlentities(trim($frets->fields['metadata_shortname'])))
                . '<br />Search Help: ' . addslashes(htmlentities($shelp)) . '\', \'trail\', \'x\');" for="id_' . $frets->fields['metadata_id'] . '"';

            if ($frets->fields['metadata_required'] == 1) {
                $display .= ' style="color:red;" ';
            }

            $display .= '>' . $frets->fields['metadata_fieldname'] . '<br />' . $frets->fields['metadata_longname'] . '
							</label>
						</td>' . "\r\n";
            $display .= '<td align="center">';

            if (
                $frets->fields['metadata_interpretation'] == 'Lookup'
                || $frets->fields['metadata_interpretation'] == 'LookupMulti'
                || $frets->fields['metadata_interpretation'] == 'LookupBitstring'
                || $frets->fields['metadata_interpretation'] == 'LookupBitmask'
            ) {
                $display .= '<select name="' . $frets->fields['metadata_fieldname']
                    . '[]" multiple="multiple" size="7" style="width:250px;">';
                $lookupname = $frets->fields['metadata_lookupname'];

                if (!array_key_exists($lookupname, $lookups)) {
                    $this->log_action('RETS SERVER ERROR: Lookup Values missing for ' . $lookupname);
                } else {
                    foreach ($lookups[$lookupname] as $value => $lvalue) {
                        if (isset($scriteria[$frets->fields['metadata_id']])) {
                            if (
                                is_array($scriteria[$frets->fields['metadata_id']])
                                && in_array($value, $scriteria[$frets->fields['metadata_id']])
                            ) {
                                $display .= '<option value="' . $value . '" selected="selected">' . $lvalue
                                    . '</option>';
                            } elseif ($scriteria[$frets->fields['metadata_id']] == $value) {
                                $display .= '<option value="' . $value . '" selected="selected">' . $lvalue
                                    . '</option>';
                            } else {
                                $display .= '<option value="' . $value . '">' . $lvalue . '</option>';
                            }
                        } else {
                            $display .= '<option value="' . $value . '">' . $lvalue . '</option>';
                        }
                    }
                }
                $display .= '</select>';
            } else {
                if (isset($scriteria[$frets->fields['metadata_id']])) {
                    if (is_array($scriteria[$frets->fields['metadata_id']])) {
                        $scriteria[$frets->fields['metadata_id']] = implode(',', $scriteria[$frets->fields['metadata_id']]);
                    }
                    $display .= '<input type="text" name="' . $frets->fields['metadata_fieldname']
                        . '" style="width:250px;" value="' . $scriteria[$frets->fields['metadata_id']] . '" />';
                } else {
                    $display .= '<input type="text" name="' . $frets->fields['metadata_fieldname']
                        . '" style="width:250px;" />';
                }
            }

            $display .= '</td>
					</tr>';

            $frets->MoveNext();
        }
        $display .= '</table>';
        //Show Advanced Search text Bos
        $display .= '<div style="width: 100%;">
					<fieldset>
						<legend>Advanced Search Options</legend>
							Enter a precise DMQL RETS query. SearchType and Class are implied and should be excluded. If this option is used, any selections entered above will be ignored.  
							<textarea name="transparentrets_adv_search"  rows="5">' . $classtable->fields['adv_search'] . '</textarea>
					</fieldset>
					</div>
					<table>
						<!--Show Save Search Button -->
						<tr>
							<td colspan="2">
								<center>
									<input type="hidden" name="save_search" value="" />
									<input type="button" class="trets_std_button" onclick="save_import_criteria();return false;"value="Save Search Criteria" />
								</center>
							</td>
						</tr>
					</table>
					</fieldset>
				</form>
				<script type="text/javascript">
					function save_import_criteria(){
						ShowPleaseWait();
						$.post("ajax.php?action=addon_transparentRETS_saveimportcriteria&class_id=' . $class . '", $("#form_search_criteria").serialize(), function(data) {
							if(data.error == "1"){
								status_error(data.error_msg);
							}else{
								status_msg(data.status_msg);
							}
							HidePleaseWait();
						},"json");

					};
				</script>';
        return $display;
    }

    public function config_servers_show_class_select()
    {
        $display = '';
        $display .= '<form action="index.php" method="get" id="select_class">
					<fieldset class="select_bar">
						<span>Select Class:</span> <select name="class_id" onChange="document.getElementById(\'select_class\').submit();">';
        //Get all resources id for servers

        $sql = "SELECT account_id,resource_id FROM transparentrets_resources";
        $resources = $this->conn->Execute($sql);

        if (!$resources) {
            $this->log_error($sql);
        }
        $resources_array = [];

        while (!$resources->EOF) {
            $resources_array[$resources->fields['resource_id']] = $resources->fields['account_id'];
            $resources->MoveNext();
        }

        $sql = "SELECT class_name, class_id,visible_name,resource_id 
			FROM transparentrets_classes";
        $class = $this->conn->Execute($sql);

        if (!$class) {
            $this->log_error($sql);
        }

        $display .= '<option value="">None Selected</option>';
        while (!$class->EOF) {
            $display .= '<option value="' . $class->fields['class_id'] . '" ';

            if (isset($_GET['class_id']) && $_GET['class_id'] == $class->fields['class_id']) {
                $display .= ' selected="selected" ';
            }

            $display .= '>' . $class->fields['class_name'] . ' (' . $class->fields['visible_name'] . ') [' . $resources_array[$class->fields['resource_id']] . ']</option>';
            $class->MoveNext();
        }

        $display .= '		</select> 

						<input type="hidden" name="action" value="addon_transparentRETS_config_imports" />
						<!--
							<input type="submit" class="trets_std_button" value="Select Class" />
						-->
						<div class="page_name_div"> ' . $this->page_img . '	<span>' . $this->page_name . '</span></div>
					</fieldset>
				</form>
				';
        return $display;
    }

    public function config_class_import_screen($id, $name, $import, $resource, $import_class)
    {

        //Get Resouce Name
        $sql = 'SELECT resource_name 
			FROM transparentrets_resources 
			WHERE resource_id = ' . $resource;
        $r = $this->conn->execute($sql);

        if (!$r) {
            $this->log_error($sql);
        }

        $r_name = $r->fields['resource_name'];
        //Get List of Classes
        $class = $this->ca_get_classlist();

        $display = '';
        $display .= '<form action="index.php?class_id=' . $id . '&amp;action=addon_transparentRETS_config_imports" method="post"><fieldset>';
        $display .= '<table>
					<tr>
						<td>
							<label onmouseover="domTT_activate(this, event, \'caption\', \'Class Name\', \'content\', \'The name of the RETS Property Class. You can not modify this.\', \'trail\', \'x\');" for="RETS_CLASS_NAME">RETS Class: </label>
						</td>
						<td>
							<input type="text" id="RETS_CLASS_NAME" name="RETS_CLASS_NAME" value="' . $name . '" disabled="disabled" />
						</td>
					</tr>
					<tr>
						<td>
							<label onmouseover="domTT_activate(this, event, \'caption\', \'Resource Name\', \'content\', \'The name of the RETS Resource. You can not modify this.\', \'trail\', \'x\');" for="RETS_RESOURCE_NAME">RETS Resource: </label>
						</td>
						<td>
							<input type="text" id="RETS_RESOURCE_NAME" name="RETS_RESOURCE_NAME" value="' . $r_name . '" disabled="disabled" />
						</td>
					</tr>
					<tr>
					<td>
						<label onmouseover="domTT_activate(this, event, \'caption\', \'Import\', \'content\', \'Should we import this class?.\', \'trail\', \'x\');">Import Class: </label>
					</td>
					<td>
						<input type="radio" id="IMPORT_CLASS_TRUE" name="IMPORT_CLASS" value="1"';

        if ($import == 1) {
            $display .= ' checked="checked" ';
        }
        $display .= ' />

			<label for="IMPORT_CLASS_TRUE">Yes</label>
				<input type="radio" name="IMPORT_CLASS" id="IMPORT_CLASS_FALSE"value="0"';

        if ($import == 0) {
            $display .= ' checked="checked" ';
        }

        $display .= ' />
						<label for="IMPORT_CLASS_FALSE">No</label>
					</td>
				</tr>
				<tr>
					<td>
						<label onmouseover="domTT_activate(this, event, \'caption\', \'Open-Realty Class\', \'content\', \'This is the Open-Realty class that the RETS class should be imported into.\', \'trail\', \'x\');" for="RETS_Open-Realty_Class">Open-Realty Class: </label>
					</td>
					<td>
						<select name="RETS_Open-Realty_Class" id="RETS_Open-Realty_Class">
							<option value=""';

        if (!array_key_exists($import_class, $class)) {
            $display .= ' selected="selected" ';
        }

        $display .= '>NOT MAPPED</option>';

        foreach ($class as $cid => $cn) {
            $display .= '<option value="' . $cid . '"';

            if ($cid == $import_class) {
                $display .= ' selected="selected" ';
            }
            $display .= '>' . $cn . '</option>';
        }

        $display .= '	</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="hidden" name="update_settings_class_id" value="' . $id . '" />
						<input type="submit" class="trets_std_button" value="Save Settings" />
					</td>
				</tr>';
        $display .= '</table>';
        $display .= '</fieldset>
				</form>';

        return $display;
    }

    public function save_resource_config($server_id, $prop_resource)
    {
        //Clear Classes
        $sql = 'SELECT resource_id 
			FROM transparentrets_resources 
			WHERE resource_property = 1 
			AND account_id = ' . $server_id;
        $recodset = $this->conn->Execute($sql);

        if (!$recodset) {
            $this->log_error($sql);
        }
        /*
                $sql=
                            'DELETE FROM transparentrets_classes WHERE resource_id != '.$prop_resource;
                $recodset=$this->conn->Execute($sql);

                if (!$recodset) {
                    $this->log_error($sql);
                }
        */
        //First Clear property resource selections for this account
        $sql = 'UPDATE transparentrets_resources 
			SET resource_property=0 
			WHERE account_id = ' . $server_id;
        $recodset = $this->conn->Execute($sql);

        if (!$recodset) {
            $this->log_error($sql);
        }

        $sql = 'UPDATE transparentrets_resources 
			SET resource_property=1 
			WHERE resource_id = ' . $prop_resource;
        $recodset = $this->conn->Execute($sql);

        if (!$recodset) {
            $this->log_error($sql);
        }
        /*
                //Clear Media Settings
                $sql=
                            'DELETE FROM transparentrets_objects WHERE resource_id = '.$prop_resource;
                $recodset=$this->conn->Execute($sql);

                if (!$recodset) {
                    $this->log_error($sql);
                }
        */
        $sql = 'SELECT resource_id 
			FROM transparentrets_resources 
			WHERE resource_property = 1 
			AND account_id = ' . $server_id;
        $recodset = $this->conn->Execute($sql);

        if (!$recodset) {
            $this->log_error($sql);
        }

        //Get New Classes
        $this->detectRETSClasses($server_id, $recodset->fields['resource_id']);
        //Get New Media Settings
        $this->detectRETSObjects($server_id, $recodset->fields['resource_id']);
        //TODO: Figoure out why I can not set the header here..
        //header('Content-type: application/json');
        $display = json_encode(['error' => "0", 'status_msg' => 'Property Resource Saved.']);
        return $display;
    }
}