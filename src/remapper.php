<?php
    /**
     * Remapper Class
     *
     * This class contains one function remap() and is used to change listing data before it is imported into Open-Realty by TransparentRETS
     *
     */
    class remapper
    {
        /**
         * This function allows you to modify the listing data. It recieves each listing as part of the $listings array and you can loop through and walk the array as needed.
         *
         * @param array $listings This is an array of listings. You need to loop through the array and make changes to any listings you want to modify placing the changes back into this array.
         * @param int $classID The class id # of the class currently being imported by TransparnetRETS. You can use this to make sure you only make modification to certain classes.
         * @return array This function must return the modified $listings array.
         */
    
        public function remap($listings, $classID)
        {
            /*
                foreach ($listings as $count => $listing){
                    //$listing will be an array of all fields in the listing $listing  is array('field1'=> 'field value','field2'=>'value 2')
                    // LookupMulti fields that have multiple values being sent back will be comma seperated. You can use explode and implode to work with individual field values
                    if(substr( $listing['PropertyType'], 0, 4 ) == 'Lots') {
                        $listings[$count]['PropertyType'] = str_replace(',', ' ', $listing['PropertyType']);
                    }
                }
                //Return the Modified Listings
            */
            return $listings;
        }
    }
