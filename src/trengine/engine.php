<?php
class RETSENGINE
{
    public $server;                 //hostname of the server to connect to
    public $path;                   //path for connection (RETS login_url serach_url etc..)
    public $query='';                  //Query Arguements to pass to the RETS server
    public $port = 80;              //Server Port
    public $account;                //RETS Username
    public $password;               //RETS Password
    public $application;            //Application part of User-Agent
    public $version;                //Version part of User-Agent
    public $clientpass;             //RETS Client Passwords
    public $rets_vers = 'RETS/1.7'; //RETS Version
    public $ua_passwd;
    public $request_id=1;
    public $post_request = true;    //Use Post of Get Request
    public $keep_alive = false;
    //var $sessionid = array();    //Cooke/Session info. This is an array becuase a server can request more then one cookie be set.
    public $loggedon = false;       //RETS Server Login Status
    public $encode_request = true;

    //Debug
    public $conn_debug = false;
    public $autodetect_debug = false;
    public $responce_debug = false;
    public $digest_debug = false;
    public $debug_tofile=false;
    public $debug_file=null;
    //Detected Auth Variables
    public $auth = null; //Auth Type. NULL, Basic, Digest
    //Digest Auth Code
    public $A1;              //Digest Auth A1 Code
    public $A2;              //Digest Auth A2 Code
    public $qop = null;    //Digest Auth qop value
    public $nc = '00000001'; //Digest Auth nc Value
    public $nonce = '';      //Diget Auth nonce value
    public $opaque = '';     //Digest Auth Opaque Value
    public $realm;           // Digest Auth Realm
    public $resp;            //Digest Auth resp value
    //RETS URLS
    public $RETS_login_array = [];
    //HTTP Socket used for Keep Alive
    public $socket = null;
    //HTTP Data Variables
    public $request;
    public $request_string; //This is the full request sent by CURL
    public $response_body;
    public $response_headers;
    public $response_array;
    public $response_SXML; //Simple XML Object of RETS Response
    public $cookies=[];
    //Search Variables
    public $DMLQ2 = '';
    //Action Message
    public $action_msg='';
    //Interrealty UA Auth Hack
    public $interealty_ua_auth = false;

    //Error Handler
    public $LAST_ERROR_CODE = null;
    public $LAST_ERROR_MESSAGE = null;
    //Performance Time
    public $time_SXML_parse = 0;
    public $time_CURL_total_time = 0;
    public $curl_retry=0;
    public $retry_auth=0;
    public $retry_query='';
    public $retry_path='';
    
    public function __construct()
    {
        if ($this->debug_tofile == true) {
            $this->debug_file=fopen(dirname(__FILE__).'/logs/debug.log', 'wb');
        }
    }
    
    public function __destruct()
    {
        if ($this->debug_tofile == true) {
            fclose($this->debug_file);
        }
    }
    
    public function get_version($resource = '', $class = '')
    {
        if ($class!='') {
            $this->get_metadata('METADATA-TABLE', $resource, $class);
            /*
                Would be faster to pull the METADATA-CLASS. SOlidearth and Rap currently die on this though
                $this->get_metadata('METADATA-CLASS', $resource, $class);
                if ($this->LAST_ERROR_CODE != 0) {
                return FALSE;
                }
                */
            $metadata=$this->response_SXML;
        }
        $this->get_metadata('METADATA-RESOURCE', $resource);
        if ($this->LAST_ERROR_CODE != 0) {
            return false;
        }
        $resourced_all=$this->response_SXML;

        //Get Metadata from Class
        //$fieldlist = $metadata['RETS']['METADATA']['METADATA-TABLE']['Field'];
        //$resource_all = $resourced_all['RETS']['METADATA']['METADATA-RESOURCE']['Resource'];
        $resource_array=[];

        foreach ($resourced_all->xpath('//Resource')as $res) {
            if ($res->{'ResourceID'}== $resource) {
                $resource_array=$res;
                break;
            }
        }
        //Get Version Info
        $version=[];
        $version['ResourceVersion']=(string)$resourced_all->{'METADATA'}->{'METADATA-RESOURCE'}['Version'];
        if ($resource!='') {
            $version['ClassVersion']=(string)$resource_array->{'ClassVersion'};
            $version['ObjectVersion']=(string)$resource_array->{'ObjectVersion'};
            $version['SearchHelpVersion']=(string)$resource_array->{'SearchHelpVersion'};
            $version['LookupVersion']=(string)$resource_array->{'LookupVersion'};
        }
        if ($class!='') {
            $version['TableVersion']=(string)$metadata->{'METADATA'}->{'METADATA-TABLE'}['Version'];
        }
        return $version;
    }

    public function detect_key_field($resource)
    {
        $this->get_metadata('METADATA-RESOURCE', $resource);
        $resourced_all=$this->response_SXML;
        $resource_array=[];

        foreach ($resourced_all->xpath('//Resource')as $res) {
            if ($res->{'ResourceID'}== $resource) {
                $resource_array=$res;
                break;
            }
        }
        if (isset($resource_array->{'KeyField'}) && $resource_array->{'KeyField'}!= '') {
            $key_field=(string)$resource_array->{'KeyField'};
        } else {
            $key_field=false;
        }
        return $key_field;
    }
    /**
     * This Function steps the nc value for Digest Authentication calls
     *
     */
    public function step_nc()
    {
        $n1=$this->nc;
        $n1=floatval($n1) + 1;
        $len=strlen($n1);
        $n0=8 - $len;
        $i=1;

        while ($i <= $n0) {
            $n1 = '0'.strval($n1);
            $i++;
        }

        $this->nc=$n1;
    }
    /**
     * This function builds the connection header and body for all RETS requests.
     * Should only be called internally by RETS_CONNECT
     *
     */
    public function build_connection($image=false)
    {
        $this->request=[];

        $this->request[]='RETS-Version: '.$this->rets_vers;

        if ($this->auth == 'Digest') {
            if ($this->qop==null) {
                $this->build_digest_respo_2069();
                $this->request[]='Authorization: Digest username="'.$this->account.'", realm="'.$this->realm.'", nonce="'.$this->nonce.'", uri="'.$this->path.'", response="'.$this->resp.'", opaque="'.$this->opaque.'"';
            } else {
                $this->build_digest_respo();
                $this->request[]='Authorization: Digest username="'.$this->account.'", realm="'.$this->realm.'", nonce="'.$this->nonce.'", uri="'.$this->path.'", cnonce="'.$this->cnonce.'", nc='.$this->nc.', qop="'.$this->qop.'", response="'.$this->resp.'", opaque="'.$this->opaque.'"';
            }
        } elseif ($this->auth == 'Basic') {
            $this->request[]='Authorization: Basic '.base64_encode($this->account.':'.$this->password);
        }

        if (empty($this->version)) {
            $this->request[]='User-Agent: '.$this->application;
        } else {
            $this->request[]='User-Agent: '.$this->application.'/'.$this->version;
        }
        if (!empty($this->ua_passwd) && ($this->rets_vers=='RETS/1.7' || $this->rets_vers=='RETS/1.7.2')) {
            if (empty($this->version)) {
                $ua1=md5($this->application.':'.$this->ua_passwd);
                $ua1_debug='md5('.$this->application.':'.$this->ua_passwd.')';
            } else {
                $ua1=md5($this->application.'/'.$this->version.':'.$this->ua_passwd);
                $ua1_debug='md5('.$this->application.'/'.$this->version.':'.$this->ua_passwd.')';
            }
            //print_r($this->sessionid);
            $sessid='';
            if (isset($this->cookies['RETS-Session-ID'])) {
                $sessid=$this->cookies['RETS-Session-ID']['value'];
            }
            //@$cookiepart=explode("=",$this->sessionid['RETS-Session-ID']);
            //@$sessid=trim($cookiepart[1]);
            //print_r($sessid);
            if ($this->conn_debug) {
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, "<pre>\r\n ----TransparentRETS AU-Auth Deubg (START)---\r\n".
                        'RETS-UA-Authorization: Digest md5('.$ua1_debug.':'.$this->request_id.':'.$sessid.':'.$this->rets_vers.')'
                        ."\r\n ----TransparentRETS Query (END)---\r\n</pre>");
                } else {
                    echo "<pre>\r\n ----TransparentRETS AU-Auth Deubg (START)---\r\n".
                        'RETS-UA-Authorization: Digest md5('.$ua1_debug.':'.$this->request_id.':'.$sessid.':'.$this->rets_vers.')'
                        ."\r\n ----TransparentRETS Query (END)---\r\n</pre>";
                }
            }
            $this->request[]='RETS-UA-Authorization: Digest '.md5($ua1.':'.$this->request_id.':'.$sessid.':'.$this->rets_vers);
        }
        $this->request[]='RETS-Request-ID: '.$this->request_id;
        $this->request_id++;
        if ($image) {
            $this->request[]='Accept: image/jpeg';
        }
        //Add Rets Session
        /*if (count($this->sessionid) > 0) {
        foreach ($this->sessionid as $cookie) {
        $this->request[]='Cookie: '.$cookie;
        }
        }
        */
        //Add Cookies to Request
        $this->request[]=$this->cookie_build($this->cookies);

        if (!$this->keep_alive) {
            //$this->request[]="Connection: close";
        }

        //Handle Content Length Header
        $content_type_header=null;
    }
    /**
     * This function logs you off the RETS Server
     *
     * @return unknown
     */
    public function rets_logout()
    {
        if ($this->loggedon == true) {
            $this->path=$this->RETS_login_array['Logout'];
            $this->query='';
            $this->RETS_CONNECT();
            $this->path=$this->RETS_login_array['Login'];
            $this->loggedon=false;
            $this->nc='00000001';
            $this->auth=null;
            $this->cookies=[];
            $this->RETS_login_array = [];
            $this->request_id=1;
        //$this->sessionid=array();
        } else {
            $this->nc='00000001';
            $this->auth=null;
            $this->cookies=[];
            $this->query='';
            $this->path=$this->RETS_login_array['Login'];
            $this->RETS_login_array = [];
            $this->request_id=1;
        }
    }
    /**
     * This function builds the digest auth hashes
     *
     */
    public function build_digest_respo_2069()
    {
        if ($this->digest_debug) {
            echo "\r\n\r\n<pre>---DIGEST AUTH BUILD INFO (START)---\r\n A1: ".$this->account.':'
            .$this->realm.':'.$this->password;
        }

        $this->A1=md5($this->account.':'.$this->realm.':'.$this->password);

        // Not sure yet what this does or is used for. I believe it is the request method and location
        // (method:digest-uri-value)
        if ($this->post_request) {
            $method='POST';
        } else {
            $method='GET';
        }

        $this->A2=md5($method.':'.$this->path);

        if ($this->digest_debug) {
            echo "\r\n A2: ".$method.':'.$this->path."\r\n\r\n";
        }

        if ($this->clientpass!=null) {
            $this->cnonce=md5($this->application.'/'.$this->version.':'.$this->clientpass.':RETS-Request-ID: '.$this->request_id.':'.$this->nonce);
        } else {
            $this->cnonce=md5($this->application.'/'.$this->version.'::RETS-Request-ID: '.$this->request_id.''.$this->nonce);
        }

        if ($this->digest_debug) {
            echo "\r\n Resp: ".$this->A1.':'.$this->nonce.':'.$this->nc.':'.$this->cnonce.':'
            .$this->qop.':'.$this->A2."\r\n---DIGEST AUTH BUILD INFO (START)---</pre>\r\n";
        }
        $this->resp=md5($this->A1.':'.$this->nonce.':'.$this->A2);
        //$this->resp=md5($this->A1.':'.$this->nonce.':'.$this->nc.':'.$this->cnonce.':'.$this->qop.':'.$this->A2);
    }
    /**
     * This function builds the digest auth hashes from RFC 2617
     *
     */
    public function build_digest_respo()
    {
        if ($this->digest_debug) {
            echo "\r\n\r\n<pre>---DIGEST AUTH BUILD INFO (START)---\r\n A1: ".$this->account.':'
            .$this->realm.':'.$this->password;
        }

        $this->A1=md5($this->account.':'.$this->realm.':'.$this->password);

        // Not sure yet what this does or is used for. I believe it is the request method and location
        // (method:digest-uri-value)
        if ($this->post_request) {
            $method='POST';
        } else {
            $method='GET';
        }

        $this->A2=md5($method.':'.$this->path);

        if ($this->digest_debug) {
            echo "\r\n A2: ".$method.':'.$this->path."\r\n\r\n";
        }

        if ($this->clientpass!=null) {
            $this->cnonce=md5($this->application.'/'.$this->version.':'.$this->clientpass.':RETS-Request-ID: '.$this->request_id.':'.$this->nonce);
        } else {
            $this->cnonce=md5($this->application.'/'.$this->version.'::RETS-Request-ID: '.$this->request_id.''.$this->nonce);
        }

        if ($this->digest_debug) {
            echo "\r\n Resp: ".$this->A1.':'.$this->nonce.':'.$this->nc.':'.$this->cnonce.':'
            .$this->qop.':'.$this->A2."\r\n---DIGEST AUTH BUILD INFO (START)---</pre>\r\n";
        }
        //$this->resp=md5($this->A1.':'.$this->nonce.':'.$this->A2);
        $this->resp=md5($this->A1.':'.$this->nonce.':'.$this->nc.':'.$this->cnonce.':'.$this->qop.':'.$this->A2);
    }

    /**
     * This function make the actual RETS Connection.
     *
     * @param unknown_type $out
     * @return unknown
     */
    public function RETS_CONNECT($skipXML = false, $image=false)
    {
        set_time_limit(0);
        error_reporting(E_ALL);
        if ($this->conn_debug == false) {
            if (defined('TRETS_CONN_DEBUG')) {
                $this->conn_debug=TRETS_CONN_DEBUG;
            }
        }
        if ($this->responce_debug == false) {
            if (defined('TRETS_RESPONCE_DEBUG')) {
                $this->responce_debug =TRETS_RESPONCE_DEBUG;
            }
        }
        if (defined('TRETS_DIGEST_DEBUG')) {
            $this->digest_debug =TRETS_DIGEST_DEBUG;
        }
        if (defined('TRETS_DEBUG_TOFILE')) {
            $this->debug_tofile =TRETS_DEBUG_TOFILE;
        }
        if ($this->debug_tofile == true && $this->debug_file == null && file_exists($this->debug_file)) {
            $this->debug_file=fopen($this->debug_file, 'wb');
        } else {
            $this->debug_tofile=false;
        }
        //Check if path is a full url
        if (strpos($this->path, 'http://') !== false || strpos($this->path, 'https://') !== false) {
            $udata=parse_url($this->path);
            if (isset($udata['port'])) {
                $this->port=intval($udata['port']);
            }
            $this->server=$udata['scheme'].'://'.$udata['host'];
            $this->path=$udata['path'];

            if (isset($udata['query'])) {
                $this->query=$udata['query'];
            }
        }
        //Clear Headers & body
        $this->response_body='';
        $this->response_headers=[];
        //Build new connection headers
        $this->build_connection($image);
        //Create new connection if not reusing last.
        if (!$this->keep_alive || $this->socket == null) {
            $this->socket=curl_init();
        }

        if ($this->post_request) {
            curl_setopt($this->socket, CURLOPT_URL, $this->server.':'.$this->port.$this->path);
            curl_setopt($this->socket, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->socket, CURLOPT_POSTFIELDS, $this->query);
        //}
        } else {
            curl_setopt(
                $this->socket,
                CURLOPT_URL,
                $this->server.':'.$this->port.$this->path.'?'.$this->query
            );
            curl_setopt($this->socket, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        curl_setopt($this->socket, CURLOPT_PORT, $this->port);
        curl_setopt($this->socket, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->socket, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->socket, CURLOPT_VERBOSE, 0);
        curl_setopt($this->socket, CURLOPT_TIMEOUT, 600);
        curl_setopt($this->socket, CURLOPT_HTTPHEADER, $this->request);
        curl_setopt($this->socket, CURLOPT_HEADERFUNCTION, [$this,'curl_read_header']);
        curl_setopt($this->socket, CURLOPT_WRITEFUNCTION, [$this,'curl_read_body']);
        curl_setopt($this->socket, CURLOPT_ENCODING, 'gzip');
        //curl_setopt($ch,CURLOPT_READFUNCTION,array($this,'curl_read_request'))
        if ($this->conn_debug) {
            curl_setopt($this->socket, CURLOPT_VERBOSE, 1);
            if ($this->debug_tofile == true) {
                fwrite($this->debug_file, "<pre>\r\n\r\n ----TransparentRETS Request Headers (START)---\r\n".implode(
                    "\r\n",
                    $this->request
                )."\r\n ----TransparentRETS Request (END)---\r\n</pre>");
                fwrite($this->debug_file, "<pre>\r\n ----TransparentRETS Request Query (START)---\r\n".$this->query
                ."\r\n ----TransparentRETS Query (END)---\r\n</pre>");
            } else {
                //echo "<pre>\r\n\r\n Connecting to : ".$this->server.':'.$this->port.$this->path."\r\n</pre>";
                echo "<pre>\r\n\r\n ----TransparentRETS Request Headers (START)---\r\n".implode(
                    "\r\n",
                    $this->request
                )."\r\n ----TransparentRETS Request (END)---\r\n</pre>";
                echo "<pre>\r\n ----TransparentRETS Request Query (START)---\r\n".$this->query
                ."\r\n ----TransparentRETS Query (END)---\r\n</pre>";
            }
        }

        curl_exec($this->socket);

        if ($this->responce_debug) {
            if ($this->debug_tofile == true) {
                fwrite($this->debug_file, "<pre>\r\n\r\n ----RETS SERVER RESPONSE (START)---\r\n");

                foreach ($this->response_headers as $i => $h) {
                    if (is_array($h)) {
                        fwrite($this->debug_file, $i.' ');
                        foreach ($h as $si => $sh) {
                            fwrite($this->debug_file, $si.' '.$sh."\r\n");
                        }
                    } else {
                        fwrite($this->debug_file, $i.' '.$h."\r\n");
                    }
                }
                fwrite($this->debug_file, "\r\n".$this->response_body."\r\n ----RETS SERVER RESPONSE (END)---\r\n</pre>");
            } else {
                echo "<pre>\r\n\r\n ----RETS SERVER RESPONSE (START)---\r\n";

                foreach ($this->response_headers as $i => $h) {
                    if (is_array($h)) {
                        echo $i.' ';
                        foreach ($h as $si => $sh) {
                            echo $si.' '.$sh."\r\n";
                        }
                    } else {
                        echo $i.' '.$h."\r\n";
                    }
                }
                echo "\r\n".$this->response_body."\r\n ----RETS SERVER RESPONSE (END)---\r\n</pre>";
            }
        }
        //Flush output buffer. Needed for debug mode so you do not run out of memory under PHP 5.2
        flush();
        if (curl_errno($this->socket)) {
            //Check for Timeout
            if (curl_errno($this->socket) == 7 && $this->curl_retry < 3) {
                $this->curl_retry++;
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, 'Curl Unable to connect to host #'.$this->curl_retry.' retrying....');
                }
                echo "Curl Unable to connect to host #'.$this->curl_retry.' retrying....\r\n";
                return $this->RETS_CONNECT($skipXML, $image);
            }
            //Check for Timeout
            if (curl_errno($this->socket) == 28 && $this->curl_retry < 3) {
                $this->curl_retry++;
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, 'Curl Timeout #'.$this->curl_retry.' retrying....');
                }
                echo "Curl Timeout #'.$this->curl_retry.' retrying....\r\n";
                return $this->RETS_CONNECT($skipXML, $image);
            }
            //Check For Empty Replies
            if (curl_errno($this->socket) == 52 && $this->curl_retry < 3) {
                $this->curl_retry++;
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, 'Curl Recieved Empty Reply #'.$this->curl_retry.' retrying....');
                }
                echo "Curl Recieved Empty Reply #'.$this->curl_retry.' retrying....\r\n";
                return $this->RETS_CONNECT($skipXML, $image);
            }
            //Check For Network Errors
            if (curl_errno($this->socket) == 56 && $this->curl_retry < 3) {
                $this->curl_retry++;
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, 'Failure when receiving data from the peer #'.$this->curl_retry.' ('.curl_errno($this->socket).') retrying....');
                }
                echo 'Failure when receiving data from the peer #'.$this->curl_retry.' ('.curl_errno($this->socket).') retrying....'."\r\n";
                return $this->RETS_CONNECT($skipXML, $image);
            }
            //Check for transfer closed with outstanding read data remaining
            if (curl_errno($this->socket) == 18 && $this->curl_retry < 3) {
                $this->curl_retry++;
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, 'Transfer closed with outstanding read data remaining #'.$this->curl_retry.' retrying....');
                }
                echo "Transfer closed with outstanding read data remaining #'.$this->curl_retry.' retrying....\r\n";
                return $this->RETS_CONNECT($skipXML, $image);
            }
            if ($this->debug_tofile == true) {
                fwrite($this->debug_file, 'Fatal Curl Error'.curl_error($this->socket));
            }
            print 'Fatal Curl Error'.curl_error($this->socket);
            die();
        } else {
            $this->curl_retry=0;
            $this->time_CURL_total_time+=curl_getinfo($this->socket, CURLINFO_TOTAL_TIME);
            if (!$this->keep_alive || $this->socket == null) {
                curl_close($this->socket);
                $this->socket == null;
            }
        }
        //Check for Session ID
        $RETS_SID_SET = false;
        $new_cookies=[];
        if (isset($this->response_headers['set-cookie'])) {
            $new_cookies=$this->cookie_parse($this->response_headers['set-cookie']);
            $this->cookies = array_merge($this->cookies, $new_cookies);
        }
        //print_r($this->cookies);
        if (isset($this->cookies['RETS-Session-ID'])) {
            $RETS_SID_SET = true;
        }
        //Copy Cookie ID for Broken RETS PRO 2 Servers
        if ($RETS_SID_SET == false && $this->loggedon && strpos($this->server, 'interealty')!== false) {
            if (isset($this->cookies['ASP.NET_SessionId'])) {
                $this->cookies['RETS-Session-ID'] = $this->cookies['ASP.NET_SessionId'];
            }
        }


        switch (substr($this->response_headers['HTTP/1.1'], 0, 3)) {
            case '401':

                //Check to see if server uses Digest Auth
                $authtype=$this->response_headers['www-authenticate'];
                $authtype=explode(' ', $authtype);
                if (strtolower($authtype[0]) == 'digest' && $this->auth != 'Digest') {
                    $this->nonce=$this->parse_digest('nonce=');
                    $this->opaque=$this->parse_digest('opaque=');
                    $this->realm=$this->parse_digest('realm=');
                    $this->qop=$this->parse_digest('qop=');
                    $this->auth='Digest';
                    $response=$this->RETS_CONNECT($skipXML, $image);
                    return $response;
                //$this->auth='Null';
                } elseif (strtolower($authtype[0]) == 'digest' && $this->auth == 'Digest') {
                    //Nonce is valid
                    //Digest Auth Failed. Check for stale
                    $stale = $this->parse_digest('stale=');
                    if (strtolower($stale)=='true') {
                        $this->nonce=$this->parse_digest('nonce=');
                        $this->opaque=$this->parse_digest('opaque=');
                        $this->realm=$this->parse_digest('realm=');
                        $this->qop=$this->parse_digest('qop=');
                        $this->auth='Digest';
                        $response=$this->RETS_CONNECT($skipXML, $image);
                        return $response;
                    } elseif ($this->loggedon && $this->retry_auth == 0) {
                        if ($this->debug_tofile == true) {
                            fwrite($this->debug_file, 'Server appears to have lost our session (401:BAD) and returned a Digst Header without setting stale=true retrying....');
                        }
                        echo "Server appears to have lost our session  (401:BAD) and returned a Digst Header without setting stale=true retrying....\r\n";
                        $this->nonce=$this->parse_digest('nonce=');
                        $this->opaque=$this->parse_digest('opaque=');
                        $this->realm=$this->parse_digest('realm=');
                        $this->qop=$this->parse_digest('qop=');
                        $this->auth='Digest';
                        $response=$this->RETS_CONNECT($skipXML, $image);
                        return $response;
                    } else {
                        if ($this->debug_tofile == true) {
                            fwrite($this->debug_file, 'Connection Failed:. '.$this->response_headers['rawresponsecode']);
                        }
                        die('Connection Failed: '.$this->response_headers['rawresponsecode']);
                    }
                } elseif (strtolower($authtype[0]) == 'basic' && $this->auth != 'Basic') {
                    $this->auth='Basic';
                    $response=$this->RETS_CONNECT($skipXML, $image);
                    return $response;
                } elseif (strtolower($authtype[0]) == 'basic' && $this->auth == 'Basic') {
                    //Digest Auth Failed. Check for stale
                    if ($this->debug_tofile == true) {
                        fwrite($this->debug_file, 'Connection Failed:. '.$this->response_headers['rawresponsecode']);
                    }
                    die('Connection Failed: '.$this->response_headers['rawresponsecode']);
                } else {
                    //Login Failure without auth challenge.
                    //Make sure we have already logged in before.
                    if ($this->loggedon && $this->retry_auth == 0) {
                        if ($this->debug_tofile == true) {
                            fwrite($this->debug_file, 'Server appears to have lost our session (401:BAD) retrying....');
                        }
                        echo "Server appears to have lost our session  (401:BAD) retrying....\r\n";
                        //We logged in before, assume server sucks and lost the session.
                        $this->loggedon=false;
                        $this->nonce='';
                        $this->opaque='';
                        $this->realm='';
                        $this->qop='';
                        $this->auth='';
                        $this->retry_path=$this->path;
                        $this->retry_query=$this->query;
                        $this->path=$this->RETS_login_array['Login'];
                        $this->query=null;
                        $retry_response = $this->RETS_CONNECT();
                        //See if we logged in.
                        $this->retry_auth++;
                        if ($retry_response) {
                            //Clear our retry auth as we logged in.
                            $this->retry_auth=0;
                            $this->path = $this->retry_path;
                            $this->query = $this->retry_query;
                            //Rerun last query.
                            $response=$this->RETS_CONNECT($skipXML, $image);
                            return $response;
                        } else {
                            if ($this->debug_tofile == true) {
                                fwrite($this->debug_file, 'Connection Failed:. '.$this->response_headers['rawresponsecode']);
                            }
                            die('Connection Failed: '.$this->response_headers['rawresponsecode']);
                        }
                    } else {
                        if ($this->debug_tofile == true) {
                            fwrite($this->debug_file, 'Connection Failed:. '.$this->response_headers['rawresponsecode']);
                        }
                        die('Connection Failed: '.$this->response_headers['rawresponsecode']);
                    }
                }

                break;
            case '400':
                if ($this->loggedon) {
                    if ($this->check_rets_response_code()) {
                        if ($this->auth == 'Digest') {
                            $this->step_nc();
                        }
                        return true;
                    } else {
                        $this->auth=null;
                        return false;
                    }
                } else {
                    $this->auth=null;
                    return false;
                }
                break;
            case '200':
                if ($this->debug_tofile == true) {
                    fwrite($this->debug_file, 'Curl Time: '.$this->time_CURL_total_time);
                }
                if (!$skipXML) {
                    $this->GetXMLTree();
                    if ($this->debug_tofile == true) {
                        fwrite($this->debug_file, 'XML Parse Time: '.$this->time_SXML_parse);
                    }
                }
                if ($this->auth == 'Digest') {
                    $this->step_nc();
                }
                if ($this->check_rets_response_code() && (int)$this->response_SXML['ReplyCode'] !=0) {
                    //MRIS Session Expiratio Work Arround
                    if (strpos($this->LAST_ERROR_MESSAGE, 'The session has failed because the user session has expired')!==false && $this->loggedon && $this->retry_auth == 0) {
                        if ($this->debug_tofile == true) {
                            fwrite($this->debug_file, 'Server appears to have lost our session (200:BAD) retrying....');
                        }
                        echo "Server appears to have lost our session  (200:BAD) retrying....\r\n";
                        //We logged in before, assume server sucks and lost the session.
                        $this->retry_auth++;
                        $this->loggedon=false;
                        $this->nonce='';
                        $this->opaque='';
                        $this->realm='';
                        $this->qop='';
                        $this->auth='';
                        $this->retry_path=$this->path;
                        $this->retry_query=$this->query;
                        $this->path=$this->RETS_login_array['Login'];
                        $this->query=null;
                        $retry_response = $this->RETS_CONNECT();
                        if ($retry_response) {
                            //Clear our retry auth as we logged in.
                            $this->retry_auth=0;
                            $this->path = $this->retry_path;
                            $this->query = $this->retry_query;
                            //Rerun last query.
                            $response=$this->RETS_CONNECT($skipXML, $image);
                            return $response;
                        } else {
                            if ($this->debug_tofile == true) {
                                fwrite($this->debug_file, 'Connection Failed:. '.$this->response_headers['rawresponsecode']);
                            }
                            die('Connection Failed: '.$this->response_headers['rawresponsecode']);
                        }
                    }
                }
                if (!$this->loggedon) {
                    //Check for RETS Errors on Login
                    if (!$this->check_rets_response_code()) {
                        return false;
                    }
                    if ((int)$this->response_SXML['ReplyCode'] !=0) {
                        //Check for Interrealty Client UA Server
                        if (strpos($this->LAST_ERROR_MESSAGE, 'Client password invalid or unknown application')!==false && $this->interealty_ua_auth == false) {
                            $this->interealty_ua_auth = true;
                            return $this->RETS_CONNECT();
                        }
                        return false;
                        //die ('RETS ERROR '.(int)$this->response_SXML['ReplyCode'].' - '.(string)$this->response_SXML['ReplyText']);
                    }
                    $this->loggedon=true;
                    $this->retry_auth=0;
                    //Get URL This was a login
                    $this->get_rets_urls();
                    //Check For Action-URL and get it as per the RETS Spec.

                    if (isset($this->RETS_login_array['Action'])) {
                        //PER the spec this is a GET request so save our post request status and use get then reset
                        $method=$this->post_request;
                        $this->post_request=false;
                        $this->path=$this->RETS_login_array['Action'];
                        $this->query=null;
                        $this->RETS_CONNECT(true);
                        $this->action_msg=$this->response_body;
                        $this->post_request=$method;
                    }
                }
                break;
            case '500':
                //RETS Compliancy test return 500 Server Error... How dumb..
                if ($this->loggedon) {
                    if ($this->auth == 'Digest') {
                        $this->step_nc();
                    }
                }
                return false;
                break;
        }

        if (!$this->check_rets_response_code()) {
            return false;
        }

        return true;
    }

    public function GetXMLTree()
    {
        $start=microtime(true);
        //Remove non UTF-8 chars from xml paylod
        $this->response_body = utf8_encode($this->response_body);
        //Parse new xml
        $this->response_SXML=simplexml_load_string($this->response_body);
        $end=microtime(true);
        $this->time_SXML_parse+=$end - $start;
    }

    public function curl_read_header($ch, $string)
    {
        $length=strlen($string);
        if (strpos($string, 'HTTP/1.1') === 0) {
            $this->response_headers['rawresponsecode']=$string;
            $this->response_headers['HTTP/1.1']=strtoupper(trim(substr($string, 8)));
        } else {
            $header=explode(':', $string, 2);
            if (count($header) > 1) {
                if (strtolower($header[0]) == 'set-cookie') {
                    $cookiepart=explode('=', $header[1]);
                    $this->response_headers[strtolower($header[0])][$cookiepart[0]]=trim($header[1]);
                } else {
                    $this->response_headers[strtolower($header[0])]=trim($header[1]);
                }
            }
        }

        return $length;
    }

    public function curl_read_body($ch, $string)
    {
        $length=strlen($string);
        $this->response_body.=$string;
        return $length;
    }

    public function check_rets_response_code()
    {
        if (!isset($this->response_SXML['ReplyCode'])) {
            $this->LAST_ERROR_CODE=null;
            $this->LAST_ERROR_MESSAGE=null;
            return false;
        } else {
            $this->LAST_ERROR_CODE=(int)$this->response_SXML['ReplyCode'];
            $this->LAST_ERROR_MESSAGE=(string)$this->response_SXML['ReplyText'];
            return true;
        }
    }

    // Used to strip nonce, auth, opaque, and session id
    public function parse_digest($needle)
    {
        $haystack=$this->response_headers['www-authenticate'];
        $needle_length = strlen($needle);
        $needle_position=strpos($haystack, $needle);
        if ($needle_position===false) {
            return '';
        }
        $first_char = substr($haystack, $needle_position + $needle_length, 1);
        if ($first_char=='"') {
            $start_point = $needle_position + $needle_length+1;
            $end_point = strpos($haystack, '"', $start_point);
            $value_length = $end_point-$start_point;
        } else {
            $start_point = $needle_position + $needle_length;
            $end_point = strpos($haystack, ',', $start_point);
            if ($end_point===false) {
                $end_point=strlen($haystack);
            }
            $value_length = $end_point-$start_point;
        }
        return substr($haystack, $start_point, $value_length);
    }

    public function get_classes($resource, $standardnames = false)
    {
        $status=$this->get_metadata('METADATA-CLASS', $resource);

        if ($status) {
            $sxml=$this->response_SXML;
            //print_r($sxml);
            if ($standardnames) {
                foreach ($sxml->xpath('//Class')as $resource) {
                    if (isset($resource->{'StandardName'}) && $resource->{'StandardName'}!= '') {
                        if ($resource->{'VisibleName'} != '') {
                            $resourcea[(string)$resource->{'StandardName'}]=(string)$resource->{'VisibleName'};
                        } else {
                            $resourcea[(string)$resource->{'StandardName'}]=(string)$resource->{'StandardName'};
                        }
                    }
                }
            } else {
                foreach ($sxml->xpath('//Class')as $resource) {
                    if (isset($resource->{'ClassName'}) && $resource->{'ClassName'}!= '') {
                        if ($resource->{'VisibleName'} != '') {
                            $resourcea[(string)$resource->{'ClassName'}]=(string)$resource->{'VisibleName'};
                        } else {
                            $resourcea[(string)$resource->{'ClassName'}]=(string)$resource->{'ClassName'};
                        }
                    }
                }
            }
            return $resourcea;
        } else {
            return false;
        }
    }

    public function build_DMQL2_query($resource, $class, $search_fields, $query, $offsetCount=0, $format='COMPACT-DECODED', $standardnames=0, $count=0, $limit='NONE')
    {
        $SearchType='SearchType='.$resource;
        $Class='&Class='.$class;
        //if(strpos($this->server,'flexmls.com')!==FALSE){
        if ($this->encode_request) {
            $Query='&Query='.urlencode($query).'';
        } else {
            $Query='&Query='.$query.'';
        }
        /*}else{
         if($this->encode_request){
         $Query='&Query=('.urlencode($query).')';
         }else{
         $Query='&Query=('.$query.')';
         }
            }*/


        $QueryType='&QueryType=DMQL2';
        //If this argument is set to one (?1?), then a record-count is returned in the response in addition to the data. Note that on some servers this will cause the search to take longer since the count must be returned before any records are received. If this entry is set to two (?2?) then only a record-count is returned; no data is returned. If this entry is not present or set to zero (?0?) there is no record count returned
        $Count='&Count='.$count;
        $Format='&Format='.$format;
        $Limit='&Limit='.$limit;
        //Offset support is shitty at best due to it being a MAY for clients and server vendors incorrectly thinking that means they can ignore it. We got this changed for RETS 2.0 and will revisit the use of offset at that time.
        $offsetCount=intval($offsetCount);
        if ($offsetCount ==0) {
            $Offset='';
        } else {
            $Offset = '&Offset='.$offsetCount;
        }
        //This parameter is used to set the fields that are returned by the query. If this entry is not present then all allowable fields for the search/class are returned.
        $Select='';

        if (is_array($search_fields) && count($search_fields) > 1) {
            $Select='&Select='.implode(',', $search_fields);
        } elseif (is_array($search_fields) && count($search_fields) == 1) {
            $Select='&Select='.$search_fields[0];
        }
        //Drop Select if Stanard-XML
        if ($format=='STANDARD-XML') {
            $Select='';
        }
        //In some instances, the server may withhold the values of selected fields on selected records. In this case, the server SHOULD send back a null value, unless the client has specified a RestrictedIndicator.
        //$Restrictedindicator = 'NOT AVALIABLE';
        $Restrictedindicator='';
        //Are we sending teh query using standard names.
        $StandardNames='&StandardNames='.$standardnames;

        $this->query = $SearchType.$Class.$Query.$QueryType.$Count.$Format.$Limit.$Offset.$Select.$Restrictedindicator.$StandardNames;
    }

    public function get_objects($resource, $standardnames = false)
    {
        $status=$this->get_metadata('METADATA-OBJECT', $resource);

        if ($status) {
            $resourcea=[];
            $sxml=$this->response_SXML;

            if ($standardnames) {
                foreach ($sxml->xpath('//Object')as $resource) {
                    if (isset($resource->{'StandardName'}) && $resource->{'StandardName'}!= '') {
                        $resourcea[]=(string)$resource->{'StandardName'};
                    }
                }
            } else {
                foreach ($sxml->xpath('//Object')as $resource) {
                    if (isset($resource->{'ObjectType'}) && $resource->{'ObjectType'}!= '') {
                        $resourcea[]=(string)$resource->{'ObjectType'};
                    }
                }
            }
            return $resourcea;
        } else {
            return false;
        }
        //parse the array and return resource names...
    }

    public function get_searchhelp($resource, $standardnames = false)
    {
        $status=$this->get_metadata('METADATA-SEARCH_HELP', $resource);

        if ($status) {
            $searchhelparray=[];
            $sxml=$this->response_SXML;

            foreach ($sxml->xpath('//SearchHelp')as $sh) {
                if (isset($sh->{'SearchHelpID'}) && $sh->{'SearchHelpID'}!= '') {
                    $searchhelparray[(string)$sh->{'SearchHelpID'}]=(string)$sh->{'Value'};
                }
            }
            return $searchhelparray;
        } else {
            return [];
        }
        //parse the array and return resource names...
    }
    
    public function get_lookuptype($resource, $standardnames = false)
    {
        $status=$this->get_metadata('METADATA-LOOKUP_TYPE', $resource, '*');

        if ($status) {
            $lookuptype=[];
            $sxml=$this->response_SXML;
            //echo '<pre>'.print_r($sxml,TRUE).'</pre>';

            foreach ($sxml->xpath('//METADATA-LOOKUP_TYPE')as $lt) {
                $ltname = (string)$lt['Lookup'];
                if ($this->rets_vers == 'RETS/1.5' || $this->rets_vers == 'RETS/1.7') {
                    foreach ($lt->{'Lookup'}as $v) {
                        if (!empty($v->{'Value'}) || $v->{'Value'} == 0) {
                            $lookuptype[]=['name'=>(string)$ltname,'value'=>(string)$v->{'Value'},'lvalue'=>(string)$v->{'LongValue'},'svalue'=>(string)$v->{'LongValue'}];
                        }
                    }
                } else {
                    foreach ($lt->{'LookupType'}as $v) {
                        if (!empty($v->{'Value'}) || $v->{'Value'} == 0 || (empty($v->{'Value'}) && !empty($v->{'LongValue'}))) {
                            $lookuptype[]=['name'=>(string)$ltname,'value'=>(string)$v->{'Value'},'lvalue'=>(string)$v->{'LongValue'},'svalue'=>(string)$v->{'LongValue'}];
                        }
                    }
                }
            }
            return $lookuptype;
        } else {
            return false;
        }
        //parse the array and return resource names...
    }
    
    public function get_metadatatable($resource, $class, $standardnames = false)
    {
        $status=$this->get_metadata('METADATA-TABLE', $resource, $class);

        if ($status) {
            if ($this->LAST_ERROR_CODE != 0) {
                return false;
            }
            $mdtable=[];
            $metadata=$this->response_SXML;

            foreach ($metadata->xpath('//Field')as $v) {
                if ($v->{'Default'}!= '-1') {
                    $default=true;
                } else {
                    $default=false;
                }
                if ($v->{'Searchable'}== 1 || strtolower($v->{'Searchable'}) == 'y'	|| strtolower($v->{'Searchable'}) == 'yes' || strtolower($v->{'Searchable'}) == 'true' || strtolower($v->{'Searchable'}) == 't') {
                    $searchable=true;
                } else {
                    $searchable=false;
                }
                if ($v->{'Unique'}== 1 || strtolower($v->{'Unique'}) == 'y'	|| strtolower($v->{'Unique'}) == 'yes' || strtolower($v->{'Unique'}) == 't' || strtolower($v->{'Unique'}) == 'true') {
                    $unique=true;
                } else {
                    $unique=false;
                }
                $mdtable[]=[
                        'SystemName'=>(string)$v->{'SystemName'},
                        'Default'=>$default,
                        'Searchable'=>$searchable,
                        'Unique'=>$unique,
                        'DataType'=>(string)$v->{'DataType'},
                        'Interpretation'=>(string)$v->{'Interpretation'},
                        'LookupName'=>(string)$v->{'LookupName'},
                        'Required'=>(string)$v->{'Required'},
                        'LongName'=>(string)$v->{'LongName'},
                        'ShortName'=>(string)$v->{'ShortName'},
                        'SearchHelpID'=>(string)$v->{'SearchHelpID'},
                ];
            }
            return $mdtable;
        } else {
            return false;
        }
        //parse the array and return resource names...
    }

    public function getObjects($resource, $type, $resourceSet, $location = 0, $force_https = 0)
    {
        $this->path=$this->RETS_login_array['GetObject'];

        if ($this->encode_request) {
            $this->query='Resource='.$resource.'&Type='.$type.'&ID='.implode('%2C', $resourceSet).'&Location='.$location;
        } else {
            $this->query='Resource='.$resource.'&Type='.$type.'&ID='.implode(',', $resourceSet).'&Location='.$location;
        }

        $this->RETS_CONNECT(true, true);
        //Split MP up and return an array of iamges with key being LISTING_#
        $boundary=$this->response_headers['content-type'];
        $images=[];
        $imagesdesc=[];
        //Check for boundry type..
        if (strpos($boundary, 'boundary="') !== false) {
            $boundary_start=strpos($boundary, 'boundary="') + 10;
            $boundary_end=strpos($boundary, '"', $boundary_start);
            $boundary=substr($boundary, $boundary_start, $boundary_end - $boundary_start);
        } elseif (strpos($boundary, 'boundary=') !== false) {
            $boundary_start=strpos($boundary, 'boundary=') + 9;
            //See the boundary contains a ; if not this is the last eliment in the Content-Type header so use the reost of the line
            if (strpos($boundary, ';', $boundary_start)===false) {
                $boundary=substr($boundary, $boundary_start);
            } else {
                $boundary_end=strpos($boundary, ';', $boundary_start);
                $boundary=substr($boundary, $boundary_start, $boundary_end - $boundary_start);
            }
        } elseif (trim($boundary) == 'image/jpeg' || trim($boundary) == 'image/jpg') {
            //We got back a single image. (Interealty servers are broken and return a non multipart response) also needed for future engine usage.
            $cid = $this->response_headers['content-id'];
            $oid = $this->response_headers['object-id'];
            if ($cid != '' && $oid != '') {
                $name=$cid.'_'.$oid;
            }
            //echo 'GOT SINGLE IMAGE (DEBUG)';
            $images[$name] = $this->response_body;
            //echo '<pre>'.print_r($images,TRUE).'</pre>';
            $imagesdesc[$name]='';
            //die('Got Images 2');
            return [0=>$images,1=>$imagesdesc];
        } else {
            //die('Got Images 1');
            return [$images,$imagesdesc];
        }
        $body=explode('--'.$boundary, $this->response_body);


        foreach ($body as $bc => $response) {
            $response = trim($response);
            $bcontent=false;
            $imgres=false;
            $cid='';
            $oid='';
            $clen='';
            $cdec='';
            $name='';
            $cloc='';
            $imagelen=0;
            $lines=explode("\r\n", $response);
            foreach ($lines as $line) {
                if ($bcontent == false) {
                    if (stripos($line, 'content-id:') !== false) {
                        $cid=explode(':', $line);
                        $cid=trim($cid[1]);
                    }

                    if (stripos($line, 'object-id:') !== false) {
                        $oid=explode(':', $line);
                        $oid=trim($oid[1]);
                    }

                    if (stripos($line, 'content-length:') !== false) {
                        $clen=explode(':', $line);
                        $clen=trim($clen[1]);
                    }
                    if (stripos($line, 'description:') !== false) {
                        $cdec=explode(':', $line);
                        $cdec=trim($cdec[1]);
                    }

                    if (stripos($line, 'content-type:') !== false) {
                        $ctype=explode(':', $line);
                        $ctype=trim($ctype[1]);
                        if ($ctype == 'image/jpeg' || $ctype == 'image/jpg') {
                            $imgres=true;
                        }
                    }
                    if (stripos($line, 'location:') !== false && $location == 1) {
                        $cloc = str_ireplace('location:', '', $line);
                        $cloc=trim($cloc);
                    }

                    if ($line == '' && $imgres == false && $location!=1) {
                        //This is not an image so skip
                        continue;
                    }
                    if ($line == '' && $imgres == true) {
                        $bcontent=true;
                    }
                } else {
                    $name=$cid.'_'.$oid;
                    if (isset($images[$name])) {
                        $images[$name].="\r\n".$line;
                    } else {
                        $images[$name]=$line;
                    }

                    $imagelen+=strlen($line);
                    if ($imagelen == $clen) {
                        //This is all of the image.
                        continue;
                    }
                }
            }
            if ($location==1) {
                //Get Location Only
                if ($cloc != '') {
                    $name=$cid.'_'.$oid;
                    if ($force_https == 1) {
                        $images[$name] = str_replace('http://', 'https://', $cloc);
                    } else {
                        $images[$name] = $cloc;
                    }
                }
            }
            //If this was an image save desc to array.
            if ($name!='') {
                $imagesdesc[$name]=$cdec;
            } else {
                continue;
            }
            //Clear Body Part to save RAM
            $body[$bc]='';
        }
        //die('Got Images');
        return [0=>$images,1=>$imagesdesc];
    }

    public function get_resources($standardnames = false)
    {
        $status=$this->get_metadata('METADATA-RESOURCE');

        if ($status) {
            $resourcea=[];
            $sxml=$this->response_SXML;

            if ($standardnames) {
                foreach ($sxml->xpath('//Resource')as $resource) {
                    if (isset($resource->{'StandardName'}) && $resource->{'StandardName'}!= '') {
                        $resourcea[]=(string)$resource->{'StandardName'};
                    }
                }
            } else {
                foreach ($sxml->xpath('//Resource')as $resource) {
                    if (isset($resource->{'ResourceID'}) && $resource->{'ResourceID'}!= '') {
                        $resourcea[]=(string)$resource->{'ResourceID'};
                    }
                }
            }
            return $resourcea;
        } else {
            return false;
        }
        //parse the array and return resource names...
    }

    public function get_rets_urls()
    {
        if ($this->rets_vers == 'RETS/1.0') {
            if ($this->response_SXML->{'RETS-RESPONSE'}) {
                $login_data=(string)$this->response_SXML->{'RETS-RESPONSE'};
            } else {
                $login_data=$this->response_SXML;
            }

            $login_data=preg_split("/\r{0,1}\n/", $login_data);
            foreach ($login_data as $line) {
                $array = explode('=', $line, 2);
                if (count($array) > 1) {
                    $this->RETS_login_array[trim($array[0])]=trim($array[1]);
                }
            }
        } else {
            $login_data=$this->response_SXML->{'RETS-RESPONSE'};
            $login_data=preg_split("/\r{0,1}\n/", $login_data);
            foreach ($login_data as $line) {
                $array = explode('=', $line, 2);
                if (count($array) > 1) {
                    $this->RETS_login_array[trim($array[0])]=trim($array[1]);
                }
            }
        }
    }

    public function get_metadata($metadata_type, $metadata_resource = '', $metadata_class = '', $format = 'STANDARD-XML', $fulltree=false)
    {
        if ($fulltree===true) {
            $id='*';
        } else {
            $id='0';
        }
        if ($metadata_type == 'METADATA-RESOURCE') {
            $this->query='Type='.$metadata_type.'&ID='.$id.'&Format='.$format;
        }

        if ($metadata_type == 'METADATA-CLASS') {
            //if($metadata_class == ''){
            $this->query='Type='.$metadata_type.'&ID='.$metadata_resource.'&Format='.$format;
            //}else{
            //		$this->query='Type='.$metadata_type.'&ID='
            //	.urlencode($metadata_resource.':'.$metadata_class).'&Format='.$format;
            //}
        }

        if ($metadata_type == 'METADATA-TABLE') {
            $this->query='Type='.$metadata_type.'&ID='
            .urlencode($metadata_resource.':'.$metadata_class).'&Format='.$format;
        }

        if ($metadata_type == 'METADATA-OBJECT') {
            $this->query='Type='.$metadata_type.'&ID='.$metadata_resource.'&Format='.$format;
        }

        if ($metadata_type == 'METADATA-LOOKUP_TYPE') {
            $this->query='Type='.$metadata_type.'&ID='
            .urlencode($metadata_resource.":".$metadata_class).'&Format='.$format;
        }
        if ($metadata_type == 'METADATA-LOOKUP') {
            $this->query='Type='.$metadata_type.'&ID='
            .urlencode($metadata_resource.":".$metadata_class).'&Format='.$format;
        }

        if ($metadata_type == 'METADATA-SEARCH_HELP') {
            $this->query='Type='.$metadata_type.'&ID='.$metadata_resource.'&Format='.$format;
        }

        $this->path=$this->RETS_login_array['GetMetadata'];
        $status= $this->RETS_CONNECT();
        if ($status && $this->LAST_ERROR_CODE==0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function loginDirect($account, $password, $url, $application, $version, $client_password = null, $port = 80, $keep_alive = false, $post_request = true, $encode_request = true, $rets_ver='RETS/1.5', $ua_pass='')
    {
        //Prevent Double logins being called. The engine will reconnect as needed.
        if ($this->loggedon == true) {
            return true;
        }
        $this->rets_vers=$rets_ver;
        $this->ua_passwd=$ua_pass;
        $this->account=$account;
        $this->password=$password;
        $udata=parse_url($url);
        if (isset($udata['port'])) {
            $this->port=intval($udata['port']);
        } else {
            $this->port=intval($port);
        }

        $this->server=$udata['scheme'].'://'.$udata['host'];
        $this->path=$udata['path'];

        if (isset($udata['query'])) {
            $this->query=$udata['query'];
        }

        $this->application=$application;
        $this->version=$version;
        $this->clientpass=$client_password;
        $this->keep_alive=$keep_alive;
        $this->post_request=$post_request;
        $this->encode_request=$encode_request;
        return $this->RETS_CONNECT();
    }
    
    public function cookie_parse($cookie_array)
    {
        //echo 'Cookie Array: <pre>'.print_r($cookie_array,TRUE).'</pre>';
        $cookies = [];
        foreach ($cookie_array as $line) {
            $csplit = explode(';', $line);
            $cdata = [];
            foreach ($csplit as $data) {
                $cinfo = explode('=', $data);
                // echo 'Cinfo Array: <pre>'.print_r($cinfo,TRUE).'</pre>';
                $cinfo[0] = trim($cinfo[0]);

                if (strtolower($cinfo[0]) == 'expires') {
                    $cinfo[1] = strtotime($cinfo[1]);
                }
                if (strtolower($cinfo[0]) == 'secure') {
                    $cinfo[1] = "true";
                }
                if (in_array(strtolower($cinfo[0]), [ 'domain', 'expires', 'path', 'secure', 'comment' ])) {
                    $cdata[trim(strtolower($cinfo[0]))] = $cinfo[1];
                } else {
                    if (isset($cinfo[1])) {
                        $cdata['key'] = $cinfo[0];
                        $cdata['value'] = $cinfo[1];
                    }
                }
            }
            if (isset($cdata['key'])) {
                $cookies[$cdata['key']] = $cdata;
            }
        }
        return $cookies;
    }

    public function cookie_build($data)
    {
        if (is_array($data)) {
            $cookie=[];
            foreach ($data as $v => $d) {
                $cookie[$v] = $d['key'].'='.$d['value'].';';
                if (isset($d['path'])) {
                    $cookie[$v] .= ' $Path='.$d['path'].';';
                }
                if (isset($d['domain'])) {
                    $cookie[$v] .= ' $Domain='.$d['path'].';';
                }
            }
            if (count($cookie) > 0) {
                return 'Cookie: '.trim(implode(" ", $cookie));
            }
        }
        return false;
    }
}
