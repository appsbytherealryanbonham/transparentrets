# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [2.4.0] - 2022-08-13

No changes since beta 1.

## [2.4.0-beta.1] - 2022-07-06

First beta update to support new admin interface in Open-Realty 3.6.0.

### Added

- Updating UI (tabs) for bootstrap template in Open-Realty 3.6.
- Updating UI Dialogs for bootstrap template in Open-Realty 3.6.

### Changed

### Fixed

## [2.3.3] - 2021-12-23

### Fixed

- Set minimum PHP version to 7.4.3

## [2.3.2] - 2021-11-25

### Fixed

- More PHP 8 ADODB Fixes.

### Changed

- Update php-cs-fixer to latest version

## [2.3.1] - 2021-11-21

### Added

- PHP 8.0 Support

## [2.3.0] - 2021-02-26

No Changes since beta 2.

## [2.3.0-beta.2] - 2021-02-21

### Added

- Ability to rewrite URLS for remote images to force use of https.

### Fixed

- ToolTip header was wrong for Remote Image Setting.

## [v2.3.0-beta.1] - 2021-02-09

### Added

- PHP 7.4 support

### Changed

- Project is now released under MIT license.
- Project is housed on Gitlab
