# TransarentRETS

**Description**: TransparentRETS™ is a import tool, designed to import raw RETS listing data and photos from a Multiple Listing Service (MLS) RETS server directly into a [Open-Realty](https://www.open-realty.org) listing database.

**Screenshot**:

[](https://gitlab.com/appsbytherealryanbonham/transaprentrets/-/raw/main/screenshot.png)

## Dependencies

- PHP v8.0+
- [Open-Realty](https://www.open-realty.org) 3.4.2+

## Installation

[Installation Guide](https://docs.open-realty.org/nav.guide/01.installation/)

## Getting help

For general help, try the [Open-Realty discord server](https://discord.gg/uU7EYnxW).
If you have feature request or bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

See our [CONTRIBUTING](CONTRIBUTING.md) guide.

---

## Open source licensing info

[MIT LICENSE](LICENSE)
